/* ========================================
 * File Name: main.c
 *
 * Version: 1.0
 *
 * Description:
 * This file implements the KWJ Otterbox Sensor Functionality
 *
 * Hardware Dependency:
 * CYBLE-014008
 *
 * Copyright ODI, 2016
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF ODI.
 * RCRegna - Author
 * ========================================
*/
#include <stdio.h>
#include <math.h>

#include "main.h"

#define NESTED_ISR                          (1u)
#define DEFAULT_PRIORITY                    (3u)
#define HIGHER_PRIORITY                     (2u)

/*****************************************************************************
* Connection Parameters structure
*****************************************************************************/
CYBLE_GAP_CONN_UPDATE_PARAM_T bleConnParametersLow = 
{
    360,                /* Minimum connection interval - 360 x 1.25 = 450 ms */
    600,                /* Maximum connection interval - 600 x 1.25 = 750 ms */
    0,                  /* Slave latency - 0 */
    1000                /* Supervision timeout - 1000 x 10 = 10000 ms */
}; //bleConnParametersLow
CYBLE_GAP_CONN_UPDATE_PARAM_T bleConnParametersFast = 
{
    40,                /* Minimum connection interval - 40 x 1.25 = 50 ms */
    80,                /* Maximum connection interval - 80 x 1.25 = 100 ms */
    0,                  /* Slave latency - 0 */
    600                 /* Supervision timeout - 600 x 10 = 6000 ms */
}; //bleConnParametersFast

/*******************************************************************************
* System Flag Register Handling Commands
*******************************************************************************/
static uint32 SystemFlag = 0u;
void setFlag(uint32 theFlag){ SystemFlag |= theFlag; }
void rstFlag(uint32 theFlag){ SystemFlag &= ~theFlag; }
bool chkFlag(uint32 theFlag){ return (SystemFlag & theFlag); }

/*******************************************************************************
* Function Name: System_Init
********************************************************************************
*
* Summary:
*  Initiates the part at startup or after a reset command
*
* Parameters:  
*  None
*
* Return: 
*  None
*
*******************************************************************************/
void System_Init(void)
{    
    /* Enable global interrupt, required for BLESS operation */
    CyGlobalIntEnable; 
    
    // Restore SAV_DB from SFlash
    FlashToStruct(DB_SAVE, (PARAM_DB_T *)&SAV_DB);
    // Initialise SAV_DB Calibration factors
    Init_Bat_DB();
    Init_Ess_DB();
    Init_Gas_DB();
    Init_Dev_DB();
    InitLogType();
    InitLog();
    // Update SFlash from SAV_DB
    WriteUserSFlashRow(DB_SAVE,StructToFlash((PARAM_DB_T *)&SAV_DB, sizeof(SAV_DB)));

    /******************************************************************
    * Set up the Watch Dog Timer which controls the systems cycle time
    * Interrupt priority: Higher (Mid), ISR controls tasks at Wake Up
    ******************************************************************/
    WDT_Interrupt_StartEx(&WatchdogTimer_Isr);
    WatchdogTimer_Start(WDT_1_SEC);
    
    /******************************************************************
    * Set up the Push Button which controls the system's modes
    * Interrupt priority: Default (Low), ISR controls Switch Functions
    ******************************************************************/
    isr_SW_StartEx(&ButtonPress_ISR);
    
    /******************************************************************
    * Set up the LED Timer which controls the LED On time
    * Interrupt priority: Nested (High), ISR turns off LEDs
    ******************************************************************/
    LED_Interrupt_StartEx(&LedTimer_Isr);
    LED_PWM_SetInterruptMode(LED_PWM_INTR_MASK_TC);

    /******************************************************************
    * Set up the BZR Timer which controls the BZR On time
    * Interrupt priority: Nested (High), ISR turns off BZR
    ******************************************************************/
    BZR_Interrupt_StartEx(&BzrTimer_Isr);
    BZR_PWM_SetInterruptMode(BZR_PWM_INTR_MASK_TC);

    /******************************************************************
    * Set up and start the ADC and OpAmps
    ******************************************************************/
    ADC_SAR_12Bit_Start();  // 12 Bit ADC enabled

    // Connect the VREF off-chip for the OPAMP Reference:
    uint32 SARControlReg = CY_GET_REG32(CYREG_SAR_CTRL);
    SARControlReg |= 0x00000080;
    CY_SET_REG32(CYREG_SAR_CTRL, SARControlReg);
    BIAS_OP_Start();  // Bias Opamp enabled with charge pump and lo bias
    AMux_1_Start();   // Connects RE to WE when holding (op off)
    BIAS_HOLD;        // Hold CO Bias
    MEAS_OP_Start();  // Measure Opamp enabled with charge pump and lo bias
    AMux_2_Start();   // Configures MEAS_OP as 1X follower or amplifier
//    MEAS_1X;          // Configure MEAS_OP as 1X follower
    REG_3V3_ON;
    
    /* Set up the sensor precharge timers */
    WRM_COUNT = INIT_WARM_UP;
    if(WRM_COUNT < WARM_UP)
    {
        WRM_COUNT = WARM_UP;
    }
    MIN_COUNT = NUM_MINS_STRT;
    MIN_COUNT == 0u ? rstFlag(START_UP) : setFlag(START_UP);
    

    
    /******************************************************************
    * Set up and start the I2C Port for the T, R/H and P Sensors
    ******************************************************************/
    SCB_Start();
    
    /******************************************************************
    * Set up and start the LED and Buzzer Modulators
    ******************************************************************/
    LED_PWM_Start();                  // LED Blink Rates
    BZR_PWM_Start();                  // Buzzer Annunciation Timing
    BZR_FRQ_Init(); 
    MuteBZR();                        // Buzzer Frequency Off
    AllOffLED();
    alert = GOOD;
    rstFlag(WAKEUP_SOURCE_WDT);
    BLE_RETRY = NUM_RETRY_BLE;
    
} // System_Init()

/* BLEmode() ******************************************
*  Configures System to be in BLE connected mode
******************************************************/
void BLEmode(void)
{
    ECO_ENA;
    IMO_SEL; // clears select register
    ECO_SEL;
    CyDelay(1u); // Provide 1mS delay
    rstFlag(LOW_RATE_BLE);
    rstFlag(STAND_ALONE_MODE);
#  ifdef AUTO_CONNECT
    setFlag(AUTO_CONNECT_MODE);
#  endif
}
/*  SAmode() ******************************************
*  Configures System to be in Stand Alone mode
******************************************************/
void SAmode(void)
{
    CyBle_Stop();
    IMO_SEL;
    ECO_DIS;
    setFlag(NEW_GATT_DB);
    BLE_RETRY = NUM_RETRY_BLE;
    setFlag(ALL_NOTIFICATION);
    CyDelay(1u); // Provide 1mS delay
    setFlag(STAND_ALONE_MODE);
    rstFlag(AUTO_CONNECT_MODE);
}
#ifdef FAIL_SAFE
/* SysTick_ISR() ***************************************
* Provide Fail-Safe protection in the event of a system
* fault hanging the CPU
*******************************************************/
CY_ISR(SysTick_ISR)
{
    SYSTK_CNT++;
    
    switch(SYSTK_CNT)
    {
        case 1: // Possible Hangup, reset system timer
         WatchdogTimer_Isr();
         RED_LED_ON;
         CyDelay(50u);
         RED_LED_OFF;
        break;
        
        case 2: // Hangup, reinit system
         System_Init();
        break;
        
        default: // Total Hangup, reset system
         CySoftwareReset();
        break;
    }
}
#endif
int main()
{
    uint8 interruptStatus;
    
#  ifdef FAIL_SAFE
    CyIntSetSysVector(SYSTICK_VECTOR_NUMBER, SysTick_ISR);
    CySysTickClear();
    SysTick_Config(SYSTK_RELOAD);
    SYSTK_CNT = 0u;
#  endif    /* Powerstate indicators of BLESS */
    CYBLE_LP_MODE_T     lpMode     = CYBLE_BLESS_SLEEP;
    CYBLE_BLESS_STATE_T blessState = CYBLE_BLESS_STATE_SLEEP;
    System_Init();
    SAmode();
#  ifdef AUTO_CONNECT
    BLEmode();
#  endif
    rstFlag(LOW_RATE_BLE);
    setFlag(CHANGE_BLE);
    
    CyBle_Start(ConnectionEventHandler);
    
    for(;;)
    {
        CyBle_ProcessEvents();
        
         if((CyBle_GetState() == CYBLE_STATE_DISCONNECTED) && chkFlag(AUTO_CONNECT_MODE))
         {
            CyBle_GappStartAdvertisement(CYBLE_ADVERTISING_FAST);
         }
        setFlag(DEEP_SLEEP_DISABLED);
        if(chkFlag(LOW_RATE_BLE) || chkFlag( STAND_ALONE_MODE))
        {
            rstFlag(DEEP_SLEEP_DISABLED);
        }
#      ifndef DS_OKAY
        setFlag(DEEP_SLEEP_DISABLED);
#      endif
        /* Periodically measure sensors and send results to the Client */
        if(chkFlag(WAKEUP_SOURCE_WDT))
        {
#          ifdef FAIL_SAFE
            SYSTK_CNT = 0u;
            CySysTickClear();
            SysTick_Config(SYSTK_RELOAD);
#          endif
            UpdateCycle();
            SampleAll();
        }
        if(false == chkFlag(STAND_ALONE_MODE))
        {
            if(false == chkFlag(LOW_RATE_BLE))
            {
                setFlag(DEEP_SLEEP_DISABLED);
            }
            if(CyBle_GetState() != CYBLE_STATE_CONNECTED)
            {
                CyBle_Shutdown();
                CYBLE_API_RESULT_T stackStatus = CyBle_Start(ConnectionEventHandler);
                if(false == chkFlag(AUTO_CONNECT_MODE) && 
                    stackStatus == CYBLE_ERROR_REPEATED_ATTEMPTS)
                {
                    WatchdogTimer_Start(WDT_1_SEC);//ts
                    SAmode();
                    HandleBZR(BZR_ALERT, true);
                }
            }
            else
            {
                /* Check BLE Rate if connected */
                if(chkFlag(CHANGE_BLE) && (false == chkFlag(NEW_GATT_DB)))
                {
                    if(chkFlag(LOW_RATE_BLE))
                    {
                        CyBle_L2capLeConnectionParamUpdateRequest(
                         cyBle_connHandle.bdHandle, &bleConnParametersLow);
                    }
                    else
                    {
                        CyBle_L2capLeConnectionParamUpdateRequest(
                         cyBle_connHandle.bdHandle, &bleConnParametersFast);
                    }
                }
            }
            if(false == chkFlag( STAND_ALONE_MODE))
            {
                CyBle_ProcessEvents();
            }
        }
        if(false == chkFlag(ENABLE_HIBERNATE))
        {
            if(chkFlag(MEASUREMENT_INTERVAL))
            {
                /* Clear Measurement Interval flag */
                rstFlag(MEASUREMENT_INTERVAL);
                // check connection
                if(CyBle_GetState() != CYBLE_STATE_CONNECTED)
                {
                    if(false == chkFlag(AUTO_CONNECT_MODE) && 
                        false == chkFlag(STAND_ALONE_MODE))
                    {
                        if(--BLE_RETRY == 0)
                        {
                          /* Sets the STAND_ALONE_MODE flag to put system in Stand Alone mode */
                            SAmode();
                            HandleBZR(BZR_ALERT, true);
                        }
                    }
                }
                if(false == chkFlag( STAND_ALONE_MODE))
                {
                    KWJ_BLE_Process();
                }
            }

            /* Enter a sleep mode if OK */
            if(chkFlag(DEEP_SLEEP_DISABLED) | chkFlag(LED_PWM_RUNNING) | chkFlag(BZR_PWM_RUNNING))
            {
                GoToSleep();
            }
            else
            {
                if(chkFlag(STAND_ALONE_MODE))
                {
                    GoToDeepSleep();
                }
                else
                {
                    /* If BLE is successfully initialized then execute the lo pwr routine */
                    if( CyBle_GetState() == CYBLE_STATE_ADVERTISING ||
                        CyBle_GetState() == CYBLE_STATE_CONNECTED)
                    {
                        
                        /* Enter Deep Sleep mode between connection intervals */
                        lpMode = CyBle_EnterLPM(CYBLE_BLESS_DEEPSLEEP); // uses 2K!
                        blessState = CyBle_GetBleSsState();

                        interruptStatus = CyEnterCriticalSection();
                        /* Try putting system in DeepSleep mode if BLESS succesfully entered Deep Sleep mode*/
                        if(lpMode == CYBLE_BLESS_DEEPSLEEP &&
                            blessState == CYBLE_BLESS_STATE_DEEPSLEEP)// && chkFlag(LOW_RATE_BLE)) 
                        {
                            GoToDeepSleep();
                        }
                        /* If BLESS is in Active state */
                        else
                        {
                            /* If BLESS Tx/Rx Event is not complete, reduce IMO and put CPU to Sleep */
                            if(blessState != CYBLE_BLESS_STATE_EVENT_CLOSE)
                            {
                                GoToSleep();
                            }
                        }
                        CyExitCriticalSection(interruptStatus);
                    }    
                }
            }
            /* Wakeup the system from any sleep mode */
            WakeFromSleep();
        }
        else
        {
            Hibernate();
        }
    }
} // main()

/* [] END OF FILE */
