/* ========================================
 * File Name: KWJ_BLE_Process.h
 *
 * Version: 1.0
 *
 * Description:
 * This file defines the handles for the GATT DB Entries for BLE Communication
 *
 * Hardware Dependency:
 * CYBLE-014008
 *
 * Copyright ODI, 2016
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF ODI.
 * RCRegna - Author
 * ========================================
*/
#ifndef KWJ_BLE_H
#define KWJ_BLE_H

#include <project.h>
    
/* KWJ Custom Service Indexes */
#define KWJ_SERVICE_HANDLE (CYBLE_KWJ_SERVICE_HANDLE)

/* KWJ Custom CO Service Characteristic */
#define CO_CHARC (CYBLE_KWJ_CO_CHAR_HANDLE) /*< Handle of the CO Measurement Characteristic */ 
#define CO_MEASD (CYBLE_KWJ_CO_MEASUREMENT_DESC_HANDLE) /*< Handle of the CO Measurement descriptor */ 
#define CO_CCCD  (CYBLE_KWJ_CO_CLIENT_CHARACTERISTIC_CONFIGURATION_DESC_HANDLE) /*< Handle of the Client Characteristic Configuration descriptor */ 
#define CO_UNIT  (CYBLE_KWJ_CO_UNITS_DESC_HANDLE) /*< Handle of the Units descriptor */
#define CO_CUDD  (CYBLE_KWJ_CO_CHARACTERISTIC_USER_DESCRIPTION_DESC_HANDLE) /*< Handle of the Characteristic User Description descriptor */ 

/* KWJ Custom O3 Service Characteristic */
#define O3_CHARC (CYBLE_KWJ_O3_CHAR_HANDLE) /*< Handle of the O3 Measurement Characteristic */ 
#define O3_MEASD (CYBLE_KWJ_O3_MEASUREMENT_DESC_HANDLE) /*< Handle of the O3 Measurement descriptor */ 
#define O3_CCCD  (CYBLE_KWJ_O3_CLIENT_CHARACTERISTIC_CONFIGURATION_DESC_HANDLE) /*< Handle of the Client Characteristic Configuration descriptor */ 
#define O3_UNIT  (CYBLE_KWJ_O3_UNITS_DESC_HANDLE) /*< Handle of the Units descriptor */
#define O3_CUDD  (CYBLE_KWJ_O3_CHARACTERISTIC_USER_DESCRIPTION_DESC_HANDLE) /*< Handle of the Characteristic User Description descriptor */ 

/* KWJ Custom Data Log Service Characteristic */
#define LOG_CHARC     (CYBLE_KWJ_LOG_CHAR_HANDLE) /*< Handle of the LOG Characteristic */ 
#define LOG_CCCD      (CYBLE_KWJ_LOG_CLIENT_CHARACTERISTIC_CONFIGURATION_DESC_HANDLE) /*< Handle of the Client Characteristic Configuration descriptor */ 
#define LOG_TIME_STMP (CYBLE_KWJ_LOG_TIMESTAMP_DESC_HANDLE) /*< Handle of the Timestamp descriptor */ 
#define LOG_BASE16    (CYBLE_KWJ_LOG_BASE16_DESC_HANDLE) /*< Handle of the 16 bit Base array descriptor */ 
#define LOG_BASE32    (CYBLE_KWJ_LOG_BASE32_DESC_HANDLE) /*< Handle of the 32 bit Base descriptor */ 
#define LOG_TYPE      (CYBLE_KWJ_LOG_TYPE_DESC_HANDLE) /*< Handle of the Type descriptor */ 
#define LOG_LENGTH    (CYBLE_KWJ_LOG_LENGTH_DESC_HANDLE) /*< Handle of the Length descriptor */ 
#define LOG_DATA      (CYBLE_KWJ_LOG_DATA_DESC_HANDLE) /*< Handle of the Data descriptor */ 
#define LOG_CUDD      (CYBLE_KWJ_LOG_CHARACTERISTIC_USER_DESCRIPTION_DESC_HANDLE) /*< Handle of the Characteristic User Description descriptor */ 

/* KWJ Custom Calibration Service Characteristic */
#define CAL_CHARC        (CYBLE_KWJ_CAL_CHAR_HANDLE) /*< Handle of the CAL Characteristic */ 
#define CO_RAW_INDEX     (CYBLE_KWJ_CAL_CO_RAW_DESC_HANDLE) /*< Handle of the CO Raw descriptor */ 
#define CO_GAIN_INDEX    (CYBLE_KWJ_CAL_CO_GAIN_DESC_HANDLE) /*< Handle of the CO Gain descriptor */ 
#define CO_OS_INDEX      (CYBLE_KWJ_CAL_CO_OS_DESC_HANDLE) /*< Handle of the CO OS descriptor */ 
#define CO_TZRO_TC_INDEX (CYBLE_KWJ_CAL_CO_TZERO_TC_DESC_HANDLE) /*< Handle of the CO Temp Zero descriptor */ 
#define CO_SNSFAC_INDEX  (CYBLE_KWJ_CAL_CO_SNSFAC_DESC_HANDLE) /*< Handle of the CO Sensitivity Factor descriptor */ 
#define CO_MOD_INDEX     (CYBLE_KWJ_CAL_CO_MOD_DESC_HANDLE) /*< Handle of the CO MOD descriptor */ 
#define CO_UNH_INDEX     (CYBLE_KWJ_CAL_CO_UNH_DESC_HANDLE) /*< Handle of the CO UNH descriptor */ 
#define CO_HAZ_INDEX     (CYBLE_KWJ_CAL_CO_HAZ_DESC_HANDLE) /*< Handle of the CO HAZ descriptor */ 
#define O3_RAW_INDEX     (CYBLE_KWJ_CAL_O3_RAW_DESC_HANDLE) /*< Handle of the O3 Raw descriptor */ 
#define O3_GAIN_INDEX    (CYBLE_KWJ_CAL_O3_GAIN_DESC_HANDLE) /*< Handle of the O3 Gain descriptor */ 
#define O3_OS_INDEX      (CYBLE_KWJ_CAL_O3_OS_DESC_HANDLE) /*< Handle of the O3 OS descriptor */ 
#define O3_TZRO_TC_INDEX (CYBLE_KWJ_CAL_O3_TZERO_TC_DESC_HANDLE) /*< Handle of the O3 Temp Zero descriptor */ 
#define O3_SNSFAC_INDEX  (CYBLE_KWJ_CAL_O3_SNSFAC_DESC_HANDLE) /*< Handle of the O3 Sensitivity Factor descriptor */ 
#define O3_MOD_INDEX     (CYBLE_KWJ_CAL_O3_MOD_DESC_HANDLE) /*< Handle of the O3 MOD descriptor */ 
#define O3_UNH_INDEX     (CYBLE_KWJ_CAL_O3_UNH_DESC_HANDLE) /*< Handle of the O3 UNH descriptor */ 
#define O3_HAZ_INDEX     (CYBLE_KWJ_CAL_O3_HAZ_DESC_HANDLE) /*< Handle of the O3 HAZ descriptor */ 
#define T_RAW_INDEX      (CYBLE_KWJ_CAL_T_RAW_DESC_HANDLE) /*< Handle of the T Raw descriptor */ 
#define T_GAIN_INDEX     (CYBLE_KWJ_CAL_T_GAIN_DESC_HANDLE) /*< Handle of the T Gain descriptor */ 
#define T_OS_INDEX       (CYBLE_KWJ_CAL_T_OS_DESC_HANDLE) /*< Handle of the T OS descriptor */ 
#define RH_RAW_INDEX     (CYBLE_KWJ_CAL_RH_RAW_DESC_HANDLE) /*< Handle of the RH Raw descriptor */ 
#define RH_GAIN_INDEX    (CYBLE_KWJ_CAL_RH_GAIN_DESC_HANDLE) /*< Handle of the RH Gain descriptor */ 
#define RH_OS_INDEX      (CYBLE_KWJ_CAL_RH_OS_DESC_HANDLE) /*< Handle of the RH OS descriptor */ 
#define P_RAW_INDEX      (CYBLE_KWJ_CAL_P_RAW_DESC_HANDLE) /*< Handle of the P Raw descriptor */ 
#define P_GAIN_INDEX     (CYBLE_KWJ_CAL_P_GAIN_DESC_HANDLE) /*< Handle of the P Gain descriptor */ 
#define P_OS_INDEX       (CYBLE_KWJ_CAL_P_OS_DESC_HANDLE) /*< Handle of the P OS descriptor */ 
#define BAT_RAW_INDEX    (CYBLE_KWJ_CAL_BAT_RAW_DESC_HANDLE) /*< Handle of the BAT Raw descriptor */ 
#define BAT_GAIN_INDEX   (CYBLE_KWJ_CAL_BAT_GAIN_DESC_HANDLE) /*< Handle of the BAT Gain descriptor */ 
#define BAT_OS_INDEX     (CYBLE_KWJ_CAL_BAT_OS_DESC_HANDLE) /*< Handle of the BAT OS descriptor */ 
#define BAT_LO_INDEX     (CYBLE_KWJ_CAL_BAT_LO_DESC_HANDLE) /*< Handle of the BAT LO descriptor */ 
#define BAT_CRIT_INDEX   (CYBLE_KWJ_CAL_BAT_CRIT_DESC_HANDLE) /*< Handle of the BAT CRIT descriptor */
#define CAL_CCCD         (CYBLE_KWJ_CAL_CLIENT_CHARACTERISTIC_CONFIGURATION_DESC_HANDLE) /*< Handle of the Client Characteristic Configuration descriptor */ 
#define CAL_CUDD         (CYBLE_KWJ_CAL_CHARACTERISTIC_USER_DESCRIPTION_DESC_HANDLE) /*< Handle of the Characteristic User Description descriptor */ 

/* Current Time */
#define CURRENT_TIME     (CYBLE_KWJ_CAL_CURRENT_TIME_DESC_HANDLE) /* Handle of Current Time characteristic */

/* ESS Environmental Service Indexes */
#define ESS_SERVICE_HANDLE           (0x0049u) /*< Handle of Environmental Sensor Service */
#define ESS_DESCRIPTOR_VALUE_CHANGED (0x004Bu) /*< Descriptor Value Changed Characteristic index */
#define ESS_CCCD                     (0x004Cu) /*< Handle of the Client Characteristic Configuration descriptor */ 

/* Humidity characteristic data */
#define ESS_HUMIDITY             (0x004Eu) /*< Humidity Characteristic index */
#define ESS_HUMIDITY_MEAS        (0x004Fu) /*< Handle of the ES Measurement descriptor */ 
#define ESS_HUMIDITY_TRIGGER     (0x0050u) /*< Handle of the ES Trigger Setting descriptor */ 
#define ESS_HUMIDITY_CUDD        (0x0051u) /*< Handle of the Characteristic User Description descriptor */ 
#define ESS_HUMIDITY_VALID_RANGE (0x0052u) /*< Handle of the Valid Range descriptor */ 
#define ESS_HUMIDITY_CCCD        (0x0053u) /*< Handle of the Client Characteristic Configuration descriptor */ 

/* Pressure characteristic data */
#define ESS_PRESSURE             (0x0055u) /*< Handle of the Pressure characteristic */ 
#define ESS_PRESSURE_MEAS        (0x0056u) /*< Handle of the ES Measurement descriptor */ 
#define ESS_PRESSURE_TRIGGER     (0x0057u) /*< Handle of the ES Trigger Setting descriptor */ 
#define ESS_PRESSURE_CUDD        (0x0058u) /*< Handle of the Characteristic User Description descriptor */ 
#define ESS_PRESSURE_VALID_RANGE (0x0059u) /*< Handle of the Valid Range descriptor */ 
#define ESS_PRESSURE_CCCD        (0x005Au) /*< Handle of the Client Characteristic Configuration descriptor */ 

/* Temperature characteristic data */
#define ESS_TEMPERATURE             (0x005Cu) /*< Handle of the Temperature characteristic */ 
#define ESS_TEMPERATURE_MEAS        (0x005Du) /*< Handle of the ES Measurement descriptor */ 
#define ESS_TEMPERATURE_TRIGGER     (0x005Eu) /*< Handle of the ES Trigger Setting descriptor */ 
#define ESS_TEMPERATURE_CUDD        (0x005Fu) /*< Handle of the Characteristic User Description descriptor */ 
#define ESS_TEMPERATURE_VALID_RANGE (0x0060u) /*< Handle of the Valid Range descriptor */ 
#define ESS_TEMPERATURE_CCCD        (0x0061u) /*< Handle of the Client Characteristic Configuration descriptor */ 

/* BAS Battery Service Indexes */
#define BAS_SERVICE_HANDLE (0x0062u) /*< Handle of Battery Service */
#define BAS_BATTERY_LEVEL  (0x0064u) /*< Handle of the Battery Level characteristic */ 
#define BAS_BATTERY_CPFD   (0x0065u) /*< Handle of the Characteristic Presentation Format descriptor */ 
#define BAS_BATTERY_CCCD   (0x0066u) /*< Handle of the Client Characteristic Configuration descriptor */ 

/* DIS Device Service Indexes */
#define DIS_SERVICE_HANDLE   (0x0067u) /*< Handle of Device Information Service */
#define DIS_DEVICE_MNFR_NAME (0x0069u) /*< Handle of the Manufacturer Name String characteristic */ 
#define DIS_DEVICE_MODL_NMBR (0x006Bu) /*< Handle of the Model Number String characteristic */ 
#define DIS_DEVICE_SER_NMBR  (0x006Du) /*< Handle of the Serial Number String characteristic */ 
#define DIS_DEVICE_HW_REV    (0x006Fu) /*< Handle of the Hardware Revision String characteristic */ 
#define DIS_DEVICE_FW_REV    (0x0071u) /*< Handle of the Firmware Revision String characteristic */ 
#define DIS_DEVICE_SW_REV    (0x0073u) /*< Handle of the Software Revision String characteristic */ 
#define DIS_DEVICE_SYS_ID    (0x0075u) /*< Handle of the System ID characteristic */ 
#define DIS_DEVICE_IEEE      (0x0077u) /*< Handle of the IEEE 11073-20601 Regulatory Certification Data List characteristic */ 
#define DIS_DEVICE_PNP_ID    (0x0079u) /*< Handle of the PnP ID characteristic */ 

#endif
/* [] END OF FILE */
