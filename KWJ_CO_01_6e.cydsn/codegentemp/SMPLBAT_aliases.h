/*******************************************************************************
* File Name: SMPLBAT.h  
* Version 2.20
*
* Description:
*  This file contains the Alias definitions for Per-Pin APIs in cypins.h. 
*  Information on using these APIs can be found in the System Reference Guide.
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_SMPLBAT_ALIASES_H) /* Pins SMPLBAT_ALIASES_H */
#define CY_PINS_SMPLBAT_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"


/***************************************
*              Constants        
***************************************/
#define SMPLBAT_0			(SMPLBAT__0__PC)
#define SMPLBAT_0_PS		(SMPLBAT__0__PS)
#define SMPLBAT_0_PC		(SMPLBAT__0__PC)
#define SMPLBAT_0_DR		(SMPLBAT__0__DR)
#define SMPLBAT_0_SHIFT	(SMPLBAT__0__SHIFT)
#define SMPLBAT_0_INTR	((uint16)((uint16)0x0003u << (SMPLBAT__0__SHIFT*2u)))

#define SMPLBAT_INTR_ALL	 ((uint16)(SMPLBAT_0_INTR))


#endif /* End Pins SMPLBAT_ALIASES_H */


/* [] END OF FILE */
