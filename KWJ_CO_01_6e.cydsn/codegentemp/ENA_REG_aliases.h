/*******************************************************************************
* File Name: ENA_REG.h  
* Version 2.20
*
* Description:
*  This file contains the Alias definitions for Per-Pin APIs in cypins.h. 
*  Information on using these APIs can be found in the System Reference Guide.
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_ENA_REG_ALIASES_H) /* Pins ENA_REG_ALIASES_H */
#define CY_PINS_ENA_REG_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"


/***************************************
*              Constants        
***************************************/
#define ENA_REG_0			(ENA_REG__0__PC)
#define ENA_REG_0_PS		(ENA_REG__0__PS)
#define ENA_REG_0_PC		(ENA_REG__0__PC)
#define ENA_REG_0_DR		(ENA_REG__0__DR)
#define ENA_REG_0_SHIFT	(ENA_REG__0__SHIFT)
#define ENA_REG_0_INTR	((uint16)((uint16)0x0003u << (ENA_REG__0__SHIFT*2u)))

#define ENA_REG_INTR_ALL	 ((uint16)(ENA_REG_0_INTR))


#endif /* End Pins ENA_REG_ALIASES_H */


/* [] END OF FILE */
