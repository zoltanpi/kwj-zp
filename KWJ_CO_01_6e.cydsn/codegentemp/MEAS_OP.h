/*******************************************************************************
* File Name: MEAS_OP.h
* Version 1.20
*
* Description:
*  This file contains the function prototypes and constants used in
*  the Opamp (Analog Buffer) Component.
*
*
********************************************************************************
* Copyright 2013-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/


#if !defined(CY_OPAMP_MEAS_OP_H)
#define CY_OPAMP_MEAS_OP_H

#include "cytypes.h"
#include "cyfitter.h"


/***************************************
*       Type Definitions
***************************************/

/* Structure to save state before go to sleep */
typedef struct
{
    uint8  enableState;
} MEAS_OP_BACKUP_STRUCT;


/**************************************
*        Function Prototypes
**************************************/
void MEAS_OP_Init(void);
void MEAS_OP_Enable(void);
void MEAS_OP_Start(void);
void MEAS_OP_Stop(void);
void MEAS_OP_SetPower(uint32 power);
void MEAS_OP_PumpControl(uint32 onOff);
void MEAS_OP_Sleep(void);
void MEAS_OP_Wakeup(void);
void MEAS_OP_SaveConfig(void);
void MEAS_OP_RestoreConfig(void);


/**************************************
*           API Constants
**************************************/

/* Parameters for SetPower() function */
#define MEAS_OP_LOW_POWER      (1u)
#define MEAS_OP_MED_POWER      (2u)
#define MEAS_OP_HIGH_POWER     (3u)


/* Parameters for PumpControl() function */
#define MEAS_OP_PUMP_ON        (1u)
#define MEAS_OP_PUMP_OFF       (0u)


/***************************************
*   Initial Parameter Constants
****************************************/

#define MEAS_OP_OUTPUT_CURRENT         (1u)
#define MEAS_OP_POWER                  (1u)
#define MEAS_OP_MODE                   (0u)
#define MEAS_OP_OA_COMP_TRIM_VALUE     (1u)
#define MEAS_OP_DEEPSLEEP_SUPPORT      (0u)


/***************************************
*    Variables with External Linkage
***************************************/

extern uint8  MEAS_OP_initVar;


/**************************************
*             Registers
**************************************/

#ifdef CYIPBLOCK_m0s8pass4b_VERSION
    #define MEAS_OP_CTB_CTRL_REG       (*(reg32 *) MEAS_OP_cy_psoc4_abuf__CTB_CTB_CTRL)
    #define MEAS_OP_CTB_CTRL_PTR       ( (reg32 *) MEAS_OP_cy_psoc4_abuf__CTB_CTB_CTRL)
#else
    #define MEAS_OP_CTB_CTRL_REG       (*(reg32 *) MEAS_OP_cy_psoc4_abuf__CTBM_CTB_CTRL)
    #define MEAS_OP_CTB_CTRL_PTR       ( (reg32 *) MEAS_OP_cy_psoc4_abuf__CTBM_CTB_CTRL)
#endif /* CYIPBLOCK_m0s8pass4b_VERSION */

#define MEAS_OP_OA_RES_CTRL_REG    (*(reg32 *) MEAS_OP_cy_psoc4_abuf__OA_RES_CTRL)
#define MEAS_OP_OA_RES_CTRL_PTR    ( (reg32 *) MEAS_OP_cy_psoc4_abuf__OA_RES_CTRL)
#define MEAS_OP_OA_COMP_TRIM_REG   (*(reg32 *) MEAS_OP_cy_psoc4_abuf__OA_COMP_TRIM)
#define MEAS_OP_OA_COMP_TRIM_PTR   ( (reg32 *) MEAS_OP_cy_psoc4_abuf__OA_COMP_TRIM)


/***************************************
*        Registers Constants
***************************************/

/* MEAS_OP_CTB_CTRL_REG */
#define MEAS_OP_CTB_CTRL_DEEPSLEEP_ON_SHIFT    (30u)   /* [30] Selects behavior CTB IP in the DeepSleep power mode */
#define MEAS_OP_CTB_CTRL_ENABLED_SHIFT         (31u)   /* [31] Enable of the CTB IP */


#define MEAS_OP_CTB_CTRL_DEEPSLEEP_ON          ((uint32) 0x01u << MEAS_OP_CTB_CTRL_DEEPSLEEP_ON_SHIFT)
#define MEAS_OP_CTB_CTRL_ENABLED               ((uint32) 0x01u << MEAS_OP_CTB_CTRL_ENABLED_SHIFT)


/* MEAS_OP_OA_RES_CTRL_REG */
#define MEAS_OP_OA_PWR_MODE_SHIFT          (0u)    /* [1:0]    Power level */
#define MEAS_OP_OA_DRIVE_STR_SEL_SHIFT     (2u)    /* [2]      Opamp output strenght select: 0 - 1x, 1 - 10x */
#define MEAS_OP_OA_COMP_EN_SHIFT           (4u)    /* [4]      CTB IP mode: 0 - Opamp, 1 - Comparator  */
#define MEAS_OP_OA_PUMP_EN_SHIFT           (11u)   /* [11]     Pump enable */


#define MEAS_OP_OA_PWR_MODE                ((uint32) 0x02u << MEAS_OP_OA_PWR_MODE_SHIFT)
#define MEAS_OP_OA_PWR_MODE_MASK           ((uint32) 0x03u << MEAS_OP_OA_PWR_MODE_SHIFT)
#define MEAS_OP_OA_DRIVE_STR_SEL_1X        ((uint32) 0x00u << MEAS_OP_OA_DRIVE_STR_SEL_SHIFT)
#define MEAS_OP_OA_DRIVE_STR_SEL_10X       ((uint32) 0x01u << MEAS_OP_OA_DRIVE_STR_SEL_SHIFT)
#define MEAS_OP_OA_DRIVE_STR_SEL_MASK      ((uint32) 0x01u << MEAS_OP_OA_DRIVE_STR_SEL_SHIFT)
#define MEAS_OP_OA_COMP_EN                 ((uint32) 0x00u << MEAS_OP_OA_COMP_EN_SHIFT)
#define MEAS_OP_OA_PUMP_EN                 ((uint32) 0x01u << MEAS_OP_OA_PUMP_EN_SHIFT)


/***************************************
*       Init Macros Definitions
***************************************/

#define MEAS_OP_GET_DEEPSLEEP_ON(deepSleep)    ((0u != (deepSleep)) ? (MEAS_OP_CTB_CTRL_DEEPSLEEP_ON) : (0u))
#define MEAS_OP_GET_OA_DRIVE_STR(current)      ((0u != (current)) ? (MEAS_OP_OA_DRIVE_STR_SEL_10X) : \
                                                                             (MEAS_OP_OA_DRIVE_STR_SEL_1X))
#define MEAS_OP_GET_OA_PWR_MODE(mode)          ((mode) & MEAS_OP_OA_PWR_MODE_MASK)
#define MEAS_OP_CHECK_PWR_MODE_OFF             (0u != (MEAS_OP_OA_RES_CTRL_REG & \
                                                                MEAS_OP_OA_PWR_MODE_MASK))

/* Returns true if component available in Deep Sleep power mode*/ 
#define MEAS_OP_CHECK_DEEPSLEEP_SUPPORT        (0u != MEAS_OP_DEEPSLEEP_SUPPORT) 

#define MEAS_OP_DEFAULT_CTB_CTRL (MEAS_OP_GET_DEEPSLEEP_ON(MEAS_OP_DEEPSLEEP_SUPPORT) | \
                                           MEAS_OP_CTB_CTRL_ENABLED)

#define MEAS_OP_DEFAULT_OA_RES_CTRL (MEAS_OP_OA_COMP_EN | \
                                              MEAS_OP_GET_OA_DRIVE_STR(MEAS_OP_OUTPUT_CURRENT))

#define MEAS_OP_DEFAULT_OA_COMP_TRIM_REG (MEAS_OP_OA_COMP_TRIM_VALUE)


/***************************************
* The following code is DEPRECATED and 
* should not be used in new projects.
***************************************/

#define MEAS_OP_LOWPOWER                   (MEAS_OP_LOW_POWER)
#define MEAS_OP_MEDPOWER                   (MEAS_OP_MED_POWER)
#define MEAS_OP_HIGHPOWER                  (MEAS_OP_HIGH_POWER)

/* PUMP ON/OFF defines */
#define MEAS_OP_PUMPON                     (MEAS_OP_PUMP_ON)
#define MEAS_OP_PUMPOFF                    (MEAS_OP_PUMP_OFF)

#define MEAS_OP_OA_CTRL                    (MEAS_OP_CTB_CTRL_REG)
#define MEAS_OP_OA_RES_CTRL                (MEAS_OP_OA_RES_CTRL_REG)

/* Bit Field  OA_CTRL */
#define MEAS_OP_OA_CTB_EN_SHIFT            (MEAS_OP_CTB_CTRL_ENABLED_SHIFT)
#define MEAS_OP_OA_PUMP_CTRL_SHIFT         (MEAS_OP_OA_PUMP_EN_SHIFT)
#define MEAS_OP_OA_PUMP_EN_MASK            (0x800u)
#define MEAS_OP_PUMP_PROTECT_MASK          (1u)


#endif    /* CY_OPAMP_MEAS_OP_H */


/* [] END OF FILE */
