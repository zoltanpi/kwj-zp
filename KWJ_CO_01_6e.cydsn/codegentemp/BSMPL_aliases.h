/*******************************************************************************
* File Name: BSMPL.h  
* Version 2.20
*
* Description:
*  This file contains the Alias definitions for Per-Pin APIs in cypins.h. 
*  Information on using these APIs can be found in the System Reference Guide.
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_BSMPL_ALIASES_H) /* Pins BSMPL_ALIASES_H */
#define CY_PINS_BSMPL_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"


/***************************************
*              Constants        
***************************************/
#define BSMPL_0			(BSMPL__0__PC)
#define BSMPL_0_PS		(BSMPL__0__PS)
#define BSMPL_0_PC		(BSMPL__0__PC)
#define BSMPL_0_DR		(BSMPL__0__DR)
#define BSMPL_0_SHIFT	(BSMPL__0__SHIFT)
#define BSMPL_0_INTR	((uint16)((uint16)0x0003u << (BSMPL__0__SHIFT*2u)))

#define BSMPL_INTR_ALL	 ((uint16)(BSMPL_0_INTR))


#endif /* End Pins BSMPL_ALIASES_H */


/* [] END OF FILE */
