/*******************************************************************************
* File Name: FAULT_B.h  
* Version 2.20
*
* Description:
*  This file contains the Alias definitions for Per-Pin APIs in cypins.h. 
*  Information on using these APIs can be found in the System Reference Guide.
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_FAULT_B_ALIASES_H) /* Pins FAULT_B_ALIASES_H */
#define CY_PINS_FAULT_B_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"


/***************************************
*              Constants        
***************************************/
#define FAULT_B_0			(FAULT_B__0__PC)
#define FAULT_B_0_PS		(FAULT_B__0__PS)
#define FAULT_B_0_PC		(FAULT_B__0__PC)
#define FAULT_B_0_DR		(FAULT_B__0__DR)
#define FAULT_B_0_SHIFT	(FAULT_B__0__SHIFT)
#define FAULT_B_0_INTR	((uint16)((uint16)0x0003u << (FAULT_B__0__SHIFT*2u)))

#define FAULT_B_INTR_ALL	 ((uint16)(FAULT_B_0_INTR))


#endif /* End Pins FAULT_B_ALIASES_H */


/* [] END OF FILE */
