/*******************************************************************************
* File Name: CK32KHz.h
* Version 2.20
*
*  Description:
*   Provides the function and constant definitions for the clock component.
*
*  Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_CLOCK_CK32KHz_H)
#define CY_CLOCK_CK32KHz_H

#include <cytypes.h>
#include <cyfitter.h>


/***************************************
*        Function Prototypes
***************************************/
#if defined CYREG_PERI_DIV_CMD

void CK32KHz_StartEx(uint32 alignClkDiv);
#define CK32KHz_Start() \
    CK32KHz_StartEx(CK32KHz__PA_DIV_ID)

#else

void CK32KHz_Start(void);

#endif/* CYREG_PERI_DIV_CMD */

void CK32KHz_Stop(void);

void CK32KHz_SetFractionalDividerRegister(uint16 clkDivider, uint8 clkFractional);

uint16 CK32KHz_GetDividerRegister(void);
uint8  CK32KHz_GetFractionalDividerRegister(void);

#define CK32KHz_Enable()                         CK32KHz_Start()
#define CK32KHz_Disable()                        CK32KHz_Stop()
#define CK32KHz_SetDividerRegister(clkDivider, reset)  \
    CK32KHz_SetFractionalDividerRegister((clkDivider), 0u)
#define CK32KHz_SetDivider(clkDivider)           CK32KHz_SetDividerRegister((clkDivider), 1u)
#define CK32KHz_SetDividerValue(clkDivider)      CK32KHz_SetDividerRegister((clkDivider) - 1u, 1u)


/***************************************
*             Registers
***************************************/
#if defined CYREG_PERI_DIV_CMD

#define CK32KHz_DIV_ID     CK32KHz__DIV_ID

#define CK32KHz_CMD_REG    (*(reg32 *)CYREG_PERI_DIV_CMD)
#define CK32KHz_CTRL_REG   (*(reg32 *)CK32KHz__CTRL_REGISTER)
#define CK32KHz_DIV_REG    (*(reg32 *)CK32KHz__DIV_REGISTER)

#define CK32KHz_CMD_DIV_SHIFT          (0u)
#define CK32KHz_CMD_PA_DIV_SHIFT       (8u)
#define CK32KHz_CMD_DISABLE_SHIFT      (30u)
#define CK32KHz_CMD_ENABLE_SHIFT       (31u)

#define CK32KHz_CMD_DISABLE_MASK       ((uint32)((uint32)1u << CK32KHz_CMD_DISABLE_SHIFT))
#define CK32KHz_CMD_ENABLE_MASK        ((uint32)((uint32)1u << CK32KHz_CMD_ENABLE_SHIFT))

#define CK32KHz_DIV_FRAC_MASK  (0x000000F8u)
#define CK32KHz_DIV_FRAC_SHIFT (3u)
#define CK32KHz_DIV_INT_MASK   (0xFFFFFF00u)
#define CK32KHz_DIV_INT_SHIFT  (8u)

#else 

#define CK32KHz_DIV_REG        (*(reg32 *)CK32KHz__REGISTER)
#define CK32KHz_ENABLE_REG     CK32KHz_DIV_REG
#define CK32KHz_DIV_FRAC_MASK  CK32KHz__FRAC_MASK
#define CK32KHz_DIV_FRAC_SHIFT (16u)
#define CK32KHz_DIV_INT_MASK   CK32KHz__DIVIDER_MASK
#define CK32KHz_DIV_INT_SHIFT  (0u)

#endif/* CYREG_PERI_DIV_CMD */

#endif /* !defined(CY_CLOCK_CK32KHz_H) */

/* [] END OF FILE */
