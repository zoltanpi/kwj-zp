/*******************************************************************************
* File Name: OPREF.h  
* Version 2.20
*
* Description:
*  This file contains the Alias definitions for Per-Pin APIs in cypins.h. 
*  Information on using these APIs can be found in the System Reference Guide.
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_OPREF_ALIASES_H) /* Pins OPREF_ALIASES_H */
#define CY_PINS_OPREF_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"


/***************************************
*              Constants        
***************************************/
#define OPREF_0			(OPREF__0__PC)
#define OPREF_0_PS		(OPREF__0__PS)
#define OPREF_0_PC		(OPREF__0__PC)
#define OPREF_0_DR		(OPREF__0__DR)
#define OPREF_0_SHIFT	(OPREF__0__SHIFT)
#define OPREF_0_INTR	((uint16)((uint16)0x0003u << (OPREF__0__SHIFT*2u)))

#define OPREF_INTR_ALL	 ((uint16)(OPREF_0_INTR))


#endif /* End Pins OPREF_ALIASES_H */


/* [] END OF FILE */
