/*******************************************************************************
* File Name: BIAS_OP.c
* Version 1.20
*
* Description:
*  This file provides the source code to the API for the Opamp (Analog Buffer)
*  Component.
*
*
********************************************************************************
* Copyright 2013-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "BIAS_OP.h"

uint8 BIAS_OP_initVar = 0u; /* Defines if component was initialized */
static uint32 BIAS_OP_internalPower = 0u; /* Defines component Power value */


/*******************************************************************************
* Function Name: BIAS_OP_Init
********************************************************************************
*
* Summary:
*  Initializes or restores the component according to the customizer Configure 
*  dialog settings. It is not necessary to call Init() because the Start() API 
*  calls this function and is the preferred method to begin the component operation.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void BIAS_OP_Init(void)
{
    BIAS_OP_internalPower = BIAS_OP_POWER;
    BIAS_OP_CTB_CTRL_REG = BIAS_OP_DEFAULT_CTB_CTRL;
    BIAS_OP_OA_RES_CTRL_REG = BIAS_OP_DEFAULT_OA_RES_CTRL;
    BIAS_OP_OA_COMP_TRIM_REG = BIAS_OP_DEFAULT_OA_COMP_TRIM_REG;
}


/*******************************************************************************
* Function Name: BIAS_OP_Enable
********************************************************************************
*
* Summary:
*  Activates the hardware and begins the component operation. It is not necessary to 
*  call Enable() because the Start() API calls this function, which is the 
*  preferred method to begin the component operation.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void BIAS_OP_Enable(void)
{
    BIAS_OP_OA_RES_CTRL_REG |= BIAS_OP_internalPower | \
                                        BIAS_OP_OA_PUMP_EN;
}


/*******************************************************************************
* Function Name: BIAS_OP_Start
********************************************************************************
*
* Summary:
*  Performs all of the required initialization for the component and enables power 
*  to the block. The first time the routine is executed, the Power level, Mode, 
*  and Output mode are set. When called to restart the Opamp following a Stop() call, 
*  the current component parameter settings are retained.
*
* Parameters:
*  None
*
* Return:
*  None
*
* Global variables:
*  BIAS_OP_initVar: Used to check the initial configuration, modified
*  when this function is called for the first time.
*
*******************************************************************************/
void BIAS_OP_Start(void)
{
    if( 0u == BIAS_OP_initVar)
    {
        BIAS_OP_Init();
        BIAS_OP_initVar = 1u;
    }
    BIAS_OP_Enable();
}


/*******************************************************************************
* Function Name: BIAS_OP_Stop
********************************************************************************
*
* Summary:
*  Turn off the Opamp block.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void BIAS_OP_Stop(void)
{
    BIAS_OP_OA_RES_CTRL_REG &= ((uint32)~(BIAS_OP_OA_PWR_MODE_MASK | \
                                                   BIAS_OP_OA_PUMP_EN));
}


/*******************************************************************************
* Function Name: BIAS_OP_SetPower
********************************************************************************
*
* Summary:
*  Sets the Opamp to one of the three power levels.
*
* Parameters:
*  power: power levels.
*   BIAS_OP_LOW_POWER - Lowest active power
*   BIAS_OP_MED_POWER - Medium power
*   BIAS_OP_HIGH_POWER - Highest active power
*
* Return:
*  None
*
**********************************************************************************/
void BIAS_OP_SetPower(uint32 power)
{
    uint32 tmp;
    
    BIAS_OP_internalPower = BIAS_OP_GET_OA_PWR_MODE(power);
    tmp = BIAS_OP_OA_RES_CTRL_REG & \
           (uint32)~BIAS_OP_OA_PWR_MODE_MASK;
    BIAS_OP_OA_RES_CTRL_REG = tmp | BIAS_OP_internalPower;
}


/*******************************************************************************
* Function Name: BIAS_OP_PumpControl
********************************************************************************
*
* Summary:
*  Allows the user to turn the Opamp's boost pump on or off. By Default the Start() 
*  function turns on the pump. Use this API to turn it off. The boost must be 
*  turned on when the supply is less than 2.7 volts and off if the supply is more 
*  than 4 volts.
*
* Parameters:
*  onOff: Control the pump.
*   BIAS_OP_PUMP_OFF - Turn off the pump
*   BIAS_OP_PUMP_ON - Turn on the pump
*
* Return:
*  None
*
**********************************************************************************/
void BIAS_OP_PumpControl(uint32 onOff)
{
    
    if(0u != onOff)
    {
        BIAS_OP_OA_RES_CTRL |= BIAS_OP_OA_PUMP_EN;    
    }
    else
    {
        BIAS_OP_OA_RES_CTRL &= (uint32)~BIAS_OP_OA_PUMP_EN;
    }
}


/* [] END OF FILE */
