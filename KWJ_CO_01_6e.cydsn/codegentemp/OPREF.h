/*******************************************************************************
* File Name: OPREF.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_OPREF_H) /* Pins OPREF_H */
#define CY_PINS_OPREF_H

#include "cytypes.h"
#include "cyfitter.h"
#include "OPREF_aliases.h"


/***************************************
*     Data Struct Definitions
***************************************/

/**
* \addtogroup group_structures
* @{
*/
    
/* Structure for sleep mode support */
typedef struct
{
    uint32 pcState; /**< State of the port control register */
    uint32 sioState; /**< State of the SIO configuration */
    uint32 usbState; /**< State of the USBIO regulator */
} OPREF_BACKUP_STRUCT;

/** @} structures */


/***************************************
*        Function Prototypes             
***************************************/
/**
* \addtogroup group_general
* @{
*/
uint8   OPREF_Read(void);
void    OPREF_Write(uint8 value);
uint8   OPREF_ReadDataReg(void);
#if defined(OPREF__PC) || (CY_PSOC4_4200L) 
    void    OPREF_SetDriveMode(uint8 mode);
#endif
void    OPREF_SetInterruptMode(uint16 position, uint16 mode);
uint8   OPREF_ClearInterrupt(void);
/** @} general */

/**
* \addtogroup group_power
* @{
*/
void OPREF_Sleep(void); 
void OPREF_Wakeup(void);
/** @} power */


/***************************************
*           API Constants        
***************************************/
#if defined(OPREF__PC) || (CY_PSOC4_4200L) 
    /* Drive Modes */
    #define OPREF_DRIVE_MODE_BITS        (3)
    #define OPREF_DRIVE_MODE_IND_MASK    (0xFFFFFFFFu >> (32 - OPREF_DRIVE_MODE_BITS))

    /**
    * \addtogroup group_constants
    * @{
    */
        /** \addtogroup driveMode Drive mode constants
         * \brief Constants to be passed as "mode" parameter in the OPREF_SetDriveMode() function.
         *  @{
         */
        #define OPREF_DM_ALG_HIZ         (0x00u) /**< \brief High Impedance Analog   */
        #define OPREF_DM_DIG_HIZ         (0x01u) /**< \brief High Impedance Digital  */
        #define OPREF_DM_RES_UP          (0x02u) /**< \brief Resistive Pull Up       */
        #define OPREF_DM_RES_DWN         (0x03u) /**< \brief Resistive Pull Down     */
        #define OPREF_DM_OD_LO           (0x04u) /**< \brief Open Drain, Drives Low  */
        #define OPREF_DM_OD_HI           (0x05u) /**< \brief Open Drain, Drives High */
        #define OPREF_DM_STRONG          (0x06u) /**< \brief Strong Drive            */
        #define OPREF_DM_RES_UPDWN       (0x07u) /**< \brief Resistive Pull Up/Down  */
        /** @} driveMode */
    /** @} group_constants */
#endif

/* Digital Port Constants */
#define OPREF_MASK               OPREF__MASK
#define OPREF_SHIFT              OPREF__SHIFT
#define OPREF_WIDTH              1u

/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in OPREF_SetInterruptMode() function.
     *  @{
     */
        #define OPREF_INTR_NONE      ((uint16)(0x0000u)) /**< \brief Disabled             */
        #define OPREF_INTR_RISING    ((uint16)(0x5555u)) /**< \brief Rising edge trigger  */
        #define OPREF_INTR_FALLING   ((uint16)(0xaaaau)) /**< \brief Falling edge trigger */
        #define OPREF_INTR_BOTH      ((uint16)(0xffffu)) /**< \brief Both edge trigger    */
    /** @} intrMode */
/** @} group_constants */

/* SIO LPM definition */
#if defined(OPREF__SIO)
    #define OPREF_SIO_LPM_MASK       (0x03u)
#endif

/* USBIO definitions */
#if !defined(OPREF__PC) && (CY_PSOC4_4200L)
    #define OPREF_USBIO_ENABLE               ((uint32)0x80000000u)
    #define OPREF_USBIO_DISABLE              ((uint32)(~OPREF_USBIO_ENABLE))
    #define OPREF_USBIO_SUSPEND_SHIFT        CYFLD_USBDEVv2_USB_SUSPEND__OFFSET
    #define OPREF_USBIO_SUSPEND_DEL_SHIFT    CYFLD_USBDEVv2_USB_SUSPEND_DEL__OFFSET
    #define OPREF_USBIO_ENTER_SLEEP          ((uint32)((1u << OPREF_USBIO_SUSPEND_SHIFT) \
                                                        | (1u << OPREF_USBIO_SUSPEND_DEL_SHIFT)))
    #define OPREF_USBIO_EXIT_SLEEP_PH1       ((uint32)~((uint32)(1u << OPREF_USBIO_SUSPEND_SHIFT)))
    #define OPREF_USBIO_EXIT_SLEEP_PH2       ((uint32)~((uint32)(1u << OPREF_USBIO_SUSPEND_DEL_SHIFT)))
    #define OPREF_USBIO_CR1_OFF              ((uint32)0xfffffffeu)
#endif


/***************************************
*             Registers        
***************************************/
/* Main Port Registers */
#if defined(OPREF__PC)
    /* Port Configuration */
    #define OPREF_PC                 (* (reg32 *) OPREF__PC)
#endif
/* Pin State */
#define OPREF_PS                     (* (reg32 *) OPREF__PS)
/* Data Register */
#define OPREF_DR                     (* (reg32 *) OPREF__DR)
/* Input Buffer Disable Override */
#define OPREF_INP_DIS                (* (reg32 *) OPREF__PC2)

/* Interrupt configuration Registers */
#define OPREF_INTCFG                 (* (reg32 *) OPREF__INTCFG)
#define OPREF_INTSTAT                (* (reg32 *) OPREF__INTSTAT)

/* "Interrupt cause" register for Combined Port Interrupt (AllPortInt) in GSRef component */
#if defined (CYREG_GPIO_INTR_CAUSE)
    #define OPREF_INTR_CAUSE         (* (reg32 *) CYREG_GPIO_INTR_CAUSE)
#endif

/* SIO register */
#if defined(OPREF__SIO)
    #define OPREF_SIO_REG            (* (reg32 *) OPREF__SIO)
#endif /* (OPREF__SIO_CFG) */

/* USBIO registers */
#if !defined(OPREF__PC) && (CY_PSOC4_4200L)
    #define OPREF_USB_POWER_REG       (* (reg32 *) CYREG_USBDEVv2_USB_POWER_CTRL)
    #define OPREF_CR1_REG             (* (reg32 *) CYREG_USBDEVv2_CR1)
    #define OPREF_USBIO_CTRL_REG      (* (reg32 *) CYREG_USBDEVv2_USB_USBIO_CTRL)
#endif    
    
    
/***************************************
* The following code is DEPRECATED and 
* must not be used in new designs.
***************************************/
/**
* \addtogroup group_deprecated
* @{
*/
#define OPREF_DRIVE_MODE_SHIFT       (0x00u)
#define OPREF_DRIVE_MODE_MASK        (0x07u << OPREF_DRIVE_MODE_SHIFT)
/** @} deprecated */

#endif /* End Pins OPREF_H */


/* [] END OF FILE */
