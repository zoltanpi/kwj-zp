/*******************************************************************************
* File Name: SMPLBAT.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_SMPLBAT_H) /* Pins SMPLBAT_H */
#define CY_PINS_SMPLBAT_H

#include "cytypes.h"
#include "cyfitter.h"
#include "SMPLBAT_aliases.h"


/***************************************
*     Data Struct Definitions
***************************************/

/**
* \addtogroup group_structures
* @{
*/
    
/* Structure for sleep mode support */
typedef struct
{
    uint32 pcState; /**< State of the port control register */
    uint32 sioState; /**< State of the SIO configuration */
    uint32 usbState; /**< State of the USBIO regulator */
} SMPLBAT_BACKUP_STRUCT;

/** @} structures */


/***************************************
*        Function Prototypes             
***************************************/
/**
* \addtogroup group_general
* @{
*/
uint8   SMPLBAT_Read(void);
void    SMPLBAT_Write(uint8 value);
uint8   SMPLBAT_ReadDataReg(void);
#if defined(SMPLBAT__PC) || (CY_PSOC4_4200L) 
    void    SMPLBAT_SetDriveMode(uint8 mode);
#endif
void    SMPLBAT_SetInterruptMode(uint16 position, uint16 mode);
uint8   SMPLBAT_ClearInterrupt(void);
/** @} general */

/**
* \addtogroup group_power
* @{
*/
void SMPLBAT_Sleep(void); 
void SMPLBAT_Wakeup(void);
/** @} power */


/***************************************
*           API Constants        
***************************************/
#if defined(SMPLBAT__PC) || (CY_PSOC4_4200L) 
    /* Drive Modes */
    #define SMPLBAT_DRIVE_MODE_BITS        (3)
    #define SMPLBAT_DRIVE_MODE_IND_MASK    (0xFFFFFFFFu >> (32 - SMPLBAT_DRIVE_MODE_BITS))

    /**
    * \addtogroup group_constants
    * @{
    */
        /** \addtogroup driveMode Drive mode constants
         * \brief Constants to be passed as "mode" parameter in the SMPLBAT_SetDriveMode() function.
         *  @{
         */
        #define SMPLBAT_DM_ALG_HIZ         (0x00u) /**< \brief High Impedance Analog   */
        #define SMPLBAT_DM_DIG_HIZ         (0x01u) /**< \brief High Impedance Digital  */
        #define SMPLBAT_DM_RES_UP          (0x02u) /**< \brief Resistive Pull Up       */
        #define SMPLBAT_DM_RES_DWN         (0x03u) /**< \brief Resistive Pull Down     */
        #define SMPLBAT_DM_OD_LO           (0x04u) /**< \brief Open Drain, Drives Low  */
        #define SMPLBAT_DM_OD_HI           (0x05u) /**< \brief Open Drain, Drives High */
        #define SMPLBAT_DM_STRONG          (0x06u) /**< \brief Strong Drive            */
        #define SMPLBAT_DM_RES_UPDWN       (0x07u) /**< \brief Resistive Pull Up/Down  */
        /** @} driveMode */
    /** @} group_constants */
#endif

/* Digital Port Constants */
#define SMPLBAT_MASK               SMPLBAT__MASK
#define SMPLBAT_SHIFT              SMPLBAT__SHIFT
#define SMPLBAT_WIDTH              1u

/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in SMPLBAT_SetInterruptMode() function.
     *  @{
     */
        #define SMPLBAT_INTR_NONE      ((uint16)(0x0000u)) /**< \brief Disabled             */
        #define SMPLBAT_INTR_RISING    ((uint16)(0x5555u)) /**< \brief Rising edge trigger  */
        #define SMPLBAT_INTR_FALLING   ((uint16)(0xaaaau)) /**< \brief Falling edge trigger */
        #define SMPLBAT_INTR_BOTH      ((uint16)(0xffffu)) /**< \brief Both edge trigger    */
    /** @} intrMode */
/** @} group_constants */

/* SIO LPM definition */
#if defined(SMPLBAT__SIO)
    #define SMPLBAT_SIO_LPM_MASK       (0x03u)
#endif

/* USBIO definitions */
#if !defined(SMPLBAT__PC) && (CY_PSOC4_4200L)
    #define SMPLBAT_USBIO_ENABLE               ((uint32)0x80000000u)
    #define SMPLBAT_USBIO_DISABLE              ((uint32)(~SMPLBAT_USBIO_ENABLE))
    #define SMPLBAT_USBIO_SUSPEND_SHIFT        CYFLD_USBDEVv2_USB_SUSPEND__OFFSET
    #define SMPLBAT_USBIO_SUSPEND_DEL_SHIFT    CYFLD_USBDEVv2_USB_SUSPEND_DEL__OFFSET
    #define SMPLBAT_USBIO_ENTER_SLEEP          ((uint32)((1u << SMPLBAT_USBIO_SUSPEND_SHIFT) \
                                                        | (1u << SMPLBAT_USBIO_SUSPEND_DEL_SHIFT)))
    #define SMPLBAT_USBIO_EXIT_SLEEP_PH1       ((uint32)~((uint32)(1u << SMPLBAT_USBIO_SUSPEND_SHIFT)))
    #define SMPLBAT_USBIO_EXIT_SLEEP_PH2       ((uint32)~((uint32)(1u << SMPLBAT_USBIO_SUSPEND_DEL_SHIFT)))
    #define SMPLBAT_USBIO_CR1_OFF              ((uint32)0xfffffffeu)
#endif


/***************************************
*             Registers        
***************************************/
/* Main Port Registers */
#if defined(SMPLBAT__PC)
    /* Port Configuration */
    #define SMPLBAT_PC                 (* (reg32 *) SMPLBAT__PC)
#endif
/* Pin State */
#define SMPLBAT_PS                     (* (reg32 *) SMPLBAT__PS)
/* Data Register */
#define SMPLBAT_DR                     (* (reg32 *) SMPLBAT__DR)
/* Input Buffer Disable Override */
#define SMPLBAT_INP_DIS                (* (reg32 *) SMPLBAT__PC2)

/* Interrupt configuration Registers */
#define SMPLBAT_INTCFG                 (* (reg32 *) SMPLBAT__INTCFG)
#define SMPLBAT_INTSTAT                (* (reg32 *) SMPLBAT__INTSTAT)

/* "Interrupt cause" register for Combined Port Interrupt (AllPortInt) in GSRef component */
#if defined (CYREG_GPIO_INTR_CAUSE)
    #define SMPLBAT_INTR_CAUSE         (* (reg32 *) CYREG_GPIO_INTR_CAUSE)
#endif

/* SIO register */
#if defined(SMPLBAT__SIO)
    #define SMPLBAT_SIO_REG            (* (reg32 *) SMPLBAT__SIO)
#endif /* (SMPLBAT__SIO_CFG) */

/* USBIO registers */
#if !defined(SMPLBAT__PC) && (CY_PSOC4_4200L)
    #define SMPLBAT_USB_POWER_REG       (* (reg32 *) CYREG_USBDEVv2_USB_POWER_CTRL)
    #define SMPLBAT_CR1_REG             (* (reg32 *) CYREG_USBDEVv2_CR1)
    #define SMPLBAT_USBIO_CTRL_REG      (* (reg32 *) CYREG_USBDEVv2_USB_USBIO_CTRL)
#endif    
    
    
/***************************************
* The following code is DEPRECATED and 
* must not be used in new designs.
***************************************/
/**
* \addtogroup group_deprecated
* @{
*/
#define SMPLBAT_DRIVE_MODE_SHIFT       (0x00u)
#define SMPLBAT_DRIVE_MODE_MASK        (0x07u << SMPLBAT_DRIVE_MODE_SHIFT)
/** @} deprecated */

#endif /* End Pins SMPLBAT_H */


/* [] END OF FILE */
