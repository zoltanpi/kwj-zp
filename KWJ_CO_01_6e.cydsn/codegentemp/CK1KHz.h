/*******************************************************************************
* File Name: CK1KHz.h
* Version 2.20
*
*  Description:
*   Provides the function and constant definitions for the clock component.
*
*  Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_CLOCK_CK1KHz_H)
#define CY_CLOCK_CK1KHz_H

#include <cytypes.h>
#include <cyfitter.h>


/***************************************
*        Function Prototypes
***************************************/
#if defined CYREG_PERI_DIV_CMD

void CK1KHz_StartEx(uint32 alignClkDiv);
#define CK1KHz_Start() \
    CK1KHz_StartEx(CK1KHz__PA_DIV_ID)

#else

void CK1KHz_Start(void);

#endif/* CYREG_PERI_DIV_CMD */

void CK1KHz_Stop(void);

void CK1KHz_SetFractionalDividerRegister(uint16 clkDivider, uint8 clkFractional);

uint16 CK1KHz_GetDividerRegister(void);
uint8  CK1KHz_GetFractionalDividerRegister(void);

#define CK1KHz_Enable()                         CK1KHz_Start()
#define CK1KHz_Disable()                        CK1KHz_Stop()
#define CK1KHz_SetDividerRegister(clkDivider, reset)  \
    CK1KHz_SetFractionalDividerRegister((clkDivider), 0u)
#define CK1KHz_SetDivider(clkDivider)           CK1KHz_SetDividerRegister((clkDivider), 1u)
#define CK1KHz_SetDividerValue(clkDivider)      CK1KHz_SetDividerRegister((clkDivider) - 1u, 1u)


/***************************************
*             Registers
***************************************/
#if defined CYREG_PERI_DIV_CMD

#define CK1KHz_DIV_ID     CK1KHz__DIV_ID

#define CK1KHz_CMD_REG    (*(reg32 *)CYREG_PERI_DIV_CMD)
#define CK1KHz_CTRL_REG   (*(reg32 *)CK1KHz__CTRL_REGISTER)
#define CK1KHz_DIV_REG    (*(reg32 *)CK1KHz__DIV_REGISTER)

#define CK1KHz_CMD_DIV_SHIFT          (0u)
#define CK1KHz_CMD_PA_DIV_SHIFT       (8u)
#define CK1KHz_CMD_DISABLE_SHIFT      (30u)
#define CK1KHz_CMD_ENABLE_SHIFT       (31u)

#define CK1KHz_CMD_DISABLE_MASK       ((uint32)((uint32)1u << CK1KHz_CMD_DISABLE_SHIFT))
#define CK1KHz_CMD_ENABLE_MASK        ((uint32)((uint32)1u << CK1KHz_CMD_ENABLE_SHIFT))

#define CK1KHz_DIV_FRAC_MASK  (0x000000F8u)
#define CK1KHz_DIV_FRAC_SHIFT (3u)
#define CK1KHz_DIV_INT_MASK   (0xFFFFFF00u)
#define CK1KHz_DIV_INT_SHIFT  (8u)

#else 

#define CK1KHz_DIV_REG        (*(reg32 *)CK1KHz__REGISTER)
#define CK1KHz_ENABLE_REG     CK1KHz_DIV_REG
#define CK1KHz_DIV_FRAC_MASK  CK1KHz__FRAC_MASK
#define CK1KHz_DIV_FRAC_SHIFT (16u)
#define CK1KHz_DIV_INT_MASK   CK1KHz__DIVIDER_MASK
#define CK1KHz_DIV_INT_SHIFT  (0u)

#endif/* CYREG_PERI_DIV_CMD */

#endif /* !defined(CY_CLOCK_CK1KHz_H) */

/* [] END OF FILE */
