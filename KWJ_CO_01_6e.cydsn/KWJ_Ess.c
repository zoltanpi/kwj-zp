/* ========================================
 * File Name: KWJ_Ess.c
 *
 * Version: 1.0
 *
 * Description:
 * This file implements the BLE service for Enviromental Sensors
 *
 * Hardware Dependency:
 * CYBLE-014008
 *
 * Copyright ODI, 2016
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF ODI.
 * RCRegna - Author
 * ========================================
*/
#include "project.h"
#include "main.h"
#include "KWJ_BLE_Process.h"

// I2C Parameters
static uint16 FULL_SCL = 65535;      // Sensor Full Scale Range
static uint16 P_RES = 4096;          // P - Sensor Full LSB Resolution
//static uint32 I2C_Byte;              // I2C Return Byte

/******************************************************************************
* Function Name: Init_Ess_DB
*******************************************************************************
* Summary:
*  Initializes the SAV_DB Environmental entries with factory defaults
*
******************************************************************************/
void Init_Ess_DB(void)
{
    rstFlag(ESS_H_NOTIFICATION);
    rstFlag(ESS_P_NOTIFICATION);
    rstFlag(ESS_T_NOTIFICATION);
    
        // Temperature Factory Default
    if(false == (SAV_DB.CAL_DONE & T_OS_CAL))
        SAV_DB.T_OS   = -46.85;                // Temperature Calc Scaler Offset
    if(false == (SAV_DB.CAL_DONE & T_GAIN_CAL))
        SAV_DB.T_GAIN =  175.72;               // Temperature Calc Scaler
        
        // Rel Humidity Factory Default
    if(false == (SAV_DB.CAL_DONE & RH_OS_CAL))
        SAV_DB.RH_OS   = -6.0;                // Rel Humidity Calc Scaler Offset
    if(false == (SAV_DB.CAL_DONE & RH_GAIN_CAL))
        SAV_DB.RH_GAIN =  125.0;               // Rel Humidity Calc Scaler
        
        // Air Pressure Factory Default
    if(false == (SAV_DB.CAL_DONE & P_OS_CAL))
        SAV_DB.P_OS   = 0.0;                   // Pressure Calc Scaler Offset
    if(false == (SAV_DB.CAL_DONE & P_GAIN_CAL))
        SAV_DB.P_GAIN = 1.0;                   // Pressure Calc Scaler
}
/*********************************************
* Helper function getTRH()
* Communicates with T/RH sensor and returns the RAW reading
*************************************************************************/
uint16 getTRH(uint32 requestCmd)
{
    uint32 packet = 0u;
    uint32 result = NO_I2C_ERR;
    
    SCB_ClearPendingInt();
    uint8 i;
    for(i = 0; i < 3; i++) //lockup prevention
    {
        switch(i)
        {
            case 0:
                // Enable and allow 15ms settling
                result = SCB_I2CMasterSendStart(T_RH_ADR, SCB_I2C_WRITE_XFER_MODE);
            break;
            case 1:
                result = SCB_I2CMasterWriteByte(requestCmd);
            break;
            case 2:
                result = SCB_I2CMasterSendRestart(T_RH_ADR, SCB_I2C_READ_XFER_MODE);
                packet = SCB_I2CMasterReadByte(0u);
                packet <<= 8;
                packet |= SCB_I2CMasterReadByte(1u);
            break;
        }
        if(result != NO_I2C_ERR)
        {
            break;
        }
    }
    SCB_I2CMasterSendStop();
    if(result != NO_I2C_ERR)
    {
        SCB_ClearPendingInt();
        packet = 0xFFFF; // failed
    }
    
    return packet;
}
/******************************************************************************
* Function Name: Make_T_Measurement
*******************************************************************************
* Summary:
*  A request for a Measurement of Temperature is made through its I2C interface
*
* Parameters:
*  None
*
* Return:
*  NOW_DB.T_MEAS, an 16 bit signed integer ranged -32768 to +32767 degree F
*
******************************************************************************/
int16   Make_T_Measurement()
{
    
    NOW_DB.T_RAW = getTRH(T_REQ);
    uint16 tmp = NOW_DB.T_RAW;
    // Normalize Temperature Reading
    if(tmp != 0xffff) // good range
    {
        NOW_DB.T_MEAS = (int16)((float)NOW_DB.T_RAW * SAV_DB.T_GAIN/(float)FULL_SCL + SAV_DB.T_OS);
    
        if(false == chkFlag( STAND_ALONE_MODE))
        {
            if((CYBLE_ERROR_OK == 
                GattSetCharVal(ESS_TEMPERATURE, SIZE_2_BYTES,
                VarToCharArray((uint16*)&NOW_DB.T_MEAS,SIZE_2_BYTES))) &&
            (CYBLE_ERROR_OK == 
                GattSetCharVal(T_RAW_INDEX,  SIZE_2_BYTES,
                VarToCharArray((uint16*)&NOW_DB.T_RAW,SIZE_2_BYTES))) &&
            (SAV_DB.T_RAW != NOW_DB.T_RAW))
            {
                setFlag( ESS_T_NOTIFICATION);
            }
        }
        
        SAV_DB.T_RAW = NOW_DB.T_RAW;
    }
    return NOW_DB.T_MEAS;
}
/******************************************************************************
* Function Name: Make_RH_Measurement
*******************************************************************************
* Summary:
*  A request for a Measure of Rel Humidity is made through its I2C interface
*
* Parameters:
*  None
*
* Return:
*  the_RH, an 16 bit integer in the range of 0 to 100%
*
******************************************************************************/
uint16  Make_RH_Measurement()
{
    
    NOW_DB.RH_RAW = getTRH(RH_REQ);

    uint16 tmp = NOW_DB.RH_RAW;
    // Normalize Relative Humidity Reading
    if(tmp != 0xffff) // good range
    {
        NOW_DB.RH_MEAS = (uint16)((float)NOW_DB.RH_RAW * SAV_DB.RH_GAIN/(float)FULL_SCL + SAV_DB.RH_OS);
        
        if(NOW_DB.RH_MEAS > 100) NOW_DB.RH_MEAS = 100;
    
        if(false == chkFlag( STAND_ALONE_MODE))
        {
            if((CYBLE_ERROR_OK == 
                GattSetCharVal(ESS_HUMIDITY, SIZE_2_BYTES,
                VarToCharArray((uint16*)&NOW_DB.RH_MEAS,SIZE_2_BYTES))) &&
            (CYBLE_ERROR_OK == 
                GattSetCharVal(RH_RAW_INDEX, SIZE_2_BYTES,
                VarToCharArray((uint16*)&NOW_DB.RH_RAW,SIZE_2_BYTES))) &&
            (SAV_DB.RH_RAW != NOW_DB.RH_RAW))
            {
                setFlag( ESS_H_NOTIFICATION);
            }
        }
        
        SAV_DB.RH_RAW = NOW_DB.RH_RAW;
    }
    
    return NOW_DB.RH_RAW;
}
/******************************************************************************
* Function Name: Make_P_Measurement
*******************************************************************************
* Summary:
*  A request for a Measurement of Pressure is made through its I2C interface
*
* Parameters:
*  None
*
* Return:
*  the_P
*
******************************************************************************/
uint32 Make_P_Measurement()
{
    uint32 result = NO_I2C_ERR;
    uint32 tmp = 0;
    uint8 i;
    for(i = 0; i < 6; i++) //lockup prevention
    {
        switch(i)
        {
            case 0:
                // Enable and allow 15ms settling
                result = SCB_I2CMasterSendStart(P_ADR, SCB_I2C_WRITE_XFER_MODE);
            break;
            case 1:
                result = SCB_I2CMasterWriteByte(P_CTR1);
            break;
            case 2:
                result = SCB_I2CMasterWriteByte(P_ENA);
                SCB_I2CMasterSendStop();
                CyDelay(15u); // Provide 15mS conversion time
            break;
            case 3:
                // request pressure measurement
                result = SCB_I2CMasterSendStart(P_ADR, SCB_I2C_WRITE_XFER_MODE);
            break;
            case 4:
                result = SCB_I2CMasterWriteByte(P_DAT);
            break;
            case 5:
                result = SCB_I2CMasterSendRestart(P_ADR, SCB_I2C_READ_XFER_MODE);
                tmp = SCB_I2CMasterReadByte(0u); // XL Byte
                tmp <<= 8;
                tmp |= SCB_I2CMasterReadByte(0u); // L Byte
                tmp <<= 8;
                tmp |= SCB_I2CMasterReadByte(1u); // M Byte
            break;
        }
        if(result != NO_I2C_ERR)
        {
            break;
        }
    }

    SCB_I2CMasterSendStop();
    if(result != NO_I2C_ERR)
    {
        SCB_ClearPendingInt();
        NOW_DB.P_RAW = 0x00FFFFFF; // failed
    }
    else
    {
        // XL.L.M -> M.L.XL
        NOW_DB.P_RAW = tmp & 0xff;
        NOW_DB.P_RAW <<= 8;
        tmp >>= 8;
        NOW_DB.P_RAW |= tmp & 0xff;
        NOW_DB.P_RAW <<= 8;
        tmp >>= 8;
        NOW_DB.P_RAW |= tmp & 0xff;
    }
   
    // Normalize Pressure Reading
    NOW_DB.P_MEAS = (uint32)((float)NOW_DB.P_RAW * SAV_DB.P_GAIN/(float)P_RES + SAV_DB.P_OS);
    
    if(false == chkFlag( STAND_ALONE_MODE))
    {
        if((CYBLE_ERROR_OK == 
            GattSetCharVal(ESS_PRESSURE, SIZE_4_BYTES, 
            VarToCharArray((uint32*)&NOW_DB.P_MEAS,SIZE_4_BYTES))) &&
        (CYBLE_ERROR_OK == 
            GattSetCharVal(P_RAW_INDEX, SIZE_3_BYTES, 
            VarToCharArray((uint32*)&NOW_DB.P_RAW,SIZE_4_BYTES))) &&
        (SAV_DB.P_RAW != NOW_DB.P_RAW))
        {
            setFlag( ESS_P_NOTIFICATION);
        }
    }
    
    SAV_DB.P_RAW = NOW_DB.P_RAW;
    return NOW_DB.P_MEAS;
}

/*****************************************************************************
* Function Name: EssEventHandler
******************************************************************************
* Summary:
* Event handler for the Enviromental Service specific events.
*
* Params:
*  *wrReqParam - Enviromental Characteristic Value GATT DB Index
*
* Return:
*  None
*
*****************************************************************************/
void   EssEventHandler(CYBLE_GATTS_WRITE_REQ_PARAM_T *wrReqParam)
{
    /* Event handler switch statement for the ESS service specific events. */
    uint16  index = wrReqParam->handleValPair.attrHandle;
    //uint8 * value = wrReqParam->handleValPair.value.val;

    switch(index)
	{

        case ESS_SERVICE_HANDLE:
          /* Request from App to send Environmental Data */
        setFlag( ESS_T_NOTIFICATION);
        setFlag( ESS_P_NOTIFICATION);
        setFlag( ESS_H_NOTIFICATION);
        SendEssOverBLE();
    // ------------------add code if needed-------------------------
        break;
		
		default:
    // ------------------add code if needed-------------------------
        break;
	}
}
/*****************************************************************************
* Function Name: SendEssOverBLE
******************************************************************************
* Summary:
* Sends the Environment Measurement characteristic notification 
* packet.
*
*****************************************************************************/
void SendEssOverBLE(void)
{
	if(chkFlag( ESS_T_NOTIFICATION))
	{
    	uint8 essMeasPacket[SIZE_2_BYTES];

        /* Read the existing characteristic value into a 
         * two-byte array, by using CyBle_EsssGetCharacteristicValue(). 
         */
        CyBle_EsssGetCharacteristicValue(CYBLE_ESS_TEMPERATURE,
            CHARACTERISTIC_INSTANCE_1, SIZE_2_BYTES, essMeasPacket);
        
        /* Update the packet with the actual temperature value. */
        essMeasPacket[0] = NOW_DB.T_MEAS;
        essMeasPacket[1] = NOW_DB.T_MEAS >> 8;
		        
        /* Call the BLE component API to send notification */
		CyBle_EsssSendNotification(cyBle_connHandle, CYBLE_ESS_TEMPERATURE,
            CHARACTERISTIC_INSTANCE_1, SIZE_2_BYTES, essMeasPacket);
 
        rstFlag(ESS_T_NOTIFICATION);
    }
	if(chkFlag( ESS_P_NOTIFICATION))
	{
    	uint8 essMeasPacket[SIZE_4_BYTES];

        /* Read the existing characteristic value into a 
         * four-byte array, by using CyBle_EsssGetCharacteristicValue(). 
         */
        CyBle_EsssGetCharacteristicValue(CYBLE_ESS_PRESSURE,
            CHARACTERISTIC_INSTANCE_1, SIZE_4_BYTES, essMeasPacket);
        
        /* Update the packet with the actual pressure value. */
        essMeasPacket[0] = NOW_DB.P_MEAS;
        essMeasPacket[1] = NOW_DB.P_MEAS >> 8;
        essMeasPacket[2] = NOW_DB.P_MEAS >> 16;
        essMeasPacket[3] = NOW_DB.P_MEAS >> 24;
		        
        /* Call the BLE component API to send notification */
		CyBle_EsssSendNotification(cyBle_connHandle, CYBLE_ESS_PRESSURE,
            CHARACTERISTIC_INSTANCE_1, SIZE_4_BYTES, essMeasPacket);
 
        rstFlag(ESS_P_NOTIFICATION);
    }
	if(chkFlag( ESS_H_NOTIFICATION))
	{
    	uint8 essMeasPacket[SIZE_2_BYTES];

        /* Read the existing characteristic value into a 
         * two-byte array, by using CyBle_EsssGetCharacteristicValue(). 
         */
        CyBle_EsssGetCharacteristicValue(CYBLE_ESS_HUMIDITY,
            CHARACTERISTIC_INSTANCE_1, SIZE_2_BYTES, essMeasPacket);
        
        /* Update the packet with the actual humidity value. */
        essMeasPacket[0] = NOW_DB.RH_MEAS;
        essMeasPacket[1] = NOW_DB.RH_MEAS >> 8;
		        
        /* Call the BLE component API to send notification */
		CyBle_EsssSendNotification(cyBle_connHandle, CYBLE_ESS_HUMIDITY,
            CHARACTERISTIC_INSTANCE_1, SIZE_2_BYTES, essMeasPacket);
 
        rstFlag(ESS_H_NOTIFICATION);
    }
}

/* [] END OF FILE */
