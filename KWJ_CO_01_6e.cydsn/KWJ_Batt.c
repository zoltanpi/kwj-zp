/* ========================================
 * File Name: KWJ_Batt.c
 *
 * Version: 1.0
 *
 * Description:
 * This file implements the BLE service for Battery Info
 *
 * Hardware Dependency:
 * CYBLE-014008
 *
 * Copyright ODI, 2016
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF ODI.
 * RCRegna - Author
 * ========================================
*/
#include "project.h"
#include "main.h"
#include "KWJ_BLE_Process.h"

/******************************************************************************
* Function Name: Init_Bat_DB
*******************************************************************************
* Summary:
*  Initializes the SAV_DB Battery entries with factory defaults
*
* Parameters:
*  None
*
* Return:
*  None
*
******************************************************************************/
void Init_Bat_DB(void)
{
    rstFlag(BAS_NOTIFICATION);

    /* Factory Defaults for the Battery parameters */
    if(false == (SAV_DB.CAL_DONE & BAT_OS_CAL))
        SAV_DB.BAT_OS   = -858.0;                  // Bat Offset @2.8V = 0%
    if(false == (SAV_DB.CAL_DONE & BAT_GAIN_CAL))
        SAV_DB.BAT_GAIN =  0.114025;               // Bat Gain @4.6V = 100%
    if(false == (SAV_DB.CAL_DONE & BAT_LO_CAL))
        BAT_LO   = 10u;                            // Battery Low Threshold
    if(false == (SAV_DB.CAL_DONE & BAT_CRIT_CAL))
        BAT_CRIT = 3u;                             // Battery Critical Threshold
}
/******************************************************************************
* Function Name: Make_BAT_Measurement
*******************************************************************************
* Summary:
*  A 12-Bit Measurement of the Sampled Battery voltage occurs
*  Offset and Gain Factors are applied to determine the reading in % Charge
*  w/ 0% = 2.8V and 100% = 4.3V
*
* Parameters:
*  None
*
* Return:
*  true  = safe range
*  false = lo voltage
*
******************************************************************************/
bool Make_BAT_Measurement()
{
    SMPLBAT_Write(1);
    ADC_SAR_12Bit_StartConvert();
    ADC_SAR_12Bit_IsEndConversion(ADC_SAR_12Bit_WAIT_FOR_RESULT);
    SMPLBAT_Write(0);
    ADC_SAR_12Bit_StopConvert();
    NOW_DB.BAT_RAW = (int16)ADC_SAR_12Bit_GetResult16(2u);
    
     NOW_DB.BAT_MEAS = (uint16)(((float)NOW_DB.BAT_RAW + SAV_DB.BAT_OS) * SAV_DB.BAT_GAIN);
   
    if(NOW_DB.BAT_MEAS > 100)
       NOW_DB.BAT_MEAS = 100;
    
    uint8 the_bat = NOW_DB.BAT_MEAS;
    
    if(false == chkFlag( STAND_ALONE_MODE))
    {
        if((CYBLE_ERROR_OK == 
            CyBle_BassSetCharacteristicValue(CYBLE_BATTERY_SERVICE_INDEX,
            CYBLE_BAS_BATTERY_LEVEL, 0x01u, &the_bat)) &&
           (CYBLE_ERROR_OK == 
            GattSetCharVal(BAT_RAW_INDEX, 
            0x02u, VarToCharArray((int16*)&NOW_DB.BAT_RAW,SIZE_2_BYTES))) &&
        (SAV_DB.BAT_MEAS != NOW_DB.BAT_MEAS))
        {
            setFlag( BAS_NOTIFICATION);
        }
    }
    SAV_DB.BAT_RAW = NOW_DB.BAT_RAW;
    SAV_DB.BAT_MEAS = NOW_DB.BAT_MEAS;
    if(NOW_DB.BAT_MEAS > BAT_LO)
    {   // Reset bat lo flags:
        rstFlag(LOW_BATTERY);
        rstFlag(CRITICAL_BATTERY);
        return true;
    }
    else
    {   // set lo bat flag:
        setFlag( LOW_BATTERY);
        if(NOW_DB.BAT_MEAS <= BAT_CRIT)
        {   // set critical flag in addition:
            setFlag( CRITICAL_BATTERY);
        }
        return false;
    }
}

/*****************************************************************************
* Function Name: BasEventHandler
******************************************************************************
* Summary:
* Event handler for the Battery Service specific events.
*
* Params:
*  *wrReqParam - Battery Characteristic Value GATT DB Index
*
* Return:
*  None
*
*****************************************************************************/
void BasEventHandler(CYBLE_GATTS_WRITE_REQ_PARAM_T *wrReqParam)
{
    /* Event handler switch statement for the BAS service specific events. */
    uint16  index = wrReqParam->handleValPair.attrHandle;
    uint8 * value = wrReqParam->handleValPair.value.val;

    switch(index)
	{
        case BAS_SERVICE_HANDLE: /*< Handle of Battery Service */
        SendBasOverBLE();
		    break;

        case BAS_BATTERY_LEVEL: /*< Handle of the Battery Level characteristic */ 
        Make_BAT_Measurement();
    // ------------------add code if needed-------------------------
	    	break;

        case BAS_BATTERY_CPFD: /*< Handle of the Characteristic Presentation Format descriptor */ 
    // ------------------add code if needed-------------------------
        break;

        case BAS_BATTERY_CCCD: /*< Handle of the Client Characteristic Configuration descriptor */ 
    // ------------------add code if needed-------------------------
        break;
		
		default:
    // ------------------add code if needed-------------------------
        break;
	}
}
/*****************************************************************************
* Function Name: SendBasOverBLE
******************************************************************************
* Summary:
* Sends the Battery Measurement characteristic notification 
* packet.
*
* Parameters:
* None
*
* Return:
* None
*
* Side Effects:
* None
*
*****************************************************************************/
void SendBasOverBLE(void)
{
    /* Two byte packet for battery characteristic value
     * measurement value is 32-bit Maximum
     */
	uint8 basMeasPacket[1];
	
	if(chkFlag( BAS_NOTIFICATION))
	{
        /* Read the existing characteristic value into a 
         * one-byte array, by using CyBle_BassGetCharacteristicValue(). 
         */
        CyBle_BassGetCharacteristicValue(CYBLE_BATTERY_SERVICE_INDEX,
           CYBLE_BAS_BATTERY_LEVEL, CYBLE_BAS_BATTERY_LEVEL_LEN, basMeasPacket);
        
        /* Update the byte with the actual battery level value. */
        basMeasPacket[0] = NOW_DB.BAT_MEAS;
		        
       /* Call the BLE component API to send notification */
	   CyBle_BassSendNotification(cyBle_connHandle, CYBLE_BATTERY_SERVICE_INDEX,
           CYBLE_BAS_BATTERY_LEVEL, CYBLE_BAS_BATTERY_LEVEL_LEN, basMeasPacket);
 
        rstFlag(BAS_NOTIFICATION);
    }
}

/* [] END OF FILE */
