/* ========================================
 * File Name: KWJ_BLE_Process.c
 *
 * Version: 1.0
 *
 * Description:
 * This file implements the BLE capability in the KWJ Otterbox System
 *
 * Hardware Dependency:
 * CYBLE-014008
 *
 * Copyright ODI, 2016
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF ODI.
 * RCRegna - Author
 * ========================================
*/
/*****************************************************************************
* Included headers
*****************************************************************************/
#include <project.h>
#include "main.h"
#include "KWJ_BLE_Process.h"

void DisconnectBLE(void)
{
    if(false == chkFlag(AUTO_CONNECT_MODE) && chkFlag(MEASUREMENT_INTERVAL))
    {
        if(--BLE_RETRY == 0)
        {
          /* Sets the STAND_ALONE_MODE flag to put system in Stand Alone mode */
            SAmode();
            HandleBZR(BZR_ALERT, true);
        }
    }
    else
    {
        CyBle_GappStartAdvertisement(CYBLE_ADVERTISING_FAST);			
    }
} // DisconnectBLE()

/*****************************************************************************
* Function Name: ConnectionEventHandler
******************************************************************************
* Summary:
* Event handler for generic Connection BLE events.
*
* Parameters:
* event: An enumerated value to be checked and accordingly some action to 
*        be taken. The list of events is defined in the CYBLE_EVENT_T enum.
*
* eventParam: The parameter associated with the event. The type of parameter
*             can vary depending on the event.
*
* Return:
* None
*
* Theory:
* The function implements a switch case to handle different events for BLE
* advertisement, connection, disconnection and service handling.
*
* Side Effects:
* None
*
*****************************************************************************/
void ConnectionEventHandler(uint32 event, void *eventParam)
{
    CYBLE_GATTS_WRITE_REQ_PARAM_T *wrReqParam;

    /* Handle various events for a general BLE connection */
      switch(event)
    {
        
        /* This event is received when component is Started */
        case CYBLE_EVT_STACK_ON: 					
          /* Start Advertisement and enter Discoverable mode*/
            CyBle_GappStartAdvertisement(CYBLE_ADVERTISING_FAST);			
        break;
              
        /* This event is received when device is disconnected or advetising times out*/
        case CYBLE_EVT_GAP_DEVICE_DISCONNECTED:
            DisconnectBLE();
            
        break;
            
        /* This event is received when event times out*/
        case CYBLE_EVT_TIMEOUT: 
            DisconnectBLE();
            
        break;
        
        /* This event is received when connection is established */
        case CYBLE_EVT_GATT_CONNECT_IND:
            /* Retrieve BLE connection handle */
            cyBle_connHandle = *(CYBLE_CONN_HANDLE_T *) eventParam;
            CyBle_GappStopAdvertisement();
            if(chkFlag(NEW_GATT_DB))
            {
                initGATT();
                rstFlag(NEW_GATT_DB);
            }           
            BLE_RETRY = NUM_RETRY_BLE;
        break;
            
        case CYBLE_EVT_L2CAP_CONN_PARAM_UPDATE_RSP:
            if(*(uint8 *)eventParam == 0x00)
            {
                /* Connection parameter update request accepted by Master */
                rstFlag(CHANGE_BLE);
            }
            else if(*(uint8 *)eventParam == 0x01)
            {
                /* Connection parameter update request rejected by Master */
            }
            break;
    /** 'GATT MTU Exchange Request' received from GATT client device. Event parameter 
       contains the MTU size of type CYBLE_GATT_XCHG_MTU_PARAM_T. */
        case CYBLE_EVT_GATTS_XCNHG_MTU_REQ:
            
        break;
            
        case CYBLE_EVT_GATTS_READ_CHAR_VAL_ACCESS_REQ:
//            setFlag(MEASUREMENT_INTERVAL);
        break;
            
        case CYBLE_EVT_GATTS_WRITE_REQ: 							
            /* This event is received when Central device sends a Write command 
             * on an Attribute that the device must service. 
             */
            if(CyBle_GetState() == CYBLE_STATE_CONNECTED) // Added by Zoltan
            {
                
                wrReqParam = (CYBLE_GATTS_WRITE_REQ_PARAM_T *) eventParam; 
                #define SERVICE_HANDLE     (wrReqParam->handleValPair.attrHandle)

                /* Determine which Service Attribute is being written to the GATT DB */
                if(SERVICE_HANDLE >= KWJ_SERVICE_HANDLE && SERVICE_HANDLE < ESS_SERVICE_HANDLE)
                {
                    KwjEventHandler(wrReqParam);      /*< KWJ Custom Command Interpretter */
                }
                if(SERVICE_HANDLE >= ESS_SERVICE_HANDLE && SERVICE_HANDLE < BAS_SERVICE_HANDLE)
                {
                    EssEventHandler(wrReqParam);      /*< ESS Environmental Command Interpretter */
                }
                if(SERVICE_HANDLE >= BAS_SERVICE_HANDLE && SERVICE_HANDLE < DIS_SERVICE_HANDLE)
                {
                    BasEventHandler(wrReqParam);      /*< BAS Battery Command Interpretter */
                }
                if(SERVICE_HANDLE >= DIS_SERVICE_HANDLE)
                {
                    DisEventHandler(wrReqParam);      /*< DIS Device Information Command Interpretter */
                }
            
            } // end check connnected
        default:
//            CyBle_GappStopAdvertisement();
        break;
    }
}
/*****************************************************************************
* Function Name: initGATT
******************************************************************************
* Summary:
* Initiates GATT on connection with the SAV_DB values
******************************************************************************/
void initGATT(void)
{
    uint8 numSnsType = 6u;
    uint8 numSnsParam = 6u;
    uint8 nxtHandle = CO_RAW_INDEX;
    uint8 savIdx = 0;
    uint8 ntfOn[2] = {0x01u, 0x00u};
    
    CYBLE_GATT_HANDLE_VALUE_PAIR_T locHandleValuePair;
    
    /* Store data in database */
    locHandleValuePair.attrHandle= CO_MEASD;
    locHandleValuePair.value.len = SIZE_2_BYTES;
    locHandleValuePair.value.val = ((uint8*)(&SAV_DB) + savIdx);
    
    uint8 i, j;
    for( i = 0; i < numSnsType; i++)
    {
        for( j = 0; j < numSnsParam; j++)
        {
            CyBle_GattsWriteAttributeValue(&locHandleValuePair, 0u,
                NULL, CYBLE_GATT_DB_LOCALLY_INITIATED);
            savIdx += locHandleValuePair.value.len; // point to next value
            locHandleValuePair.attrHandle++; // next handle
            if(locHandleValuePair.attrHandle == BAT_RAW_INDEX)
            {
                savIdx++; // Bat Meas only 1 byte, DB = 2 bytes
            }
            if(j == 0) // Load RAW handle
            {
                locHandleValuePair.attrHandle= nxtHandle;
            }
            if(j == 1) // Gain & OS Size
            {
                locHandleValuePair.value.len = SIZE_4_BYTES;
            }
            if((i < 2 && j == 5) || (i >= 2 && j == 3)) // MEAS & RAW Size
            {
                locHandleValuePair.value.len = SIZE_2_BYTES;
            }
            locHandleValuePair.value.val = ((uint8*)(&SAV_DB) + savIdx); // next
        }
        switch(i)
        {
            case 0: // O3 Meas
            {
                locHandleValuePair.attrHandle= O3_MEASD;
                nxtHandle = O3_RAW_INDEX;
                break;
            }
            case 1: // T Meas
            {
                locHandleValuePair.attrHandle= ESS_TEMPERATURE_MEAS;
                nxtHandle = T_RAW_INDEX;
                numSnsParam = 4u;
                break;
            }
            case 2: // RH Meas
            {
                locHandleValuePair.attrHandle= ESS_HUMIDITY_MEAS;
                nxtHandle = RH_RAW_INDEX;
                break;
            }
            case 3: // P Meas
            {
                locHandleValuePair.attrHandle= ESS_PRESSURE_MEAS;
                nxtHandle = P_RAW_INDEX;
                locHandleValuePair.value.len = SIZE_4_BYTES;
                break;
            }
            case 4: // BAT Meas
            {
                locHandleValuePair.attrHandle= BAS_BATTERY_LEVEL;
                nxtHandle = BAT_RAW_INDEX;
                locHandleValuePair.value.len = SIZE_1_BYTES;
                break;
            }
        }
    }
    GattSetCharVal(CO_MOD_INDEX, SIZE_2_BYTES, VarToCharArray(&CO_MOD, SIZE_2_BYTES));
    GattSetCharVal(CO_UNH_INDEX, SIZE_2_BYTES, VarToCharArray(&CO_UNH, SIZE_2_BYTES));
    GattSetCharVal(CO_HAZ_INDEX, SIZE_2_BYTES, VarToCharArray(&CO_HAZ, SIZE_2_BYTES));
    GattSetCharVal(O3_MOD_INDEX, SIZE_2_BYTES, VarToCharArray(&O3_MOD, SIZE_2_BYTES));
    GattSetCharVal(O3_UNH_INDEX, SIZE_2_BYTES, VarToCharArray(&O3_UNH, SIZE_2_BYTES));
    GattSetCharVal(O3_HAZ_INDEX, SIZE_2_BYTES, VarToCharArray(&O3_HAZ, SIZE_2_BYTES));
    GattSetCharVal(BAT_LO_INDEX, SIZE_1_BYTES, VarToCharArray(&BAT_LO, SIZE_1_BYTES));
    GattSetCharVal(BAT_CRIT_INDEX, SIZE_1_BYTES, VarToCharArray(&BAT_CRIT, SIZE_1_BYTES));
    GattSetCharVal(DIS_DEVICE_SW_REV, 15u,(uint8*) &SOFTWARE);
    GattSetCharVal(DIS_DEVICE_MODL_NMBR, 15u,(uint8*) &SAV_DB.MODEL);
    GattSetCharVal(DIS_DEVICE_MNFR_NAME, 7u,(uint8*) &SAV_DB.MNFR);
    GattSetCharVal(DIS_DEVICE_SER_NMBR, 6u,(uint8*) &cyBle_deviceAddress.bdAddr);
    GattSetCharVal(CO_CCCD, SIZE_2_BYTES, ntfOn);
    GattSetCharVal(O3_CCCD, SIZE_2_BYTES, ntfOn);
//    GattSetCharVal(LOG_CCCD, SIZE_2_BYTES, ntfOn);
    GattSetCharVal(CAL_CCCD, SIZE_2_BYTES, ntfOn);
    StartLog();
    DisUpdateFirmWareRevision();
    setFlag(NOTIFICATIONS);
}
/*****************************************************************************
* Function Name: KWJ_BLE_Process
******************************************************************************
* Summary:
* Calls all the BLE send methods when meaurements are completed
*
* Parameters:
* None
*
* Return:
* None
*
* Side Effects:
* None
*
*****************************************************************************/
void KWJ_BLE_Process(void)
{
    SendKwjOverBLE();
    SendEssOverBLE();
    SendBasOverBLE();
    SendDisOverBLE();
}
/******************************************************************************
* Function Name: GattSetCharVal
***************************************************************************//**
* 
*  Writes a characteristic value of the service in the local database.
* 
*  \param serviceIndex: The index of the service instance.
*  \param charIndex: The index of the service characteristic of type 
*  \param attrSize: The size of the characteristic value attribute.
*  \param attrValue: The pointer to the characteristic value data that should be
*               stored to the GATT database.
* 
* \return
*  Return value is of type CYBLE_API_RESULT_T.
*  * CYBLE_ERROR_OK - The request handled successfully
*  * CYBLE_ERROR_INVALID_PARAMETER - Validation of the input parameter failed
*
* \events
*  None
*
******************************************************************************/
CYBLE_API_RESULT_T GattSetCharVal(CYBLE_GATT_DB_ATTR_HANDLE_T serviceIndex,
    uint8 attrSize, uint8 *attrValue)
{
    CYBLE_API_RESULT_T apiResult = CYBLE_ERROR_OK;
    CYBLE_GATT_HANDLE_VALUE_PAIR_T locHandleValuePair;
    
    /* Store data in database */
    locHandleValuePair.attrHandle= serviceIndex;
    locHandleValuePair.value.len = attrSize;
    locHandleValuePair.value.val = attrValue;
    if(CYBLE_GATT_ERR_NONE !=
        CyBle_GattsWriteAttributeValue(&locHandleValuePair, 0u,
            NULL, CYBLE_GATT_DB_LOCALLY_INITIATED))
    {
        apiResult = CYBLE_ERROR_INVALID_PARAMETER;
    }
    return (apiResult);
}
/******************************************************************************
* Function Name: VarToCharArray
*******************************************************************************
* Summary:
*  Return a uint8[] pointer representing the Variable passed for storing
*  in GATT DB
*
* Parameters:
*  Variable pointer
*
* Return:
*  uint8 [] pointer
*
******************************************************************************/
uint8 * VarToCharArray(void * memLoc, uint8 numByte)
{
    uint8 i;
    uint8 *arr = &BFR[0];
    
    for(i = 0; i < numByte; i++){
        BFR[i] = *(uint8*)(memLoc+i);
        if(i == 127) break; // cutoff anything larger than max size
    }
    return arr;
}

/* [] END OF FILE */
