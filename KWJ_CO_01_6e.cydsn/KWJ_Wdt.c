/* ========================================
 * File Name: KWJ_Wdt.c
 *
 * Version: 1.0
 *
 * Description:
* This file implements the watchdog timer for a deep-sleep wakeup source, 
* and also to keep the timestamp.
 *
 * Hardware Dependency:
 * CYBLE-014008
 *
 * Copyright ODI, 2016
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF ODI.
 * RCRegna - Author
 * ========================================
*/
#include "project.h"
#include "main.h"


/*****************************************************************************
* Global variables
*****************************************************************************/
volatile static uint32 nextTicks = WATCHDOG_DEFAULT_PERIOD_SEC * WATCHDOG_TICKS_PER_SEC;
static uint32 oneMinute = LOG_1_MIN;
static uint32 fiveMinute = LOG_1_MIN * 5u;
static uint32 sampleInterval = SYS_TIME_FAST;
static uint32 intervalTimer = SYS_TIME_FAST;

/* interval accessor */
void setInterval(uint32 theInterval)
{
    sampleInterval = theInterval;
}


/*****************************************************************************
* Function Name: WatchdogTimer_Lock()
******************************************************************************
* Summary:
* Locks the watchdog timer to prevent configuration changes.
*
* Parameters:
* None
*
* Return:
* None
*
* Theory:
* The CLK_SELECT register is written to, such that watchdog timer is now 
* locked. Any further changes to watchdog timer registers are then ignored.
*
* Side Effects:
* None
*
* Note:
*
*****************************************************************************/
//static void WatchdogTimer_Lock(void)
void WatchdogTimer_Lock(void)
{
    uint32 ClkSelectValue;

    /* Update CLK_SELECT register to lock watchdog timer */
    ClkSelectValue = CY_GET_REG32(CYREG_CLK_SELECT) | CLK_SELECT_WDT_LOCK_SET01;
    CY_SET_REG32(CYREG_CLK_SELECT, ClkSelectValue);
}


/*****************************************************************************
* Function Name: WatchdogTimer_Unlock()
******************************************************************************
* Summary:
* Unlocks the watchdog timer to allow configuration changes.
*
* Parameters:
* None
*
* Return:
* None
*
* Theory:
* The CLK_SELECT register is written to, such that watchdog timer is now 
* unlocked. The watchdog timer registers can then be modified.
*
* Side Effects:
* None
*
* Note:
*
*****************************************************************************/
//static void WatchdogTimer_Unlock(void)
void WatchdogTimer_Unlock(void)
{
    uint32 ClkSelectValue;

    /* Update CLK_SELECT register to unlock watchdog timer */
    ClkSelectValue = CY_GET_REG32(CYREG_CLK_SELECT) & ~CLK_SELECT_WDT_LOCK_SET01;
    CY_SET_REG32(CYREG_CLK_SELECT, ClkSelectValue | CLK_SELECT_WDT_LOCK_CLR0);
    CY_SET_REG32(CYREG_CLK_SELECT, ClkSelectValue | CLK_SELECT_WDT_LOCK_CLR1);
}


/*****************************************************************************
* Public Functions
*****************************************************************************/

/*****************************************************************************
* Function Name: WatchdogTimer_Isr()
******************************************************************************
* Summary:
* The ISR for the watchdog timer. 1 Second Heartbeat for system
*
* Parameters:
* None
*
* Return:
* None
*
* Theory:
* The ISR checks if the WDT interrupt source is timer 0. If yes, then it 
* updates the WDT_MATCH register to prepare for the next interrupt and
* updates the flags for measurement interval. 
*
*****************************************************************************/
CY_ISR(WatchdogTimer_Isr)
{
    
    BLE_bless_isr_ClearPending();
    
    /* Unlock watchdog timer to enable configuration changes*/
    CyGlobalIntDisable; 
    WatchdogTimer_Unlock();

    /* Clear WDT pending interrupt */
    CY_SET_REG32(CYREG_WDT_CONTROL, CY_GET_REG32(CYREG_WDT_CONTROL) |
        WDT_CONTROL_WDT_INT0);

    /* Lock watchdog timer to prevent configuration changes */
    WatchdogTimer_Lock();
    CyGlobalIntEnable; 
    
    if(chkFlag(WAKEUP_SOURCE_WDT))
    {
        /* If flag wasn't reset by the appliction, we have a problem */
        System_Init();
    }
    /* Set WDT flag for application processing */
    setFlag( WAKEUP_SOURCE_WDT);
    
}

/*****************************************************************************
* Function Name: UpdateCycle()
******************************************************************************
* Summary:
* Updates system control parameters each WDT cycle. 
*
* Parameters:
* None
*
* Return:
* None
*
*****************************************************************************/
void UpdateCycle(void)
{
    /* Start 5 minute timer when Low Battery or HAZARDOUS levels detected */
    if(chkFlag( ALERT_HAZARDOUS) || chkFlag( LOW_BATTERY))
    {
        if(0u == --fiveMinute)
        {
            fiveMinute = 60u * 5u;             /*< Reset 5 minute counter */
            if(chkFlag( ALERT_HAZARDOUS))   /*< Hazardous Alert timer */
            {
                setFlag( FIVE_MINUTE);            
            }
            else if(chkFlag( LOW_BATTERY))  /*< Lo Battery Alert Timer */
            {
                HandleBZR(BZR_PEEP, true);
                HandleLED(RED_LED, LED_FLASH, true);
            }
        }
    }
    /* HAZARDOUS levels not detected so reset flag */
    if(false == chkFlag( ALERT_HAZARDOUS))
    {
        rstFlag(FIVE_MINUTE);            
    }
    
    /* Warmup the amplifiers prior to the sample interval */
    if(intervalTimer == WRM_COUNT)
    {
        setFlag(OP_WARMUP);
//        MEAS_1X;         // MEAS_OP = Follower
//        CyDelay(20u);    // Provide 20mS delay to settle 3v3 components
#      ifdef O3_OPTION
//        REG_3V3_ON;
//        ADC_SAR_12Bit_Start();
#      endif
        BIAS_SENS;       // RE opened from WE
//        CyDelay(80u);    // Provide 80mS delay to settle sensor
        MEAS_AMP;        // MEAS_OP = Amplifier
////        BIAS_SENS;       // RE opened from WE
    }
    /* If interval timer has elapsed then enable MEASUREMENT_INTERVAL flag */
    if(0 == intervalTimer--)
    {
        /* Initialize interval timer and set MEASUREMENT_INTERVAL flag */
        intervalTimer = sampleInterval - 1;
        REG_3V3_ON;
        ADC_SAR_12Bit_Start();
        SCB_Start();
        CyDelay(30u);    // Provide 30mS delay to settle 3v3 components
        SCB_I2CMasterSendStop();
        SCB_ClearPendingInt();
        setFlag(MEASUREMENT_INTERVAL);
    }

    /* If 1 minute timer has elapsed then enable ENABLE_LOGGING flag */
    if(oneMinute-- <=0u)
    {
        /* Initialize 1 minute timer and set ENABLE_LOGGING flag */
        oneMinute = LOG_1_MIN;
        setFlag(ENABLE_LOGGING);
        if(chkFlag(START_UP))
        {
            if(--MIN_COUNT == 0)
            {
                MIN_COUNT = NUM_MINS_STRT;
                
                WRM_COUNT == WARM_UP ? rstFlag(START_UP) : WRM_COUNT--;
            }
        }
    }
    /* resetting the WDT flag means the main loop is OK */
    rstFlag(WAKEUP_SOURCE_WDT);
    
}

/*****************************************************************************
* Function Name: WatchdogTimer_Start()
******************************************************************************
* Summary:
* Initializes the watchdog timer 0.
*
* Parameters:
* wdtPeriodMs - The watchdog timer period to set, in Seconds.
*
* Return:
* None
*
* Theory:
* Writes to the SRSS registers to start the low frequency clock and configure 
* the watchdog timer 0. The watchdog timer is configured for 16-bit timing, 
* and to generate an interrupt on match. 
*
* Side Effects:
* None
*
* Note:
*
*****************************************************************************/
void WatchdogTimer_Start(uint32 wdtPeriodSec)
{
    CyIntSetVector(WDT_INTERRUPT_NUM, &WatchdogTimer_Isr);
    
    WatchdogTimer_Unlock();

    /* Set the WDT period */
    nextTicks = WATCHDOG_TICKS_PER_SEC * wdtPeriodSec;
    UPDATE_WDT_MATCH(nextTicks);
        
    /* Set WDT_CONFIG register */
    CY_SET_REG32(CYREG_WDT_CONFIG, CY_GET_REG32(CYREG_WDT_CONFIG) |
        WDT_CONFIG_WDT_MODE0_INT);

    /* Enable WDT interrupt for the NVIC */
    CyIntEnable(WDT_INTERRUPT_NUM);

    /* Set WDT_CONTROL register */
    CY_SET_REG32(CYREG_WDT_CONTROL, CY_GET_REG32(CYREG_WDT_CONTROL) |
        WDT_CONTROL_WDT_ENABLE0);

    /* Wait for the WDT enable to complete */
    while((CY_GET_REG32(CYREG_WDT_CONTROL) & WDT_CONTROL_WDT_ENABLED0) == 0);
    
    WatchdogTimer_Lock();
}

/*****************************************************************************
* Function Name: WatchdogTimer_Stop()
******************************************************************************
* Summary:
* Disables the watchdog timer.
*
* Parameters:
* None
*
* Return:
* None
*
* Theory:
* Disables the watchdog timer 0.
*
* Side Effects:
* None
*
* Note:
*
*****************************************************************************/
void WatchdogTimer_Stop (void)
{
    WatchdogTimer_Unlock();

    /* Clear WDT pending interrupt */
    CY_SET_REG32(CYREG_WDT_CONTROL, CY_GET_REG32(CYREG_WDT_CONTROL) |
        WDT_CONTROL_WDT_INT0);

    CY_SET_REG32(CYREG_WDT_CONTROL, CY_GET_REG32(CYREG_WDT_CONTROL) & 
        ~WDT_CONTROL_WDT_ENABLE0);

    /* Wait for the WDT disable to complete */
    while((CY_GET_REG32(CYREG_WDT_CONTROL) & WDT_CONTROL_WDT_ENABLED0) != 0);
    
    WatchdogTimer_Lock();
}
/*****************************************************************************
* Sleep Routines for Sleep, Deep Sleep & Hibernate
*****************************************************************************/
/* Prepare for Sleep */
void SleepPrep(void)
{
    CyGlobalIntEnable;
#  ifdef DS_OKAY
    if(false == chkFlag(OP_WARMUP))
    {
        ADC_SAR_12Bit_Stop();
        REG_3V3_OFF;
    }
#  endif    
    SCB_Stop();
    SMPLBAT_Write(0);
}
/* Prepare for Deep Sleep */
void DeepSleepPrep(void)
{
    SleepPrep();
    MuteBZR();
    AllOffLED();
    
    P0_HIB;
    P1_HIB;
    P2_HIB;
    P3_HIB;    
    
    if((CyBle_GetBleSsState() == CYBLE_BLESS_STATE_ECO_ON) ||
       (CyBle_GetBleSsState() == CYBLE_BLESS_STATE_DEEPSLEEP) ||
        true == chkFlag(STAND_ALONE_MODE))
    {
        /* Make sure the IMO is running and selected as the main clock source
        / before going into deep sleep */
        CySysClkImoStart();
        CySysClkWriteHfclkDirect(CY_SYS_CLK_HFCLK_IMO);
    }   
}
/*****************************************************************************
* Function Name: GoToSleep()
******************************************************************************
* Summary:
* Put the system to sleep
*
* Parameters:
* None
*
* Return:
* None
*
*****************************************************************************/
void GoToSleep (void)
{
    SleepPrep();
    CySysPmSleep();
}

/*****************************************************************************
* Function Name: GoToDeepSleep()
******************************************************************************
* Summary:
* Put the system into deep sleep
*
* Parameters:
* None
*
* Return:
* None
*
*****************************************************************************/
void GoToDeepSleep (void)
{
#  ifdef DS_OKAY
    if(false == chkFlag(OP_WARMUP))
    {
        BIAS_HOLD;        // RE shorted to WE
        DeepSleepPrep();
        CySysPmDeepSleep();
    }
    else
        GoToSleep();
#  else    
        GoToSleep();
#  endif
}

/*****************************************************************************
* Function Name: WakeFromSleep()
******************************************************************************
* Summary:
* Wakes up the system from sleep
*
* Parameters:
* None
*
* Return:
* None
*
*****************************************************************************/
void WakeFromSleep (void)
{
    P0_ENA;
    P1_ENA;
    P2_ENA;
    P3_ENA;    
    /* Use the external clock as the main source while debugging, 
    / use the IMO if not debugging otherwise
    / the system will lock up while trying to wake up from deep sleep */
    if(CY_PM_PWR_CONTROL_REG & (CYVAL__DEBUG_SESSION_SESSION_ACTIVE << CYFLD__DEBUG_SESSION__OFFSET)){
        if(CySysClkEcoStart(20000) == CYRET_SUCCESS){
              CySysClkWriteHfclkDirect(CY_SYS_CLK_HFCLK_ECO);
        }
    }
    AllOffLED();
    MuteBZR();

}
/*****************************************************************************
* Function Name: PrepareForDeepSleep()
******************************************************************************
* Summary:
* Prepare the system for deep sleep
*
* Parameters:
* None
*
* Return:
* None
*
*****************************************************************************/
void Hibernate (void)
{
    rstFlag(ENABLE_HIBERNATE);
    WriteUserSFlashRow(DB_SAVE,StructToFlash((PARAM_DB_T *)&SAV_DB, sizeof(SAV_DB)));
#  ifndef AUTO_CONNECT
    SAmode();
#  endif
    DeepSleepPrep();
    WDT_Interrupt_Stop();
    WatchdogTimer_Stop();
    LED_Interrupt_Stop();
    BZR_Interrupt_Stop();

    isr_SW_ClearPending();
    SW_ClearInterrupt();
    rstFlag( MUTE_ALARM);
    CySysPmHibernate();
}
/* [] END OF FILE */
