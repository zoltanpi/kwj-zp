/* ========================================
 * File Name: KWJ_Device.c
 *
 * Version: 1.0
 *
 * Description:
 * This file implements the BLE service for Device Info
 *
 * Hardware Dependency:
 * CYBLE-014008
 *
 * Copyright ODI, 2016
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF ODI.
 * RCRegna - Author
 * ========================================
*/
#include "project.h"
#include "main.h"
#include "KWJ_BLE_Process.h"

// Device Parameters..........................................
static char   DEVICE_MNFR[7]     = "KWJ";
static char   DEVICE_MODEL[15]   = "KWJ CO/O3 01";
char          SOFTWARE[15]       = "05_29_17-beta6e";

/******************************************************************************
* Function Name: Init_Dev_DB
*******************************************************************************
* Summary:
*  Initializes the SAV_DB Device entries with factory defaults
*
******************************************************************************/
void Init_Dev_DB(void)
{
    rstFlag(DIS_NOTIFICATION);
    
    if(false == (SAV_DB.CAL_DONE & DIS_MNFR_CAL))
        strcpy((char*)SAV_DB.MNFR,(char*)DEVICE_MNFR);
    if(false == (SAV_DB.CAL_DONE & DIS_MODL_CAL))
        strcpy((char*)SAV_DB.MODEL,(char*)DEVICE_MODEL);
}/*****************************************************************************
* Function Name: DisEventHandler
******************************************************************************
* Summary:
* Event handler for the Device Information Service specific events.
*
* Params:
*  *wrReqParam - Device Information Characteristic Value GATT DB Index
*
* Return:
*  None
*
*****************************************************************************/
void DisEventHandler(CYBLE_GATTS_WRITE_REQ_PARAM_T *wrReqParam)
{
//    /* Event handler switch statement for the DIS service specific events. */
    uint16  index = wrReqParam->handleValPair.attrHandle;
    //uint8 * value = wrReqParam->handleValPair.value.val;

    switch(index)
	{

        case DIS_SERVICE_HANDLE: /*< Handle of Device Information Service */
    // ------------------add code if needed-------------------------
        break;
		
        case DIS_DEVICE_MNFR_NAME: /*< Handle of the Manufacturer Name String characteristic */ 
    // ------------------add code if needed-------------------------
//            strcpy(DEVICE_NAME,eventParam);
		    break;

        case DIS_DEVICE_MODL_NMBR: /*< Handle of the Model Number String characteristic */ 
    // ------------------add code if needed-------------------------
	    	break;

        case DIS_DEVICE_SER_NMBR: /*< Handle of the Serial Number String characteristic */ 
    // ------------------add code if needed-------------------------
	    	break;

        case DIS_DEVICE_HW_REV: /*< Handle of the Hardware Revision String characteristic */ 
    // ------------------add code if needed-------------------------
	    	break;

        case DIS_DEVICE_FW_REV: /*< Handle of the Firmware Revision String characteristic */ 
    // ------------------add code if needed-------------------------
//            strcpy(firmware_date_,eventParam);
	    	break;

        case DIS_DEVICE_SW_REV: /*< Handle of the Software Revision String characteristic */ 
//            CyBle_DissSetCharacteristicValue(CYBLE_DIS_SOFTWARE_REV,
//                sizeof((uint8 *)&wrReqParam), (uint8 *)&wrReqParam);
    // ------------------add code if needed-------------------------
	    	break;

        case DIS_DEVICE_SYS_ID: /*< Handle of the System ID characteristic */ 
    // ------------------add code if needed-------------------------
	    	break;

        case DIS_DEVICE_IEEE: /*< Handle of the IEEE 11073-20601 Regulatory Certification Data List characteristic */ 
    // ------------------add code if needed-------------------------
	    	break;

        case DIS_DEVICE_PNP_ID: /*< Handle of the PnP ID characteristic */ 
    // ------------------add code if needed-------------------------
	    	break;
		
		default:
    // ------------------add code if needed-------------------------
        break;
	}
}
/*******************************************************************************
* Function Name: DisUpdateFirmWareRevision()
********************************************************************************
* Summary:
* Updates the Firmware Revision characteristic with BLE Stack version.
*
*******************************************************************************/
void DisUpdateFirmWareRevision(void)
{
    CYBLE_STACK_LIB_VERSION_T stackVersion;
    uint8 fwRev[9u] = "0.0.0.000";
    
    if(CyBle_GetStackLibraryVersion(&stackVersion) == CYBLE_ERROR_OK)
    {
        /* Transform numbers to ASCII string */
        fwRev[0u] = stackVersion.majorVersion + '0'; 
        fwRev[2u] = stackVersion.minorVersion + '0';
        fwRev[4u] = stackVersion.patch + '0';
        fwRev[6u] = (stackVersion.buildNumber / 100u) + '0';
        stackVersion.buildNumber %= 100u; 
        fwRev[7u] = (stackVersion.buildNumber / 10u) + '0';
        fwRev[8u] = (stackVersion.buildNumber % 10u) + '0';
    }
    
    CyBle_DissSetCharacteristicValue(CYBLE_DIS_FIRMWARE_REV, sizeof(fwRev), fwRev);
    
}

/*****************************************************************************
* Function Name: SendDisOverBLE
******************************************************************************
* Summary:
* Sends the Device Information characteristic notification. None required
* packet.
*
*****************************************************************************/
void SendDisOverBLE(void)
{
	if(chkFlag( DIS_NOTIFICATION))
	{
    // ------------------add code if needed------------------------- 
        rstFlag(DIS_NOTIFICATION);
    }
}

/* [] END OF FILE */
