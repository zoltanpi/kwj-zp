/* ========================================
 * File Name: KWJ_Smpl.c
 *
 * Version: 1.0
 *
 * Description:
 * This file implements the Sensor Sampling Activities
 *
 * Hardware Dependency:
 * CYBLE-014008
 *
 * Copyright ODI, 2016
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF ODI.
 * RCRegna - Author
 * ========================================
*/
/*****************************************************************************
* Included headers
*****************************************************************************/
#include <project.h>
#include "main.h"

/*******************************************************************************
* Function Name: SampleAll
********************************************************************************
* Summary:
*  The measurement handler for WDT wakeup
*  Makes a CO reading
*  Set LED and Buzzer Mode Based on Alert Level
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void SampleAll(void)
{
//    rstFlag(WAKEUP_SOURCE_WDT);
    if(chkFlag(MEASUREMENT_INTERVAL))
    {
        CyDelay(10u); // Provide 10mS settling
        alert = GOOD;
        setInterval(SYS_TIME_FAST);
        Make_T_Measurement();
        Make_RH_Measurement();
        Make_CO_Measurement();
#      ifdef O3_OPTION
        Make_O3_Measurement();
#      endif
        Make_P_Measurement();
        Make_BAT_Measurement();
        rstFlag(OP_WARMUP);
        rstFlag(ALERT_HAZARDOUS);
        if(NOW_DB.CO_MEAS >= CO_MOD
#      ifdef O3_OPTION
            || NOW_DB.O3_MEAS >= O3_MOD
#      endif
            )
            alert = MODERATE;
        if(NOW_DB.CO_MEAS >= CO_UNH
#      ifdef O3_OPTION
            || NOW_DB.O3_MEAS >= O3_UNH
#      endif
            )
            alert = UNHEALTHY;
        if(NOW_DB.CO_MEAS >= CO_HAZ
#      ifdef O3_OPTION
            || NOW_DB.O3_MEAS >= O3_HAZ
#      endif
            )
        {
            alert = HAZARDOUS;
            setFlag( ALERT_HAZARDOUS);
        }
        if(alert == GOOD)
            HandleLED(GRN_LED, LED_FLASH, false); //not to spec

    }
    if(chkFlag(ENABLE_LOGGING))
    {
        rstFlag(ENABLE_LOGGING);
        LogMeas();
    }
    /* Reload LED & BZR status each WDT cycle (1 sec) */
    switch(alert)
    {
    case GOOD:
#      ifndef KWJ_TEST
//        setInterval(SYS_TIME_NORM);
#      endif
        break;
    case MODERATE:
        HandleLED(ORG_LED, LED_FLASH, false);
        break;
    case UNHEALTHY:
        HandleLED(ORG_LED, LED_BLINK, false);
        HandleBZR(BZR_PEEP, false);
        break;
    case HAZARDOUS:
        if(chkFlag(FIVE_MINUTE))
        {
            HandleBZR(BZR_ON, false);
            HandleLED(RED_LED, LED_ON, false);            
        }
        else
        {
            HandleBZR(BZR_ALERT, false);
            HandleLED(RED_LED, LED_BLINK, false);            
        }
        break;
    }
    
} // SampleAll()
/* [] END OF FILE */
