/* ========================================
 * File Name: KWJ_Button.c
 *
 * Version: 1.0
 *
 * Description:
 * This file implements the KWJ Button Functionality
 *
 * Hardware Dependency:
 * CYBLE-014008
 *
 * Copyright ODI, 2016
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF ODI.
 * RCRegna - Author
 * ========================================
*/
#include "project.h"
#include "main.h"

/*******************************************************************************
* Function Name: ButtonPress_ISR
********************************************************************************
*
* Summary:
*   Interupt Routine to Handle the mechanical button press(es).
*
* Parameters:
*   None.
*
* Return:
*   None.
*
*******************************************************************************/
CY_ISR(ButtonPress_ISR)
{
  //uint8 shortTime = 10u; // 1 sec window (for 1 or 2 pushes)
    uint8 longTime  = 60u; // 6 sec window (for 1 long push)
    uint8 ticks = 0u;
    uint8 pushes = 1u;

    CyGlobalIntEnable; 
    isr_SW_Disable();
    /* If no connection, turn on in Stand Alone first */
    if(CyBle_GetState() == CYBLE_STATE_DISCONNECTED)
    {
         setFlag(STAND_ALONE_MODE);
    }
    CyDelay(15u); // Provide Debounce delay (mS)
    setFlag( LAST_STATE);  // Last state of Button, Set = pushed
    /* Count the times button is pushed during the short time interval */
#if 0
    while(ticks <= shortTime)
    {
        // Button checked for being released
        if(chkFlag(LAST_STATE) && BUTTON_IS_NOT_PRESSED)
        {
            rstFlag(LAST_STATE);
        }
        // Button checked for being pushed again
        if((false == chkFlag(LAST_STATE)) && BUTTON_IS_PRESSED)
        {
            HandleLED(RED_LED, LED_FLASH, true);
            setFlag( LAST_STATE);
            pushes++;
            CyDelay(15u); // Provide Debounce delay (mS)
            HandleBZR(BZR_PEEP, true);
        }
        CyDelay(100u); // Provide 100mS delay / loop
        ticks++;
    }
#endif
    /* Long interval timer */
    while(YES == BUTTON_IS_PRESSED) 
    {
        /* check if button still pressed after long time out 
        *  and prepare the system to power down (Hibernate)
        */
        WatchdogTimer_Stop();
        if(ticks >= longTime)
        {
            setFlag(ENABLE_HIBERNATE);
            rstFlag( MUTE_ALARM);
            RED_LED_ON;
            uint8 i = 0;
            while(i++ < 3)
            {
                HandleBZR(BZR_PEEP, true);
                CyDelay(200u); // Provide 200mS delay / loop
            }
            while(YES == BUTTON_IS_PRESSED) 
            { /* wait for user to release button */ }
            CyDelay(15u); // Provide Debounce delay (mS)
            RED_LED_OFF;
            pushes = 0;
            SAmode(); // ZOLTAN
        }
        CyDelay(100u); // Provide 100mS delay / loop
        ticks++;
        
    }
    WatchdogTimer_Start(WDT_1_SEC);
    /* 1 push if system ON means toggle the alarm  mode */
    if(pushes == 1)
    {
        if(chkFlag(MUTE_ALARM))
        {
            rstFlag( MUTE_ALARM);
            uint8 i = 0;
            while(i++ < 2)
            {
                HandleBZR(BZR_PEEP, true);
                CyDelay(200u); // Provide 200mS delay / loop
            }
        }
        else
        {
            MuteBZR();
            setFlag( MUTE_ALARM);
        }
    }
    /* 1 push means pair with host if in disconnected state */
    if(pushes == 1 && (CyBle_GetState() == CYBLE_STATE_DISCONNECTED))
    {
        if(chkFlag(STAND_ALONE_MODE))
        {
            GRN_LED_ON;
            BLEmode();
            setFlag(NEW_GATT_DB);
            BLE_RETRY = NUM_RETRY_BLE;
            setFlag(ALL_NOTIFICATION);
            setFlag(CHANGE_BLE);
            rstFlag(LOW_RATE_BLE);
            CyDelay(600u);
            GRN_LED_OFF;
        }
    }
    /* clear the isr interrupt request */
    SW_ClearInterrupt();
    isr_SW_ClearPending();
    isr_SW_Enable();
} // ButtonPress_ISR()

/* [] END OF FILE */
