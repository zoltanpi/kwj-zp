/***************************************************************************//**
* \file CYBLE_custom.h
* \version 3.30
* 
* \brief
*  Contains the function prototypes and constants for the Custom Service.
* 
********************************************************************************
* \copyright
* Copyright 2014-2016, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_BLE_CYBLE_CUSTOM_H)
#define CY_BLE_CYBLE_CUSTOM_H

#include "BLE_gatt.h"


/***************************************
* Conditional Compilation Parameters
***************************************/

/* Maximum supported Custom Services */
#define CYBLE_CUSTOMS_SERVICE_COUNT                  (0x01u)
#define CYBLE_CUSTOMC_SERVICE_COUNT                  (0x00u)
#define CYBLE_CUSTOM_SERVICE_CHAR_COUNT              (0x04u)
#define CYBLE_CUSTOM_SERVICE_CHAR_DESCRIPTORS_COUNT  (0x21u)

/* Below are the indexes and handles of the defined Custom Services and their characteristics */
#define CYBLE_KWJ_SERVICE_INDEX   (0x00u) /* Index of KWJ service in the cyBle_customs array */
#define CYBLE_KWJ_CO_CHAR_INDEX   (0x00u) /* Index of CO characteristic */
#define CYBLE_KWJ_CO_MEASUREMENT_DESC_INDEX   (0x00u) /* Index of Measurement descriptor */
#define CYBLE_KWJ_CO_CLIENT_CHARACTERISTIC_CONFIGURATION_DESC_INDEX   (0x01u) /* Index of Client Characteristic Configuration descriptor */
#define CYBLE_KWJ_CO_UNITS_DESC_INDEX   (0x02u) /* Index of Units descriptor */
#define CYBLE_KWJ_CO_CHARACTERISTIC_USER_DESCRIPTION_DESC_INDEX   (0x03u) /* Index of Characteristic User Description descriptor */
#define CYBLE_KWJ_O3_CHAR_INDEX   (0x01u) /* Index of O3 characteristic */
#define CYBLE_KWJ_O3_MEASUREMENT_DESC_INDEX   (0x00u) /* Index of Measurement descriptor */
#define CYBLE_KWJ_O3_CLIENT_CHARACTERISTIC_CONFIGURATION_DESC_INDEX   (0x01u) /* Index of Client Characteristic Configuration descriptor */
#define CYBLE_KWJ_O3_UNITS_DESC_INDEX   (0x02u) /* Index of Units descriptor */
#define CYBLE_KWJ_O3_CHARACTERISTIC_USER_DESCRIPTION_DESC_INDEX   (0x03u) /* Index of Characteristic User Description descriptor */
#define CYBLE_KWJ_LOG_CHAR_INDEX   (0x02u) /* Index of LOG characteristic */
#define CYBLE_KWJ_LOG_CLIENT_CHARACTERISTIC_CONFIGURATION_DESC_INDEX   (0x00u) /* Index of Client Characteristic Configuration descriptor */
#define CYBLE_KWJ_LOG_TIMESTAMP_DESC_INDEX   (0x01u) /* Index of Timestamp descriptor */
#define CYBLE_KWJ_LOG_BASE16_DESC_INDEX   (0x02u) /* Index of Base16 descriptor */
#define CYBLE_KWJ_LOG_BASE32_DESC_INDEX   (0x03u) /* Index of Base32 descriptor */
#define CYBLE_KWJ_LOG_TYPE_DESC_INDEX   (0x04u) /* Index of Type descriptor */
#define CYBLE_KWJ_LOG_LENGTH_DESC_INDEX   (0x05u) /* Index of Length descriptor */
#define CYBLE_KWJ_LOG_DATA_DESC_INDEX   (0x06u) /* Index of Data descriptor */
#define CYBLE_KWJ_LOG_CHARACTERISTIC_USER_DESCRIPTION_DESC_INDEX   (0x07u) /* Index of Characteristic User Description descriptor */
#define CYBLE_KWJ_CAL_CHAR_INDEX   (0x03u) /* Index of CAL characteristic */
#define CYBLE_KWJ_CAL_CO_RAW_DESC_INDEX   (0x00u) /* Index of CO Raw descriptor */
#define CYBLE_KWJ_CAL_CO_GAIN_DESC_INDEX   (0x01u) /* Index of CO Gain descriptor */
#define CYBLE_KWJ_CAL_CO_OS_DESC_INDEX   (0x02u) /* Index of CO OS descriptor */
#define CYBLE_KWJ_CAL_CO_TZERO_TC_DESC_INDEX   (0x03u) /* Index of CO TZero TC descriptor */
#define CYBLE_KWJ_CAL_CO_SNSFAC_DESC_INDEX   (0x04u) /* Index of CO SnsFac descriptor */
#define CYBLE_KWJ_CAL_CO_MOD_DESC_INDEX   (0x05u) /* Index of CO MOD descriptor */
#define CYBLE_KWJ_CAL_CO_UNH_DESC_INDEX   (0x06u) /* Index of CO UNH descriptor */
#define CYBLE_KWJ_CAL_CO_HAZ_DESC_INDEX   (0x07u) /* Index of CO HAZ descriptor */
#define CYBLE_KWJ_CAL_O3_RAW_DESC_INDEX   (0x08u) /* Index of O3 Raw descriptor */
#define CYBLE_KWJ_CAL_O3_GAIN_DESC_INDEX   (0x09u) /* Index of O3 Gain descriptor */
#define CYBLE_KWJ_CAL_O3_OS_DESC_INDEX   (0x0Au) /* Index of O3 OS descriptor */
#define CYBLE_KWJ_CAL_O3_TZERO_TC_DESC_INDEX   (0x0Bu) /* Index of O3 TZero TC descriptor */
#define CYBLE_KWJ_CAL_O3_SNSFAC_DESC_INDEX   (0x0Cu) /* Index of O3 SnsFac descriptor */
#define CYBLE_KWJ_CAL_O3_MOD_DESC_INDEX   (0x0Du) /* Index of O3 MOD descriptor */
#define CYBLE_KWJ_CAL_O3_UNH_DESC_INDEX   (0x0Eu) /* Index of O3 UNH descriptor */
#define CYBLE_KWJ_CAL_O3_HAZ_DESC_INDEX   (0x0Fu) /* Index of O3 HAZ descriptor */
#define CYBLE_KWJ_CAL_T_RAW_DESC_INDEX   (0x10u) /* Index of T Raw descriptor */
#define CYBLE_KWJ_CAL_T_GAIN_DESC_INDEX   (0x11u) /* Index of T Gain descriptor */
#define CYBLE_KWJ_CAL_T_OS_DESC_INDEX   (0x12u) /* Index of T OS descriptor */
#define CYBLE_KWJ_CAL_RH_RAW_DESC_INDEX   (0x13u) /* Index of RH Raw descriptor */
#define CYBLE_KWJ_CAL_RH_GAIN_DESC_INDEX   (0x14u) /* Index of RH Gain descriptor */
#define CYBLE_KWJ_CAL_RH_OS_DESC_INDEX   (0x15u) /* Index of RH OS descriptor */
#define CYBLE_KWJ_CAL_P_RAW_DESC_INDEX   (0x16u) /* Index of P Raw descriptor */
#define CYBLE_KWJ_CAL_P_GAIN_DESC_INDEX   (0x17u) /* Index of P Gain descriptor */
#define CYBLE_KWJ_CAL_P_OS_DESC_INDEX   (0x18u) /* Index of P OS descriptor */
#define CYBLE_KWJ_CAL_BAT_RAW_DESC_INDEX   (0x19u) /* Index of BAT Raw descriptor */
#define CYBLE_KWJ_CAL_BAT_GAIN_DESC_INDEX   (0x1Au) /* Index of BAT Gain descriptor */
#define CYBLE_KWJ_CAL_BAT_OS_DESC_INDEX   (0x1Bu) /* Index of BAT OS descriptor */
#define CYBLE_KWJ_CAL_BAT_LO_DESC_INDEX   (0x1Cu) /* Index of BAT LO descriptor */
#define CYBLE_KWJ_CAL_BAT_CRIT_DESC_INDEX   (0x1Du) /* Index of BAT CRIT descriptor */
#define CYBLE_KWJ_CAL_CLIENT_CHARACTERISTIC_CONFIGURATION_DESC_INDEX   (0x1Eu) /* Index of Client Characteristic Configuration descriptor */
#define CYBLE_KWJ_CAL_CHARACTERISTIC_USER_DESCRIPTION_DESC_INDEX   (0x1Fu) /* Index of Characteristic User Description descriptor */
#define CYBLE_KWJ_CAL_CURRENT_TIME_DESC_INDEX   (0x20u) /* Index of Current Time descriptor */


#define CYBLE_KWJ_SERVICE_HANDLE   (0x000Eu) /* Handle of KWJ service */
#define CYBLE_KWJ_CO_DECL_HANDLE   (0x0010u) /* Handle of CO characteristic declaration */
#define CYBLE_KWJ_CO_CHAR_HANDLE   (0x0011u) /* Handle of CO characteristic */
#define CYBLE_KWJ_CO_MEASUREMENT_DESC_HANDLE   (0x0012u) /* Handle of Measurement descriptor */
#define CYBLE_KWJ_CO_CLIENT_CHARACTERISTIC_CONFIGURATION_DESC_HANDLE   (0x0013u) /* Handle of Client Characteristic Configuration descriptor */
#define CYBLE_KWJ_CO_UNITS_DESC_HANDLE   (0x0014u) /* Handle of Units descriptor */
#define CYBLE_KWJ_CO_CHARACTERISTIC_USER_DESCRIPTION_DESC_HANDLE   (0x0015u) /* Handle of Characteristic User Description descriptor */
#define CYBLE_KWJ_O3_DECL_HANDLE   (0x0016u) /* Handle of O3 characteristic declaration */
#define CYBLE_KWJ_O3_CHAR_HANDLE   (0x0017u) /* Handle of O3 characteristic */
#define CYBLE_KWJ_O3_MEASUREMENT_DESC_HANDLE   (0x0018u) /* Handle of Measurement descriptor */
#define CYBLE_KWJ_O3_CLIENT_CHARACTERISTIC_CONFIGURATION_DESC_HANDLE   (0x0019u) /* Handle of Client Characteristic Configuration descriptor */
#define CYBLE_KWJ_O3_UNITS_DESC_HANDLE   (0x001Au) /* Handle of Units descriptor */
#define CYBLE_KWJ_O3_CHARACTERISTIC_USER_DESCRIPTION_DESC_HANDLE   (0x001Bu) /* Handle of Characteristic User Description descriptor */
#define CYBLE_KWJ_LOG_DECL_HANDLE   (0x001Cu) /* Handle of LOG characteristic declaration */
#define CYBLE_KWJ_LOG_CHAR_HANDLE   (0x001Du) /* Handle of LOG characteristic */
#define CYBLE_KWJ_LOG_CLIENT_CHARACTERISTIC_CONFIGURATION_DESC_HANDLE   (0x001Eu) /* Handle of Client Characteristic Configuration descriptor */
#define CYBLE_KWJ_LOG_TIMESTAMP_DESC_HANDLE   (0x001Fu) /* Handle of Timestamp descriptor */
#define CYBLE_KWJ_LOG_BASE16_DESC_HANDLE   (0x0020u) /* Handle of Base16 descriptor */
#define CYBLE_KWJ_LOG_BASE32_DESC_HANDLE   (0x0021u) /* Handle of Base32 descriptor */
#define CYBLE_KWJ_LOG_TYPE_DESC_HANDLE   (0x0022u) /* Handle of Type descriptor */
#define CYBLE_KWJ_LOG_LENGTH_DESC_HANDLE   (0x0023u) /* Handle of Length descriptor */
#define CYBLE_KWJ_LOG_DATA_DESC_HANDLE   (0x0024u) /* Handle of Data descriptor */
#define CYBLE_KWJ_LOG_CHARACTERISTIC_USER_DESCRIPTION_DESC_HANDLE   (0x0025u) /* Handle of Characteristic User Description descriptor */
#define CYBLE_KWJ_CAL_DECL_HANDLE   (0x0026u) /* Handle of CAL characteristic declaration */
#define CYBLE_KWJ_CAL_CHAR_HANDLE   (0x0027u) /* Handle of CAL characteristic */
#define CYBLE_KWJ_CAL_CO_RAW_DESC_HANDLE   (0x0028u) /* Handle of CO Raw descriptor */
#define CYBLE_KWJ_CAL_CO_GAIN_DESC_HANDLE   (0x0029u) /* Handle of CO Gain descriptor */
#define CYBLE_KWJ_CAL_CO_OS_DESC_HANDLE   (0x002Au) /* Handle of CO OS descriptor */
#define CYBLE_KWJ_CAL_CO_TZERO_TC_DESC_HANDLE   (0x002Bu) /* Handle of CO TZero TC descriptor */
#define CYBLE_KWJ_CAL_CO_SNSFAC_DESC_HANDLE   (0x002Cu) /* Handle of CO SnsFac descriptor */
#define CYBLE_KWJ_CAL_CO_MOD_DESC_HANDLE   (0x002Du) /* Handle of CO MOD descriptor */
#define CYBLE_KWJ_CAL_CO_UNH_DESC_HANDLE   (0x002Eu) /* Handle of CO UNH descriptor */
#define CYBLE_KWJ_CAL_CO_HAZ_DESC_HANDLE   (0x002Fu) /* Handle of CO HAZ descriptor */
#define CYBLE_KWJ_CAL_O3_RAW_DESC_HANDLE   (0x0030u) /* Handle of O3 Raw descriptor */
#define CYBLE_KWJ_CAL_O3_GAIN_DESC_HANDLE   (0x0031u) /* Handle of O3 Gain descriptor */
#define CYBLE_KWJ_CAL_O3_OS_DESC_HANDLE   (0x0032u) /* Handle of O3 OS descriptor */
#define CYBLE_KWJ_CAL_O3_TZERO_TC_DESC_HANDLE   (0x0033u) /* Handle of O3 TZero TC descriptor */
#define CYBLE_KWJ_CAL_O3_SNSFAC_DESC_HANDLE   (0x0034u) /* Handle of O3 SnsFac descriptor */
#define CYBLE_KWJ_CAL_O3_MOD_DESC_HANDLE   (0x0035u) /* Handle of O3 MOD descriptor */
#define CYBLE_KWJ_CAL_O3_UNH_DESC_HANDLE   (0x0036u) /* Handle of O3 UNH descriptor */
#define CYBLE_KWJ_CAL_O3_HAZ_DESC_HANDLE   (0x0037u) /* Handle of O3 HAZ descriptor */
#define CYBLE_KWJ_CAL_T_RAW_DESC_HANDLE   (0x0038u) /* Handle of T Raw descriptor */
#define CYBLE_KWJ_CAL_T_GAIN_DESC_HANDLE   (0x0039u) /* Handle of T Gain descriptor */
#define CYBLE_KWJ_CAL_T_OS_DESC_HANDLE   (0x003Au) /* Handle of T OS descriptor */
#define CYBLE_KWJ_CAL_RH_RAW_DESC_HANDLE   (0x003Bu) /* Handle of RH Raw descriptor */
#define CYBLE_KWJ_CAL_RH_GAIN_DESC_HANDLE   (0x003Cu) /* Handle of RH Gain descriptor */
#define CYBLE_KWJ_CAL_RH_OS_DESC_HANDLE   (0x003Du) /* Handle of RH OS descriptor */
#define CYBLE_KWJ_CAL_P_RAW_DESC_HANDLE   (0x003Eu) /* Handle of P Raw descriptor */
#define CYBLE_KWJ_CAL_P_GAIN_DESC_HANDLE   (0x003Fu) /* Handle of P Gain descriptor */
#define CYBLE_KWJ_CAL_P_OS_DESC_HANDLE   (0x0040u) /* Handle of P OS descriptor */
#define CYBLE_KWJ_CAL_BAT_RAW_DESC_HANDLE   (0x0041u) /* Handle of BAT Raw descriptor */
#define CYBLE_KWJ_CAL_BAT_GAIN_DESC_HANDLE   (0x0042u) /* Handle of BAT Gain descriptor */
#define CYBLE_KWJ_CAL_BAT_OS_DESC_HANDLE   (0x0043u) /* Handle of BAT OS descriptor */
#define CYBLE_KWJ_CAL_BAT_LO_DESC_HANDLE   (0x0044u) /* Handle of BAT LO descriptor */
#define CYBLE_KWJ_CAL_BAT_CRIT_DESC_HANDLE   (0x0045u) /* Handle of BAT CRIT descriptor */
#define CYBLE_KWJ_CAL_CLIENT_CHARACTERISTIC_CONFIGURATION_DESC_HANDLE   (0x0046u) /* Handle of Client Characteristic Configuration descriptor */
#define CYBLE_KWJ_CAL_CHARACTERISTIC_USER_DESCRIPTION_DESC_HANDLE   (0x0047u) /* Handle of Characteristic User Description descriptor */
#define CYBLE_KWJ_CAL_CURRENT_TIME_DESC_HANDLE   (0x0048u) /* Handle of Current Time descriptor */



#if(CYBLE_CUSTOMS_SERVICE_COUNT != 0u)
    #define CYBLE_CUSTOM_SERVER
#endif /* (CYBLE_CUSTOMS_SERVICE_COUNT != 0u) */
    
#if(CYBLE_CUSTOMC_SERVICE_COUNT != 0u)
    #define CYBLE_CUSTOM_CLIENT
#endif /* (CYBLE_CUSTOMC_SERVICE_COUNT != 0u) */

/***************************************
* Data Struct Definition
***************************************/

/**
 \addtogroup group_service_api_custom
 @{
*/

#ifdef CYBLE_CUSTOM_SERVER

/** Contains information about Custom Characteristic structure */
typedef struct
{
    /** Custom Characteristic handle */
    CYBLE_GATT_DB_ATTR_HANDLE_T customServCharHandle;
    /** Custom Characteristic Descriptors handles */
    CYBLE_GATT_DB_ATTR_HANDLE_T customServCharDesc[     /* MDK doesn't allow array with zero length */
        CYBLE_CUSTOM_SERVICE_CHAR_DESCRIPTORS_COUNT == 0u ? 1u : CYBLE_CUSTOM_SERVICE_CHAR_DESCRIPTORS_COUNT];
} CYBLE_CUSTOMS_INFO_T;

/** Structure with Custom Service attribute handles. */
typedef struct
{
    /** Handle of a Custom Service */
    CYBLE_GATT_DB_ATTR_HANDLE_T customServHandle;
    
    /** Information about Custom Characteristics */
    CYBLE_CUSTOMS_INFO_T customServInfo[                /* MDK doesn't allow array with zero length */
        CYBLE_CUSTOM_SERVICE_CHAR_COUNT == 0u ? 1u : CYBLE_CUSTOM_SERVICE_CHAR_COUNT];
} CYBLE_CUSTOMS_T;


#endif /* (CYBLE_CUSTOM_SERVER) */

/** @} */

/** \cond IGNORE */
/* The custom Client functionality is not functional in current version of 
* the component.
*/
#ifdef CYBLE_CUSTOM_CLIENT

typedef struct
{
    /** Custom Descriptor handle */
    CYBLE_GATT_DB_ATTR_HANDLE_T descHandle;
	/** Custom Descriptor 128 bit UUID */
	const void *uuid;           
    /** UUID Format - 16-bit (0x01) or 128-bit (0x02) */
	uint8 uuidFormat;
   
} CYBLE_CUSTOMC_DESC_T;

typedef struct
{
    /** Characteristic handle */
    CYBLE_GATT_DB_ATTR_HANDLE_T customServCharHandle;
	/** Characteristic end handle */
    CYBLE_GATT_DB_ATTR_HANDLE_T customServCharEndHandle;
	/** Custom Characteristic UUID */
	const void *uuid;           
    /** UUID Format - 16-bit (0x01) or 128-bit (0x02) */
	uint8 uuidFormat;
    /** Properties for value field */
    uint8  properties;
	/** Number of descriptors */
    uint8 descCount;
    /** Characteristic Descriptors */
    CYBLE_CUSTOMC_DESC_T * customServCharDesc;
} CYBLE_CUSTOMC_CHAR_T;

/** Structure with discovered attributes information of Custom Service */
typedef struct
{
    /** Custom Service handle */
    CYBLE_GATT_DB_ATTR_HANDLE_T customServHandle;
	/** Custom Service UUID */
	const void *uuid;           
    /** UUID Format - 16-bit (0x01) or 128-bit (0x02) */
	uint8 uuidFormat;
	/** Number of characteristics */
    uint8 charCount;
    /** Custom Service Characteristics */
    CYBLE_CUSTOMC_CHAR_T * customServChar;
} CYBLE_CUSTOMC_T;

#endif /* (CYBLE_CUSTOM_CLIENT) */
/** \endcond */

#ifdef CYBLE_CUSTOM_SERVER

extern const CYBLE_CUSTOMS_T cyBle_customs[CYBLE_CUSTOMS_SERVICE_COUNT];

#endif /* (CYBLE_CUSTOM_SERVER) */

/** \cond IGNORE */
#ifdef CYBLE_CUSTOM_CLIENT

extern CYBLE_CUSTOMC_T cyBle_customc[CYBLE_CUSTOMC_SERVICE_COUNT];

#endif /* (CYBLE_CUSTOM_CLIENT) */
/** \endcond */


/***************************************
* Private Function Prototypes
***************************************/

/** \cond IGNORE */
void CyBle_CustomInit(void);

#ifdef CYBLE_CUSTOM_CLIENT

void CyBle_CustomcDiscoverServiceEventHandler(const CYBLE_DISC_SRVC128_INFO_T *discServInfo);
void CyBle_CustomcDiscoverCharacteristicsEventHandler(uint16 discoveryService, const CYBLE_DISC_CHAR_INFO_T *discCharInfo);
CYBLE_GATT_ATTR_HANDLE_RANGE_T CyBle_CustomcGetCharRange(uint8 incrementIndex);
void CyBle_CustomcDiscoverCharDescriptorsEventHandler(const CYBLE_DISC_DESCR_INFO_T *discDescrInfo);

#endif /* (CYBLE_CUSTOM_CLIENT) */

/** \endcond */

/***************************************
* External data references 
***************************************/

#ifdef CYBLE_CUSTOM_CLIENT

extern CYBLE_CUSTOMC_T cyBle_customCServ[CYBLE_CUSTOMC_SERVICE_COUNT];

#endif /* (CYBLE_CUSTOM_CLIENT) */


/** \cond IGNORE */
/***************************************
* The following code is DEPRECATED and
* should not be used in new projects.
***************************************/
#define customServiceCharHandle         customServCharHandle
#define customServiceCharDescriptors    customServCharDesc
#define customServiceHandle             customServHandle
#define customServiceInfo               customServInfo
/** \endcond */


#endif /* CY_BLE_CYBLE_CUSTOM_H  */

/* [] END OF FILE */
