/*******************************************************************************
* File Name: ENA_REG.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_ENA_REG_H) /* Pins ENA_REG_H */
#define CY_PINS_ENA_REG_H

#include "cytypes.h"
#include "cyfitter.h"
#include "ENA_REG_aliases.h"


/***************************************
*     Data Struct Definitions
***************************************/

/**
* \addtogroup group_structures
* @{
*/
    
/* Structure for sleep mode support */
typedef struct
{
    uint32 pcState; /**< State of the port control register */
    uint32 sioState; /**< State of the SIO configuration */
    uint32 usbState; /**< State of the USBIO regulator */
} ENA_REG_BACKUP_STRUCT;

/** @} structures */


/***************************************
*        Function Prototypes             
***************************************/
/**
* \addtogroup group_general
* @{
*/
uint8   ENA_REG_Read(void);
void    ENA_REG_Write(uint8 value);
uint8   ENA_REG_ReadDataReg(void);
#if defined(ENA_REG__PC) || (CY_PSOC4_4200L) 
    void    ENA_REG_SetDriveMode(uint8 mode);
#endif
void    ENA_REG_SetInterruptMode(uint16 position, uint16 mode);
uint8   ENA_REG_ClearInterrupt(void);
/** @} general */

/**
* \addtogroup group_power
* @{
*/
void ENA_REG_Sleep(void); 
void ENA_REG_Wakeup(void);
/** @} power */


/***************************************
*           API Constants        
***************************************/
#if defined(ENA_REG__PC) || (CY_PSOC4_4200L) 
    /* Drive Modes */
    #define ENA_REG_DRIVE_MODE_BITS        (3)
    #define ENA_REG_DRIVE_MODE_IND_MASK    (0xFFFFFFFFu >> (32 - ENA_REG_DRIVE_MODE_BITS))

    /**
    * \addtogroup group_constants
    * @{
    */
        /** \addtogroup driveMode Drive mode constants
         * \brief Constants to be passed as "mode" parameter in the ENA_REG_SetDriveMode() function.
         *  @{
         */
        #define ENA_REG_DM_ALG_HIZ         (0x00u) /**< \brief High Impedance Analog   */
        #define ENA_REG_DM_DIG_HIZ         (0x01u) /**< \brief High Impedance Digital  */
        #define ENA_REG_DM_RES_UP          (0x02u) /**< \brief Resistive Pull Up       */
        #define ENA_REG_DM_RES_DWN         (0x03u) /**< \brief Resistive Pull Down     */
        #define ENA_REG_DM_OD_LO           (0x04u) /**< \brief Open Drain, Drives Low  */
        #define ENA_REG_DM_OD_HI           (0x05u) /**< \brief Open Drain, Drives High */
        #define ENA_REG_DM_STRONG          (0x06u) /**< \brief Strong Drive            */
        #define ENA_REG_DM_RES_UPDWN       (0x07u) /**< \brief Resistive Pull Up/Down  */
        /** @} driveMode */
    /** @} group_constants */
#endif

/* Digital Port Constants */
#define ENA_REG_MASK               ENA_REG__MASK
#define ENA_REG_SHIFT              ENA_REG__SHIFT
#define ENA_REG_WIDTH              1u

/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in ENA_REG_SetInterruptMode() function.
     *  @{
     */
        #define ENA_REG_INTR_NONE      ((uint16)(0x0000u)) /**< \brief Disabled             */
        #define ENA_REG_INTR_RISING    ((uint16)(0x5555u)) /**< \brief Rising edge trigger  */
        #define ENA_REG_INTR_FALLING   ((uint16)(0xaaaau)) /**< \brief Falling edge trigger */
        #define ENA_REG_INTR_BOTH      ((uint16)(0xffffu)) /**< \brief Both edge trigger    */
    /** @} intrMode */
/** @} group_constants */

/* SIO LPM definition */
#if defined(ENA_REG__SIO)
    #define ENA_REG_SIO_LPM_MASK       (0x03u)
#endif

/* USBIO definitions */
#if !defined(ENA_REG__PC) && (CY_PSOC4_4200L)
    #define ENA_REG_USBIO_ENABLE               ((uint32)0x80000000u)
    #define ENA_REG_USBIO_DISABLE              ((uint32)(~ENA_REG_USBIO_ENABLE))
    #define ENA_REG_USBIO_SUSPEND_SHIFT        CYFLD_USBDEVv2_USB_SUSPEND__OFFSET
    #define ENA_REG_USBIO_SUSPEND_DEL_SHIFT    CYFLD_USBDEVv2_USB_SUSPEND_DEL__OFFSET
    #define ENA_REG_USBIO_ENTER_SLEEP          ((uint32)((1u << ENA_REG_USBIO_SUSPEND_SHIFT) \
                                                        | (1u << ENA_REG_USBIO_SUSPEND_DEL_SHIFT)))
    #define ENA_REG_USBIO_EXIT_SLEEP_PH1       ((uint32)~((uint32)(1u << ENA_REG_USBIO_SUSPEND_SHIFT)))
    #define ENA_REG_USBIO_EXIT_SLEEP_PH2       ((uint32)~((uint32)(1u << ENA_REG_USBIO_SUSPEND_DEL_SHIFT)))
    #define ENA_REG_USBIO_CR1_OFF              ((uint32)0xfffffffeu)
#endif


/***************************************
*             Registers        
***************************************/
/* Main Port Registers */
#if defined(ENA_REG__PC)
    /* Port Configuration */
    #define ENA_REG_PC                 (* (reg32 *) ENA_REG__PC)
#endif
/* Pin State */
#define ENA_REG_PS                     (* (reg32 *) ENA_REG__PS)
/* Data Register */
#define ENA_REG_DR                     (* (reg32 *) ENA_REG__DR)
/* Input Buffer Disable Override */
#define ENA_REG_INP_DIS                (* (reg32 *) ENA_REG__PC2)

/* Interrupt configuration Registers */
#define ENA_REG_INTCFG                 (* (reg32 *) ENA_REG__INTCFG)
#define ENA_REG_INTSTAT                (* (reg32 *) ENA_REG__INTSTAT)

/* "Interrupt cause" register for Combined Port Interrupt (AllPortInt) in GSRef component */
#if defined (CYREG_GPIO_INTR_CAUSE)
    #define ENA_REG_INTR_CAUSE         (* (reg32 *) CYREG_GPIO_INTR_CAUSE)
#endif

/* SIO register */
#if defined(ENA_REG__SIO)
    #define ENA_REG_SIO_REG            (* (reg32 *) ENA_REG__SIO)
#endif /* (ENA_REG__SIO_CFG) */

/* USBIO registers */
#if !defined(ENA_REG__PC) && (CY_PSOC4_4200L)
    #define ENA_REG_USB_POWER_REG       (* (reg32 *) CYREG_USBDEVv2_USB_POWER_CTRL)
    #define ENA_REG_CR1_REG             (* (reg32 *) CYREG_USBDEVv2_CR1)
    #define ENA_REG_USBIO_CTRL_REG      (* (reg32 *) CYREG_USBDEVv2_USB_USBIO_CTRL)
#endif    
    
    
/***************************************
* The following code is DEPRECATED and 
* must not be used in new designs.
***************************************/
/**
* \addtogroup group_deprecated
* @{
*/
#define ENA_REG_DRIVE_MODE_SHIFT       (0x00u)
#define ENA_REG_DRIVE_MODE_MASK        (0x07u << ENA_REG_DRIVE_MODE_SHIFT)
/** @} deprecated */

#endif /* End Pins ENA_REG_H */


/* [] END OF FILE */
