/***************************************************************************//**
* \file CYBLE_gatt.c
* \version 3.10
* 
* \brief
*  This file contains the source code for the GATT API of the BLE Component.
* 
********************************************************************************
* \copyright
* Copyright 2014-2016, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/


#include "BLE_1_eventHandler.h"


/***************************************
* Global variables
***************************************/

CYBLE_STATE_T cyBle_state;

#if ((CYBLE_MODE_PROFILE) && (CYBLE_BONDING_REQUIREMENT == CYBLE_BONDING_YES))
    
#if(CYBLE_MODE_PROFILE)
    #if defined(__ARMCC_VERSION)
        CY_ALIGN(CYDEV_FLS_ROW_SIZE) const CY_BLE_FLASH_STORAGE cyBle_flashStorage CY_SECTION(".cy_checksum_exclude") =
    #elif defined (__GNUC__)
        const CY_BLE_FLASH_STORAGE cyBle_flashStorage CY_SECTION(".cy_checksum_exclude")
            CY_ALIGN(CYDEV_FLS_ROW_SIZE) =
    #elif defined (__ICCARM__)
        #pragma data_alignment=CY_FLASH_SIZEOF_ROW
        #pragma location=".cy_checksum_exclude"
        const CY_BLE_FLASH_STORAGE cyBle_flashStorage =
    #endif  /* (__ARMCC_VERSION) */
    {
        { 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u }, 
        {{
            0x00u, 0x00u, 
            0x00u, 0x00u, 
            0x00u, 0x00u, 
        },
        {
            0x00u, 0x00u, 
            0x00u, 0x00u, 
            0x00u, 0x00u, 
        },
        {
            0x00u, 0x00u, 
            0x00u, 0x00u, 
            0x00u, 0x00u, 
        },
        {
            0x00u, 0x00u, 
            0x00u, 0x00u, 
            0x00u, 0x00u, 
        },
        {
            0x00u, 0x00u, 
            0x00u, 0x00u, 
            0x00u, 0x00u, 
        }}, 
        0x06u, /* CYBLE_GATT_DB_CCCD_COUNT */ 
        0x05u, /* CYBLE_GAP_MAX_BONDED_DEVICE */ 
    };
#endif /* (CYBLE_MODE_PROFILE) */

#endif  /* (CYBLE_MODE_PROFILE) && (CYBLE_BONDING_REQUIREMENT == CYBLE_BONDING_YES) */

#if(CYBLE_GATT_ROLE_SERVER)
    
    const CYBLE_GATTS_T cyBle_gatts =
{
    0x000Au,    /* Handle of the GATT service */
    0x000Cu,    /* Handle of the Service Changed characteristic */
    0x000Du,    /* Handle of the Client Characteristic Configuration descriptor */
};
    
    static uint8 cyBle_attValues[0x01A5u] = {
    /* Device Name */
    

    /* Appearance */
    0x00u, 0x00u, 

    /* Peripheral Preferred Connection Parameters */
    0x06u, 0x00u, 0x28u, 0x00u, 0x00u, 0x00u, 0xE8u, 0x03u, 

    /* Central Address Resolution */
    0x00u, 

    /* Service Changed */
    0x00u, 0x00u, 0x00u, 0x00u, 

    /* Descriptor Value Changed */
    0x00u, 0x00u, 

    /* Apparent Wind Direction */
    0x00u, 0x00u, 

    /* Environmental Sensing Measurement */
    0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 

    /* Environmental Sensing Trigger Setting */
    0x00u, 0x00u, 

    /* Characteristic User Description */
    

    /* Valid Range */
    0x00u, 0x00u, 0x9Fu, 0x8Cu, 

    /* Apparent Wind Speed */
    0x00u, 0x00u, 

    /* Environmental Sensing Measurement */
    0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 

    /* Environmental Sensing Trigger Setting */
    0x00u, 0x00u, 

    /* Characteristic User Description */
    

    /* Valid Range */
    0x00u, 0x00u, 0xFFu, 0xFFu, 

    /* Dew Point */
    0x00u, 

    /* Environmental Sensing Measurement */
    0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 

    /* Environmental Sensing Trigger Setting */
    0x00u, 0x00u, 

    /* Characteristic User Description */
    

    /* Valid Range */
    0x80u, 0x7Fu, 

    /* Elevation */
    0x00u, 0x00u, 0x00u, 

    /* Environmental Sensing Measurement */
    0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 

    /* Environmental Sensing Trigger Setting */
    0x00u, 0x00u, 

    /* Characteristic User Description */
    

    /* Valid Range */
    0x00u, 0x00u, 0x80u, 0xFFu, 0xFFu, 0x7Fu, 

    /* Gust Factor */
    0x00u, 

    /* Environmental Sensing Measurement */
    0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 

    /* Environmental Sensing Trigger Setting */
    0x00u, 0x00u, 

    /* Characteristic User Description */
    

    /* Valid Range */
    0x00u, 0xFFu, 

    /* Heat Index */
    0x00u, 

    /* Environmental Sensing Measurement */
    0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 

    /* Environmental Sensing Trigger Setting */
    0x00u, 0x00u, 

    /* Characteristic User Description */
    

    /* Valid Range */
    0x80u, 0x7Fu, 

    /* Humidity */
    0x00u, 0x00u, 

    /* Environmental Sensing Measurement */
    0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 

    /* Environmental Sensing Trigger Setting */
    0x00u, 0x00u, 

    /* Characteristic User Description */
    

    /* Valid Range */
    0x00u, 0x00u, 0xFFu, 0xFFu, 

    /* Irradiance */
    0x00u, 0x00u, 

    /* Environmental Sensing Measurement */
    0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 

    /* Environmental Sensing Trigger Setting */
    0x00u, 0x00u, 

    /* Characteristic User Description */
    

    /* Valid Range */
    0x00u, 0x00u, 0xFFu, 0xFFu, 

    /* Pollen Concentration */
    0x00u, 0x00u, 0x00u, 

    /* Environmental Sensing Measurement */
    0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 

    /* Environmental Sensing Trigger Setting */
    0x00u, 0x00u, 

    /* Characteristic User Description */
    

    /* Valid Range */
    0x00u, 0x00u, 0x00u, 0xFFu, 0xFFu, 0xFFu, 

    /* Rainfall */
    0x00u, 0x00u, 

    /* Environmental Sensing Measurement */
    0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 

    /* Environmental Sensing Trigger Setting */
    0x00u, 0x00u, 

    /* Characteristic User Description */
    

    /* Valid Range */
    0x00u, 0x00u, 0xFFu, 0xFFu, 

    /* Pressure */
    0x00u, 0x00u, 0x00u, 0x00u, 

    /* Environmental Sensing Measurement */
    0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 

    /* Environmental Sensing Trigger Setting */
    0x00u, 0x00u, 

    /* Characteristic User Description */
    

    /* Valid Range */
    0x00u, 0x00u, 0x00u, 0x00u, 0xFFu, 0xFFu, 0xFFu, 0xFFu, 

    /* Temperature */
    0x00u, 0x00u, 

    /* Environmental Sensing Measurement */
    0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 

    /* Environmental Sensing Trigger Setting */
    0x00u, 0x00u, 

    /* Characteristic User Description */
    

    /* Valid Range */
    0x00u, 0x80u, 0xFFu, 0x7Fu, 

    /* True Wind Direction */
    0x00u, 0x00u, 

    /* Environmental Sensing Measurement */
    0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 

    /* Environmental Sensing Trigger Setting */
    0x00u, 0x00u, 

    /* Characteristic User Description */
    

    /* Valid Range */
    0x00u, 0x00u, 0x9Fu, 0x8Cu, 

    /* True Wind Speed */
    0x00u, 0x00u, 

    /* Environmental Sensing Measurement */
    0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 

    /* Environmental Sensing Trigger Setting */
    0x00u, 0x00u, 

    /* Characteristic User Description */
    

    /* Valid Range */
    0x00u, 0x00u, 0xFFu, 0xFFu, 

    /* UV Index */
    0x00u, 

    /* Environmental Sensing Measurement */
    0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 

    /* Environmental Sensing Trigger Setting */
    0x00u, 0x00u, 

    /* Characteristic User Description */
    

    /* Valid Range */
    0x00u, 0xFFu, 

    /* Wind Chill */
    0x00u, 

    /* Environmental Sensing Measurement */
    0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 

    /* Environmental Sensing Trigger Setting */
    0x00u, 0x00u, 

    /* Characteristic User Description */
    

    /* Valid Range */
    0x80u, 0x7Fu, 

    /* Barometric Pressure Trend */
    0x00u, 

    /* Environmental Sensing Measurement */
    0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 

    /* Environmental Sensing Trigger Setting */
    0x00u, 0x00u, 

    /* Characteristic User Description */
    

    /* Valid Range */
    0x00u, 0xFFu, 

    /* Magnetic Declination */
    0x00u, 0x00u, 

    /* Environmental Sensing Measurement */
    0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 

    /* Environmental Sensing Trigger Setting */
    0x00u, 0x00u, 

    /* Characteristic User Description */
    

    /* Valid Range */
    0x00u, 0x00u, 0x9Fu, 0x8Cu, 

    /* Magnetic Flux Density - 2D */
    0x00u, 0x00u, 0x00u, 0x00u, 

    /* Environmental Sensing Measurement */
    0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 

    /* Environmental Sensing Trigger Setting */
    0x00u, 0x00u, 

    /* Characteristic User Description */
    

    /* Valid Range */
    0x00u, 0x80u, 0xFFu, 0x7Fu, 

    /* Magnetic Flux Density - 3D */
    0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 

    /* Environmental Sensing Measurement */
    0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 

    /* Environmental Sensing Trigger Setting */
    0x00u, 0x00u, 

    /* Characteristic User Description */
    

    /* Valid Range */
    0x00u, 0x80u, 0xFFu, 0x7Fu, 

    /* Manufacturer Name String */
    

    /* Model Number String */
    

    /* Serial Number String */
    

    /* Hardware Revision String */
    

    /* Firmware Revision String */
    

    /* Software Revision String */
    

    /* System ID */
    0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 

    /* IEEE 11073-20601 Regulatory Certification Data List */
    0x00u, 

    /* PnP ID */
    0x01u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 

    /* Battery Level */
    0x00u, 

    /* Characteristic Presentation Format */
    0x00u, 0x00u, 0x33u, 0x27u, 0x01u, 0x00u, 0x00u, 

};
#if(CYBLE_GATT_DB_CCCD_COUNT != 0u)
uint8 cyBle_attValuesCCCD[CYBLE_GATT_DB_CCCD_COUNT];
#endif /* CYBLE_GATT_DB_CCCD_COUNT != 0u */

CYBLE_GATTS_ATT_GEN_VAL_LEN_T cyBle_attValuesLen[CYBLE_GATT_DB_ATT_VAL_COUNT] = {
    { 0x0000u, (void *)&cyBle_attValues[0] }, /* Device Name */
    { 0x0002u, (void *)&cyBle_attValues[0] }, /* Appearance */
    { 0x0008u, (void *)&cyBle_attValues[2] }, /* Peripheral Preferred Connection Parameters */
    { 0x0001u, (void *)&cyBle_attValues[10] }, /* Central Address Resolution */
    { 0x0004u, (void *)&cyBle_attValues[11] }, /* Service Changed */
    { 0x0002u, (void *)&cyBle_attValuesCCCD[0] }, /* Client Characteristic Configuration */
    { 0x0002u, (void *)&cyBle_attValues[15] }, /* Descriptor Value Changed */
    { 0x0002u, (void *)&cyBle_attValuesCCCD[2] }, /* Client Characteristic Configuration */
    { 0x0002u, (void *)&cyBle_attValues[17] }, /* Apparent Wind Direction */
    { 0x000Bu, (void *)&cyBle_attValues[19] }, /* Environmental Sensing Measurement */
    { 0x0002u, (void *)&cyBle_attValues[30] }, /* Environmental Sensing Trigger Setting */
    { 0x0000u, (void *)&cyBle_attValues[32] }, /* Characteristic User Description */
    { 0x0004u, (void *)&cyBle_attValues[32] }, /* Valid Range */
    { 0x0002u, (void *)&cyBle_attValues[36] }, /* Apparent Wind Speed */
    { 0x000Bu, (void *)&cyBle_attValues[38] }, /* Environmental Sensing Measurement */
    { 0x0002u, (void *)&cyBle_attValues[49] }, /* Environmental Sensing Trigger Setting */
    { 0x0000u, (void *)&cyBle_attValues[51] }, /* Characteristic User Description */
    { 0x0004u, (void *)&cyBle_attValues[51] }, /* Valid Range */
    { 0x0001u, (void *)&cyBle_attValues[55] }, /* Dew Point */
    { 0x000Bu, (void *)&cyBle_attValues[56] }, /* Environmental Sensing Measurement */
    { 0x0002u, (void *)&cyBle_attValues[67] }, /* Environmental Sensing Trigger Setting */
    { 0x0000u, (void *)&cyBle_attValues[69] }, /* Characteristic User Description */
    { 0x0002u, (void *)&cyBle_attValues[69] }, /* Valid Range */
    { 0x0003u, (void *)&cyBle_attValues[71] }, /* Elevation */
    { 0x000Bu, (void *)&cyBle_attValues[74] }, /* Environmental Sensing Measurement */
    { 0x0002u, (void *)&cyBle_attValues[85] }, /* Environmental Sensing Trigger Setting */
    { 0x0000u, (void *)&cyBle_attValues[87] }, /* Characteristic User Description */
    { 0x0006u, (void *)&cyBle_attValues[87] }, /* Valid Range */
    { 0x0001u, (void *)&cyBle_attValues[93] }, /* Gust Factor */
    { 0x000Bu, (void *)&cyBle_attValues[94] }, /* Environmental Sensing Measurement */
    { 0x0002u, (void *)&cyBle_attValues[105] }, /* Environmental Sensing Trigger Setting */
    { 0x0000u, (void *)&cyBle_attValues[107] }, /* Characteristic User Description */
    { 0x0002u, (void *)&cyBle_attValues[107] }, /* Valid Range */
    { 0x0001u, (void *)&cyBle_attValues[109] }, /* Heat Index */
    { 0x000Bu, (void *)&cyBle_attValues[110] }, /* Environmental Sensing Measurement */
    { 0x0002u, (void *)&cyBle_attValues[121] }, /* Environmental Sensing Trigger Setting */
    { 0x0000u, (void *)&cyBle_attValues[123] }, /* Characteristic User Description */
    { 0x0002u, (void *)&cyBle_attValues[123] }, /* Valid Range */
    { 0x0002u, (void *)&cyBle_attValues[125] }, /* Humidity */
    { 0x000Bu, (void *)&cyBle_attValues[127] }, /* Environmental Sensing Measurement */
    { 0x0002u, (void *)&cyBle_attValues[138] }, /* Environmental Sensing Trigger Setting */
    { 0x0000u, (void *)&cyBle_attValues[140] }, /* Characteristic User Description */
    { 0x0004u, (void *)&cyBle_attValues[140] }, /* Valid Range */
    { 0x0002u, (void *)&cyBle_attValues[144] }, /* Irradiance */
    { 0x000Bu, (void *)&cyBle_attValues[146] }, /* Environmental Sensing Measurement */
    { 0x0002u, (void *)&cyBle_attValues[157] }, /* Environmental Sensing Trigger Setting */
    { 0x0000u, (void *)&cyBle_attValues[159] }, /* Characteristic User Description */
    { 0x0004u, (void *)&cyBle_attValues[159] }, /* Valid Range */
    { 0x0003u, (void *)&cyBle_attValues[163] }, /* Pollen Concentration */
    { 0x000Bu, (void *)&cyBle_attValues[166] }, /* Environmental Sensing Measurement */
    { 0x0002u, (void *)&cyBle_attValues[177] }, /* Environmental Sensing Trigger Setting */
    { 0x0000u, (void *)&cyBle_attValues[179] }, /* Characteristic User Description */
    { 0x0006u, (void *)&cyBle_attValues[179] }, /* Valid Range */
    { 0x0002u, (void *)&cyBle_attValues[185] }, /* Rainfall */
    { 0x000Bu, (void *)&cyBle_attValues[187] }, /* Environmental Sensing Measurement */
    { 0x0002u, (void *)&cyBle_attValues[198] }, /* Environmental Sensing Trigger Setting */
    { 0x0000u, (void *)&cyBle_attValues[200] }, /* Characteristic User Description */
    { 0x0004u, (void *)&cyBle_attValues[200] }, /* Valid Range */
    { 0x0004u, (void *)&cyBle_attValues[204] }, /* Pressure */
    { 0x000Bu, (void *)&cyBle_attValues[208] }, /* Environmental Sensing Measurement */
    { 0x0002u, (void *)&cyBle_attValues[219] }, /* Environmental Sensing Trigger Setting */
    { 0x0000u, (void *)&cyBle_attValues[221] }, /* Characteristic User Description */
    { 0x0008u, (void *)&cyBle_attValues[221] }, /* Valid Range */
    { 0x0002u, (void *)&cyBle_attValues[229] }, /* Temperature */
    { 0x000Bu, (void *)&cyBle_attValues[231] }, /* Environmental Sensing Measurement */
    { 0x0002u, (void *)&cyBle_attValues[242] }, /* Environmental Sensing Trigger Setting */
    { 0x0000u, (void *)&cyBle_attValues[244] }, /* Characteristic User Description */
    { 0x0004u, (void *)&cyBle_attValues[244] }, /* Valid Range */
    { 0x0002u, (void *)&cyBle_attValues[248] }, /* True Wind Direction */
    { 0x000Bu, (void *)&cyBle_attValues[250] }, /* Environmental Sensing Measurement */
    { 0x0002u, (void *)&cyBle_attValues[261] }, /* Environmental Sensing Trigger Setting */
    { 0x0000u, (void *)&cyBle_attValues[263] }, /* Characteristic User Description */
    { 0x0004u, (void *)&cyBle_attValues[263] }, /* Valid Range */
    { 0x0002u, (void *)&cyBle_attValues[267] }, /* True Wind Speed */
    { 0x000Bu, (void *)&cyBle_attValues[269] }, /* Environmental Sensing Measurement */
    { 0x0002u, (void *)&cyBle_attValues[280] }, /* Environmental Sensing Trigger Setting */
    { 0x0000u, (void *)&cyBle_attValues[282] }, /* Characteristic User Description */
    { 0x0004u, (void *)&cyBle_attValues[282] }, /* Valid Range */
    { 0x0001u, (void *)&cyBle_attValues[286] }, /* UV Index */
    { 0x000Bu, (void *)&cyBle_attValues[287] }, /* Environmental Sensing Measurement */
    { 0x0002u, (void *)&cyBle_attValues[298] }, /* Environmental Sensing Trigger Setting */
    { 0x0000u, (void *)&cyBle_attValues[300] }, /* Characteristic User Description */
    { 0x0002u, (void *)&cyBle_attValues[300] }, /* Valid Range */
    { 0x0001u, (void *)&cyBle_attValues[302] }, /* Wind Chill */
    { 0x000Bu, (void *)&cyBle_attValues[303] }, /* Environmental Sensing Measurement */
    { 0x0002u, (void *)&cyBle_attValues[314] }, /* Environmental Sensing Trigger Setting */
    { 0x0000u, (void *)&cyBle_attValues[316] }, /* Characteristic User Description */
    { 0x0002u, (void *)&cyBle_attValues[316] }, /* Valid Range */
    { 0x0001u, (void *)&cyBle_attValues[318] }, /* Barometric Pressure Trend */
    { 0x000Bu, (void *)&cyBle_attValues[319] }, /* Environmental Sensing Measurement */
    { 0x0002u, (void *)&cyBle_attValues[330] }, /* Environmental Sensing Trigger Setting */
    { 0x0000u, (void *)&cyBle_attValues[332] }, /* Characteristic User Description */
    { 0x0002u, (void *)&cyBle_attValues[332] }, /* Valid Range */
    { 0x0002u, (void *)&cyBle_attValues[334] }, /* Magnetic Declination */
    { 0x000Bu, (void *)&cyBle_attValues[336] }, /* Environmental Sensing Measurement */
    { 0x0002u, (void *)&cyBle_attValues[347] }, /* Environmental Sensing Trigger Setting */
    { 0x0000u, (void *)&cyBle_attValues[349] }, /* Characteristic User Description */
    { 0x0004u, (void *)&cyBle_attValues[349] }, /* Valid Range */
    { 0x0004u, (void *)&cyBle_attValues[353] }, /* Magnetic Flux Density - 2D */
    { 0x000Bu, (void *)&cyBle_attValues[357] }, /* Environmental Sensing Measurement */
    { 0x0002u, (void *)&cyBle_attValues[368] }, /* Environmental Sensing Trigger Setting */
    { 0x0000u, (void *)&cyBle_attValues[370] }, /* Characteristic User Description */
    { 0x0004u, (void *)&cyBle_attValues[370] }, /* Valid Range */
    { 0x0006u, (void *)&cyBle_attValues[374] }, /* Magnetic Flux Density - 3D */
    { 0x000Bu, (void *)&cyBle_attValues[380] }, /* Environmental Sensing Measurement */
    { 0x0002u, (void *)&cyBle_attValues[391] }, /* Environmental Sensing Trigger Setting */
    { 0x0000u, (void *)&cyBle_attValues[393] }, /* Characteristic User Description */
    { 0x0004u, (void *)&cyBle_attValues[393] }, /* Valid Range */
    { 0x0000u, (void *)&cyBle_attValues[397] }, /* Manufacturer Name String */
    { 0x0000u, (void *)&cyBle_attValues[397] }, /* Model Number String */
    { 0x0000u, (void *)&cyBle_attValues[397] }, /* Serial Number String */
    { 0x0000u, (void *)&cyBle_attValues[397] }, /* Hardware Revision String */
    { 0x0000u, (void *)&cyBle_attValues[397] }, /* Firmware Revision String */
    { 0x0000u, (void *)&cyBle_attValues[397] }, /* Software Revision String */
    { 0x0008u, (void *)&cyBle_attValues[397] }, /* System ID */
    { 0x0001u, (void *)&cyBle_attValues[405] }, /* IEEE 11073-20601 Regulatory Certification Data List */
    { 0x0007u, (void *)&cyBle_attValues[406] }, /* PnP ID */
    { 0x0001u, (void *)&cyBle_attValues[413] }, /* Battery Level */
    { 0x0007u, (void *)&cyBle_attValues[414] }, /* Characteristic Presentation Format */
    { 0x0002u, (void *)&cyBle_attValuesCCCD[4] }, /* Client Characteristic Configuration */
};

const CYBLE_GATTS_DB_T cyBle_gattDB[0xA1u] = {
    { 0x0001u, 0x2800u /* Primary service                     */, 0x00000001u /*       */, 0x0009u, {{0x1800u, NULL}}                           },
    { 0x0002u, 0x2803u /* Characteristic                      */, 0x00000201u /* rd    */, 0x0003u, {{0x2A00u, NULL}}                           },
    { 0x0003u, 0x2A00u /* Device Name                         */, 0x00000201u /* rd    */, 0x0003u, {{0x0000u, (void *)&cyBle_attValuesLen[0]}} },
    { 0x0004u, 0x2803u /* Characteristic                      */, 0x00000201u /* rd    */, 0x0005u, {{0x2A01u, NULL}}                           },
    { 0x0005u, 0x2A01u /* Appearance                          */, 0x00000201u /* rd    */, 0x0005u, {{0x0002u, (void *)&cyBle_attValuesLen[1]}} },
    { 0x0006u, 0x2803u /* Characteristic                      */, 0x00000201u /* rd    */, 0x0007u, {{0x2A04u, NULL}}                           },
    { 0x0007u, 0x2A04u /* Peripheral Preferred Connection Par */, 0x00000201u /* rd    */, 0x0007u, {{0x0008u, (void *)&cyBle_attValuesLen[2]}} },
    { 0x0008u, 0x2803u /* Characteristic                      */, 0x00000201u /* rd    */, 0x0009u, {{0x2AA6u, NULL}}                           },
    { 0x0009u, 0x2AA6u /* Central Address Resolution          */, 0x00000201u /* rd    */, 0x0009u, {{0x0001u, (void *)&cyBle_attValuesLen[3]}} },
    { 0x000Au, 0x2800u /* Primary service                     */, 0x00000001u /*       */, 0x000Du, {{0x1801u, NULL}}                           },
    { 0x000Bu, 0x2803u /* Characteristic                      */, 0x00002001u /* ind   */, 0x000Du, {{0x2A05u, NULL}}                           },
    { 0x000Cu, 0x2A05u /* Service Changed                     */, 0x00002000u /* ind   */, 0x000Du, {{0x0004u, (void *)&cyBle_attValuesLen[4]}} },
    { 0x000Du, 0x2902u /* Client Characteristic Configuration */, 0x00000A04u /* rd,wr */, 0x000Du, {{0x0002u, (void *)&cyBle_attValuesLen[5]}} },
    { 0x000Eu, 0x2800u /* Primary service                     */, 0x00000001u /*       */, 0x0089u, {{0x181Au, NULL}}                           },
    { 0x000Fu, 0x2803u /* Characteristic                      */, 0x00002001u /* ind   */, 0x0011u, {{0x2A7Du, NULL}}                           },
    { 0x0010u, 0x2A7Du /* Descriptor Value Changed            */, 0x00002000u /* ind   */, 0x0011u, {{0x0002u, (void *)&cyBle_attValuesLen[6]}} },
    { 0x0011u, 0x2902u /* Client Characteristic Configuration */, 0x00000A04u /* rd,wr */, 0x0011u, {{0x0002u, (void *)&cyBle_attValuesLen[7]}} },
    { 0x0012u, 0x2803u /* Characteristic                      */, 0x00000201u /* rd    */, 0x0017u, {{0x2A73u, NULL}}                           },
    { 0x0013u, 0x2A73u /* Apparent Wind Direction             */, 0x00000201u /* rd    */, 0x0017u, {{0x0002u, (void *)&cyBle_attValuesLen[8]}} },
    { 0x0014u, 0x290Cu /* Environmental Sensing Measurement   */, 0x00000201u /* rd    */, 0x0014u, {{0x000Bu, (void *)&cyBle_attValuesLen[9]}} },
    { 0x0015u, 0x290Du /* Environmental Sensing Trigger Setti */, 0x00000201u /* rd    */, 0x0015u, {{0x0002u, (void *)&cyBle_attValuesLen[10]}} },
    { 0x0016u, 0x2901u /* Characteristic User Description     */, 0x00000201u /* rd    */, 0x0016u, {{0x0000u, (void *)&cyBle_attValuesLen[11]}} },
    { 0x0017u, 0x2906u /* Valid Range                         */, 0x00000201u /* rd    */, 0x0017u, {{0x0004u, (void *)&cyBle_attValuesLen[12]}} },
    { 0x0018u, 0x2803u /* Characteristic                      */, 0x00000201u /* rd    */, 0x001Du, {{0x2A72u, NULL}}                           },
    { 0x0019u, 0x2A72u /* Apparent Wind Speed                 */, 0x00000201u /* rd    */, 0x001Du, {{0x0002u, (void *)&cyBle_attValuesLen[13]}} },
    { 0x001Au, 0x290Cu /* Environmental Sensing Measurement   */, 0x00000201u /* rd    */, 0x001Au, {{0x000Bu, (void *)&cyBle_attValuesLen[14]}} },
    { 0x001Bu, 0x290Du /* Environmental Sensing Trigger Setti */, 0x00000201u /* rd    */, 0x001Bu, {{0x0002u, (void *)&cyBle_attValuesLen[15]}} },
    { 0x001Cu, 0x2901u /* Characteristic User Description     */, 0x00000201u /* rd    */, 0x001Cu, {{0x0000u, (void *)&cyBle_attValuesLen[16]}} },
    { 0x001Du, 0x2906u /* Valid Range                         */, 0x00000201u /* rd    */, 0x001Du, {{0x0004u, (void *)&cyBle_attValuesLen[17]}} },
    { 0x001Eu, 0x2803u /* Characteristic                      */, 0x00000201u /* rd    */, 0x0023u, {{0x2A7Bu, NULL}}                           },
    { 0x001Fu, 0x2A7Bu /* Dew Point                           */, 0x00000201u /* rd    */, 0x0023u, {{0x0001u, (void *)&cyBle_attValuesLen[18]}} },
    { 0x0020u, 0x290Cu /* Environmental Sensing Measurement   */, 0x00000201u /* rd    */, 0x0020u, {{0x000Bu, (void *)&cyBle_attValuesLen[19]}} },
    { 0x0021u, 0x290Du /* Environmental Sensing Trigger Setti */, 0x00000201u /* rd    */, 0x0021u, {{0x0002u, (void *)&cyBle_attValuesLen[20]}} },
    { 0x0022u, 0x2901u /* Characteristic User Description     */, 0x00000201u /* rd    */, 0x0022u, {{0x0000u, (void *)&cyBle_attValuesLen[21]}} },
    { 0x0023u, 0x2906u /* Valid Range                         */, 0x00000201u /* rd    */, 0x0023u, {{0x0002u, (void *)&cyBle_attValuesLen[22]}} },
    { 0x0024u, 0x2803u /* Characteristic                      */, 0x00000201u /* rd    */, 0x0029u, {{0x2A6Cu, NULL}}                           },
    { 0x0025u, 0x2A6Cu /* Elevation                           */, 0x00000201u /* rd    */, 0x0029u, {{0x0003u, (void *)&cyBle_attValuesLen[23]}} },
    { 0x0026u, 0x290Cu /* Environmental Sensing Measurement   */, 0x00000201u /* rd    */, 0x0026u, {{0x000Bu, (void *)&cyBle_attValuesLen[24]}} },
    { 0x0027u, 0x290Du /* Environmental Sensing Trigger Setti */, 0x00000201u /* rd    */, 0x0027u, {{0x0002u, (void *)&cyBle_attValuesLen[25]}} },
    { 0x0028u, 0x2901u /* Characteristic User Description     */, 0x00000201u /* rd    */, 0x0028u, {{0x0000u, (void *)&cyBle_attValuesLen[26]}} },
    { 0x0029u, 0x2906u /* Valid Range                         */, 0x00000201u /* rd    */, 0x0029u, {{0x0006u, (void *)&cyBle_attValuesLen[27]}} },
    { 0x002Au, 0x2803u /* Characteristic                      */, 0x00000201u /* rd    */, 0x002Fu, {{0x2A74u, NULL}}                           },
    { 0x002Bu, 0x2A74u /* Gust Factor                         */, 0x00000201u /* rd    */, 0x002Fu, {{0x0001u, (void *)&cyBle_attValuesLen[28]}} },
    { 0x002Cu, 0x290Cu /* Environmental Sensing Measurement   */, 0x00000201u /* rd    */, 0x002Cu, {{0x000Bu, (void *)&cyBle_attValuesLen[29]}} },
    { 0x002Du, 0x290Du /* Environmental Sensing Trigger Setti */, 0x00000201u /* rd    */, 0x002Du, {{0x0002u, (void *)&cyBle_attValuesLen[30]}} },
    { 0x002Eu, 0x2901u /* Characteristic User Description     */, 0x00000201u /* rd    */, 0x002Eu, {{0x0000u, (void *)&cyBle_attValuesLen[31]}} },
    { 0x002Fu, 0x2906u /* Valid Range                         */, 0x00000201u /* rd    */, 0x002Fu, {{0x0002u, (void *)&cyBle_attValuesLen[32]}} },
    { 0x0030u, 0x2803u /* Characteristic                      */, 0x00000201u /* rd    */, 0x0035u, {{0x2A7Au, NULL}}                           },
    { 0x0031u, 0x2A7Au /* Heat Index                          */, 0x00000201u /* rd    */, 0x0035u, {{0x0001u, (void *)&cyBle_attValuesLen[33]}} },
    { 0x0032u, 0x290Cu /* Environmental Sensing Measurement   */, 0x00000201u /* rd    */, 0x0032u, {{0x000Bu, (void *)&cyBle_attValuesLen[34]}} },
    { 0x0033u, 0x290Du /* Environmental Sensing Trigger Setti */, 0x00000201u /* rd    */, 0x0033u, {{0x0002u, (void *)&cyBle_attValuesLen[35]}} },
    { 0x0034u, 0x2901u /* Characteristic User Description     */, 0x00000201u /* rd    */, 0x0034u, {{0x0000u, (void *)&cyBle_attValuesLen[36]}} },
    { 0x0035u, 0x2906u /* Valid Range                         */, 0x00000201u /* rd    */, 0x0035u, {{0x0002u, (void *)&cyBle_attValuesLen[37]}} },
    { 0x0036u, 0x2803u /* Characteristic                      */, 0x00000201u /* rd    */, 0x003Bu, {{0x2A6Fu, NULL}}                           },
    { 0x0037u, 0x2A6Fu /* Humidity                            */, 0x00000201u /* rd    */, 0x003Bu, {{0x0002u, (void *)&cyBle_attValuesLen[38]}} },
    { 0x0038u, 0x290Cu /* Environmental Sensing Measurement   */, 0x00000201u /* rd    */, 0x0038u, {{0x000Bu, (void *)&cyBle_attValuesLen[39]}} },
    { 0x0039u, 0x290Du /* Environmental Sensing Trigger Setti */, 0x00000201u /* rd    */, 0x0039u, {{0x0002u, (void *)&cyBle_attValuesLen[40]}} },
    { 0x003Au, 0x2901u /* Characteristic User Description     */, 0x00000201u /* rd    */, 0x003Au, {{0x0000u, (void *)&cyBle_attValuesLen[41]}} },
    { 0x003Bu, 0x2906u /* Valid Range                         */, 0x00000201u /* rd    */, 0x003Bu, {{0x0004u, (void *)&cyBle_attValuesLen[42]}} },
    { 0x003Cu, 0x2803u /* Characteristic                      */, 0x00000201u /* rd    */, 0x0041u, {{0x2A77u, NULL}}                           },
    { 0x003Du, 0x2A77u /* Irradiance                          */, 0x00000201u /* rd    */, 0x0041u, {{0x0002u, (void *)&cyBle_attValuesLen[43]}} },
    { 0x003Eu, 0x290Cu /* Environmental Sensing Measurement   */, 0x00000201u /* rd    */, 0x003Eu, {{0x000Bu, (void *)&cyBle_attValuesLen[44]}} },
    { 0x003Fu, 0x290Du /* Environmental Sensing Trigger Setti */, 0x00000201u /* rd    */, 0x003Fu, {{0x0002u, (void *)&cyBle_attValuesLen[45]}} },
    { 0x0040u, 0x2901u /* Characteristic User Description     */, 0x00000201u /* rd    */, 0x0040u, {{0x0000u, (void *)&cyBle_attValuesLen[46]}} },
    { 0x0041u, 0x2906u /* Valid Range                         */, 0x00000201u /* rd    */, 0x0041u, {{0x0004u, (void *)&cyBle_attValuesLen[47]}} },
    { 0x0042u, 0x2803u /* Characteristic                      */, 0x00000201u /* rd    */, 0x0047u, {{0x2A75u, NULL}}                           },
    { 0x0043u, 0x2A75u /* Pollen Concentration                */, 0x00000201u /* rd    */, 0x0047u, {{0x0003u, (void *)&cyBle_attValuesLen[48]}} },
    { 0x0044u, 0x290Cu /* Environmental Sensing Measurement   */, 0x00000201u /* rd    */, 0x0044u, {{0x000Bu, (void *)&cyBle_attValuesLen[49]}} },
    { 0x0045u, 0x290Du /* Environmental Sensing Trigger Setti */, 0x00000201u /* rd    */, 0x0045u, {{0x0002u, (void *)&cyBle_attValuesLen[50]}} },
    { 0x0046u, 0x2901u /* Characteristic User Description     */, 0x00000201u /* rd    */, 0x0046u, {{0x0000u, (void *)&cyBle_attValuesLen[51]}} },
    { 0x0047u, 0x2906u /* Valid Range                         */, 0x00000201u /* rd    */, 0x0047u, {{0x0006u, (void *)&cyBle_attValuesLen[52]}} },
    { 0x0048u, 0x2803u /* Characteristic                      */, 0x00000201u /* rd    */, 0x004Du, {{0x2A78u, NULL}}                           },
    { 0x0049u, 0x2A78u /* Rainfall                            */, 0x00000201u /* rd    */, 0x004Du, {{0x0002u, (void *)&cyBle_attValuesLen[53]}} },
    { 0x004Au, 0x290Cu /* Environmental Sensing Measurement   */, 0x00000201u /* rd    */, 0x004Au, {{0x000Bu, (void *)&cyBle_attValuesLen[54]}} },
    { 0x004Bu, 0x290Du /* Environmental Sensing Trigger Setti */, 0x00000201u /* rd    */, 0x004Bu, {{0x0002u, (void *)&cyBle_attValuesLen[55]}} },
    { 0x004Cu, 0x2901u /* Characteristic User Description     */, 0x00000201u /* rd    */, 0x004Cu, {{0x0000u, (void *)&cyBle_attValuesLen[56]}} },
    { 0x004Du, 0x2906u /* Valid Range                         */, 0x00000201u /* rd    */, 0x004Du, {{0x0004u, (void *)&cyBle_attValuesLen[57]}} },
    { 0x004Eu, 0x2803u /* Characteristic                      */, 0x00000201u /* rd    */, 0x0053u, {{0x2A6Du, NULL}}                           },
    { 0x004Fu, 0x2A6Du /* Pressure                            */, 0x00000201u /* rd    */, 0x0053u, {{0x0004u, (void *)&cyBle_attValuesLen[58]}} },
    { 0x0050u, 0x290Cu /* Environmental Sensing Measurement   */, 0x00000201u /* rd    */, 0x0050u, {{0x000Bu, (void *)&cyBle_attValuesLen[59]}} },
    { 0x0051u, 0x290Du /* Environmental Sensing Trigger Setti */, 0x00000201u /* rd    */, 0x0051u, {{0x0002u, (void *)&cyBle_attValuesLen[60]}} },
    { 0x0052u, 0x2901u /* Characteristic User Description     */, 0x00000201u /* rd    */, 0x0052u, {{0x0000u, (void *)&cyBle_attValuesLen[61]}} },
    { 0x0053u, 0x2906u /* Valid Range                         */, 0x00000201u /* rd    */, 0x0053u, {{0x0008u, (void *)&cyBle_attValuesLen[62]}} },
    { 0x0054u, 0x2803u /* Characteristic                      */, 0x00000201u /* rd    */, 0x0059u, {{0x2A6Eu, NULL}}                           },
    { 0x0055u, 0x2A6Eu /* Temperature                         */, 0x00000201u /* rd    */, 0x0059u, {{0x0002u, (void *)&cyBle_attValuesLen[63]}} },
    { 0x0056u, 0x290Cu /* Environmental Sensing Measurement   */, 0x00000201u /* rd    */, 0x0056u, {{0x000Bu, (void *)&cyBle_attValuesLen[64]}} },
    { 0x0057u, 0x290Du /* Environmental Sensing Trigger Setti */, 0x00000201u /* rd    */, 0x0057u, {{0x0002u, (void *)&cyBle_attValuesLen[65]}} },
    { 0x0058u, 0x2901u /* Characteristic User Description     */, 0x00000201u /* rd    */, 0x0058u, {{0x0000u, (void *)&cyBle_attValuesLen[66]}} },
    { 0x0059u, 0x2906u /* Valid Range                         */, 0x00000201u /* rd    */, 0x0059u, {{0x0004u, (void *)&cyBle_attValuesLen[67]}} },
    { 0x005Au, 0x2803u /* Characteristic                      */, 0x00000201u /* rd    */, 0x005Fu, {{0x2A71u, NULL}}                           },
    { 0x005Bu, 0x2A71u /* True Wind Direction                 */, 0x00000201u /* rd    */, 0x005Fu, {{0x0002u, (void *)&cyBle_attValuesLen[68]}} },
    { 0x005Cu, 0x290Cu /* Environmental Sensing Measurement   */, 0x00000201u /* rd    */, 0x005Cu, {{0x000Bu, (void *)&cyBle_attValuesLen[69]}} },
    { 0x005Du, 0x290Du /* Environmental Sensing Trigger Setti */, 0x00000201u /* rd    */, 0x005Du, {{0x0002u, (void *)&cyBle_attValuesLen[70]}} },
    { 0x005Eu, 0x2901u /* Characteristic User Description     */, 0x00000201u /* rd    */, 0x005Eu, {{0x0000u, (void *)&cyBle_attValuesLen[71]}} },
    { 0x005Fu, 0x2906u /* Valid Range                         */, 0x00000201u /* rd    */, 0x005Fu, {{0x0004u, (void *)&cyBle_attValuesLen[72]}} },
    { 0x0060u, 0x2803u /* Characteristic                      */, 0x00000201u /* rd    */, 0x0065u, {{0x2A70u, NULL}}                           },
    { 0x0061u, 0x2A70u /* True Wind Speed                     */, 0x00000201u /* rd    */, 0x0065u, {{0x0002u, (void *)&cyBle_attValuesLen[73]}} },
    { 0x0062u, 0x290Cu /* Environmental Sensing Measurement   */, 0x00000201u /* rd    */, 0x0062u, {{0x000Bu, (void *)&cyBle_attValuesLen[74]}} },
    { 0x0063u, 0x290Du /* Environmental Sensing Trigger Setti */, 0x00000201u /* rd    */, 0x0063u, {{0x0002u, (void *)&cyBle_attValuesLen[75]}} },
    { 0x0064u, 0x2901u /* Characteristic User Description     */, 0x00000201u /* rd    */, 0x0064u, {{0x0000u, (void *)&cyBle_attValuesLen[76]}} },
    { 0x0065u, 0x2906u /* Valid Range                         */, 0x00000201u /* rd    */, 0x0065u, {{0x0004u, (void *)&cyBle_attValuesLen[77]}} },
    { 0x0066u, 0x2803u /* Characteristic                      */, 0x00000201u /* rd    */, 0x006Bu, {{0x2A76u, NULL}}                           },
    { 0x0067u, 0x2A76u /* UV Index                            */, 0x00000201u /* rd    */, 0x006Bu, {{0x0001u, (void *)&cyBle_attValuesLen[78]}} },
    { 0x0068u, 0x290Cu /* Environmental Sensing Measurement   */, 0x00000201u /* rd    */, 0x0068u, {{0x000Bu, (void *)&cyBle_attValuesLen[79]}} },
    { 0x0069u, 0x290Du /* Environmental Sensing Trigger Setti */, 0x00000201u /* rd    */, 0x0069u, {{0x0002u, (void *)&cyBle_attValuesLen[80]}} },
    { 0x006Au, 0x2901u /* Characteristic User Description     */, 0x00000201u /* rd    */, 0x006Au, {{0x0000u, (void *)&cyBle_attValuesLen[81]}} },
    { 0x006Bu, 0x2906u /* Valid Range                         */, 0x00000201u /* rd    */, 0x006Bu, {{0x0002u, (void *)&cyBle_attValuesLen[82]}} },
    { 0x006Cu, 0x2803u /* Characteristic                      */, 0x00000201u /* rd    */, 0x0071u, {{0x2A79u, NULL}}                           },
    { 0x006Du, 0x2A79u /* Wind Chill                          */, 0x00000201u /* rd    */, 0x0071u, {{0x0001u, (void *)&cyBle_attValuesLen[83]}} },
    { 0x006Eu, 0x290Cu /* Environmental Sensing Measurement   */, 0x00000201u /* rd    */, 0x006Eu, {{0x000Bu, (void *)&cyBle_attValuesLen[84]}} },
    { 0x006Fu, 0x290Du /* Environmental Sensing Trigger Setti */, 0x00000201u /* rd    */, 0x006Fu, {{0x0002u, (void *)&cyBle_attValuesLen[85]}} },
    { 0x0070u, 0x2901u /* Characteristic User Description     */, 0x00000201u /* rd    */, 0x0070u, {{0x0000u, (void *)&cyBle_attValuesLen[86]}} },
    { 0x0071u, 0x2906u /* Valid Range                         */, 0x00000201u /* rd    */, 0x0071u, {{0x0002u, (void *)&cyBle_attValuesLen[87]}} },
    { 0x0072u, 0x2803u /* Characteristic                      */, 0x00000201u /* rd    */, 0x0077u, {{0x2AA3u, NULL}}                           },
    { 0x0073u, 0x2AA3u /* Barometric Pressure Trend           */, 0x00000201u /* rd    */, 0x0077u, {{0x0001u, (void *)&cyBle_attValuesLen[88]}} },
    { 0x0074u, 0x290Cu /* Environmental Sensing Measurement   */, 0x00000201u /* rd    */, 0x0074u, {{0x000Bu, (void *)&cyBle_attValuesLen[89]}} },
    { 0x0075u, 0x290Du /* Environmental Sensing Trigger Setti */, 0x00000201u /* rd    */, 0x0075u, {{0x0002u, (void *)&cyBle_attValuesLen[90]}} },
    { 0x0076u, 0x2901u /* Characteristic User Description     */, 0x00000201u /* rd    */, 0x0076u, {{0x0000u, (void *)&cyBle_attValuesLen[91]}} },
    { 0x0077u, 0x2906u /* Valid Range                         */, 0x00000201u /* rd    */, 0x0077u, {{0x0002u, (void *)&cyBle_attValuesLen[92]}} },
    { 0x0078u, 0x2803u /* Characteristic                      */, 0x00000201u /* rd    */, 0x007Du, {{0x2A2Cu, NULL}}                           },
    { 0x0079u, 0x2A2Cu /* Magnetic Declination                */, 0x00000201u /* rd    */, 0x007Du, {{0x0002u, (void *)&cyBle_attValuesLen[93]}} },
    { 0x007Au, 0x290Cu /* Environmental Sensing Measurement   */, 0x00000201u /* rd    */, 0x007Au, {{0x000Bu, (void *)&cyBle_attValuesLen[94]}} },
    { 0x007Bu, 0x290Du /* Environmental Sensing Trigger Setti */, 0x00000201u /* rd    */, 0x007Bu, {{0x0002u, (void *)&cyBle_attValuesLen[95]}} },
    { 0x007Cu, 0x2901u /* Characteristic User Description     */, 0x00000201u /* rd    */, 0x007Cu, {{0x0000u, (void *)&cyBle_attValuesLen[96]}} },
    { 0x007Du, 0x2906u /* Valid Range                         */, 0x00000201u /* rd    */, 0x007Du, {{0x0004u, (void *)&cyBle_attValuesLen[97]}} },
    { 0x007Eu, 0x2803u /* Characteristic                      */, 0x00000201u /* rd    */, 0x0083u, {{0x2AA0u, NULL}}                           },
    { 0x007Fu, 0x2AA0u /* Magnetic Flux Density - 2D          */, 0x00000201u /* rd    */, 0x0083u, {{0x0004u, (void *)&cyBle_attValuesLen[98]}} },
    { 0x0080u, 0x290Cu /* Environmental Sensing Measurement   */, 0x00000201u /* rd    */, 0x0080u, {{0x000Bu, (void *)&cyBle_attValuesLen[99]}} },
    { 0x0081u, 0x290Du /* Environmental Sensing Trigger Setti */, 0x00000201u /* rd    */, 0x0081u, {{0x0002u, (void *)&cyBle_attValuesLen[100]}} },
    { 0x0082u, 0x2901u /* Characteristic User Description     */, 0x00000201u /* rd    */, 0x0082u, {{0x0000u, (void *)&cyBle_attValuesLen[101]}} },
    { 0x0083u, 0x2906u /* Valid Range                         */, 0x00000201u /* rd    */, 0x0083u, {{0x0004u, (void *)&cyBle_attValuesLen[102]}} },
    { 0x0084u, 0x2803u /* Characteristic                      */, 0x00000201u /* rd    */, 0x0089u, {{0x2AA1u, NULL}}                           },
    { 0x0085u, 0x2AA1u /* Magnetic Flux Density - 3D          */, 0x00000201u /* rd    */, 0x0089u, {{0x0006u, (void *)&cyBle_attValuesLen[103]}} },
    { 0x0086u, 0x290Cu /* Environmental Sensing Measurement   */, 0x00000201u /* rd    */, 0x0086u, {{0x000Bu, (void *)&cyBle_attValuesLen[104]}} },
    { 0x0087u, 0x290Du /* Environmental Sensing Trigger Setti */, 0x00000201u /* rd    */, 0x0087u, {{0x0002u, (void *)&cyBle_attValuesLen[105]}} },
    { 0x0088u, 0x2901u /* Characteristic User Description     */, 0x00000201u /* rd    */, 0x0088u, {{0x0000u, (void *)&cyBle_attValuesLen[106]}} },
    { 0x0089u, 0x2906u /* Valid Range                         */, 0x00000201u /* rd    */, 0x0089u, {{0x0004u, (void *)&cyBle_attValuesLen[107]}} },
    { 0x008Au, 0x2800u /* Primary service                     */, 0x00000001u /*       */, 0x009Cu, {{0x180Au, NULL}}                           },
    { 0x008Bu, 0x2803u /* Characteristic                      */, 0x00000201u /* rd    */, 0x008Cu, {{0x2A29u, NULL}}                           },
    { 0x008Cu, 0x2A29u /* Manufacturer Name String            */, 0x00000201u /* rd    */, 0x008Cu, {{0x0000u, (void *)&cyBle_attValuesLen[108]}} },
    { 0x008Du, 0x2803u /* Characteristic                      */, 0x00000201u /* rd    */, 0x008Eu, {{0x2A24u, NULL}}                           },
    { 0x008Eu, 0x2A24u /* Model Number String                 */, 0x00000201u /* rd    */, 0x008Eu, {{0x0000u, (void *)&cyBle_attValuesLen[109]}} },
    { 0x008Fu, 0x2803u /* Characteristic                      */, 0x00000201u /* rd    */, 0x0090u, {{0x2A25u, NULL}}                           },
    { 0x0090u, 0x2A25u /* Serial Number String                */, 0x00000201u /* rd    */, 0x0090u, {{0x0000u, (void *)&cyBle_attValuesLen[110]}} },
    { 0x0091u, 0x2803u /* Characteristic                      */, 0x00000201u /* rd    */, 0x0092u, {{0x2A27u, NULL}}                           },
    { 0x0092u, 0x2A27u /* Hardware Revision String            */, 0x00000201u /* rd    */, 0x0092u, {{0x0000u, (void *)&cyBle_attValuesLen[111]}} },
    { 0x0093u, 0x2803u /* Characteristic                      */, 0x00000201u /* rd    */, 0x0094u, {{0x2A26u, NULL}}                           },
    { 0x0094u, 0x2A26u /* Firmware Revision String            */, 0x00000201u /* rd    */, 0x0094u, {{0x0000u, (void *)&cyBle_attValuesLen[112]}} },
    { 0x0095u, 0x2803u /* Characteristic                      */, 0x00000201u /* rd    */, 0x0096u, {{0x2A28u, NULL}}                           },
    { 0x0096u, 0x2A28u /* Software Revision String            */, 0x00000201u /* rd    */, 0x0096u, {{0x0000u, (void *)&cyBle_attValuesLen[113]}} },
    { 0x0097u, 0x2803u /* Characteristic                      */, 0x00000201u /* rd    */, 0x0098u, {{0x2A23u, NULL}}                           },
    { 0x0098u, 0x2A23u /* System ID                           */, 0x00000201u /* rd    */, 0x0098u, {{0x0008u, (void *)&cyBle_attValuesLen[114]}} },
    { 0x0099u, 0x2803u /* Characteristic                      */, 0x00000201u /* rd    */, 0x009Au, {{0x2A2Au, NULL}}                           },
    { 0x009Au, 0x2A2Au /* IEEE 11073-20601 Regulatory Certifi */, 0x00000201u /* rd    */, 0x009Au, {{0x0001u, (void *)&cyBle_attValuesLen[115]}} },
    { 0x009Bu, 0x2803u /* Characteristic                      */, 0x00000201u /* rd    */, 0x009Cu, {{0x2A50u, NULL}}                           },
    { 0x009Cu, 0x2A50u /* PnP ID                              */, 0x00000201u /* rd    */, 0x009Cu, {{0x0007u, (void *)&cyBle_attValuesLen[116]}} },
    { 0x009Du, 0x2800u /* Primary service                     */, 0x00000001u /*       */, 0x00A1u, {{0x180Fu, NULL}}                           },
    { 0x009Eu, 0x2803u /* Characteristic                      */, 0x00000201u /* rd    */, 0x00A1u, {{0x2A19u, NULL}}                           },
    { 0x009Fu, 0x2A19u /* Battery Level                       */, 0x00000201u /* rd    */, 0x00A1u, {{0x0001u, (void *)&cyBle_attValuesLen[117]}} },
    { 0x00A0u, 0x2904u /* Characteristic Presentation Format  */, 0x00000201u /* rd    */, 0x00A0u, {{0x0007u, (void *)&cyBle_attValuesLen[118]}} },
    { 0x00A1u, 0x2902u /* Client Characteristic Configuration */, 0x00000A04u /* rd,wr */, 0x00A1u, {{0x0002u, (void *)&cyBle_attValuesLen[119]}} },
};


#endif /* (CYBLE_GATT_ROLE_SERVER) */

#if(CYBLE_GATT_ROLE_CLIENT)
    
CYBLE_CLIENT_STATE_T cyBle_clientState;
CYBLE_GATTC_T cyBle_gattc;
CYBLE_GATT_ATTR_HANDLE_RANGE_T cyBle_gattcDiscoveryRange;
    
#endif /* (CYBLE_GATT_ROLE_CLIENT) */


#if(CYBLE_GATT_ROLE_SERVER)

/****************************************************************************** 
* Function Name: CyBle_GattsReInitGattDb
***************************************************************************//**
* 
*  Reinitializes the GATT database.
* 
*  \return
*  CYBLE_API_RESULT_T: An API result states if the API succeeded or failed with
*  error codes:

*  Errors codes                          | Description
*  ------------                          | -----------
*  CYBLE_ERROR_OK						 | GATT database was reinitialized successfully.
*  CYBLE_ERROR_INVALID_STATE             | If the function is called in any state except CYBLE_STATE_DISCONNECTED.
*  CYBLE_ERROR_INVALID_PARAMETER         | If the Database has zero entries or is a NULL pointer.
* 
******************************************************************************/
CYBLE_API_RESULT_T CyBle_GattsReInitGattDb(void)
{
    CYBLE_API_RESULT_T apiResult;
    
    if(CYBLE_STATE_DISCONNECTED == CyBle_GetState())
    {
        apiResult = CyBle_GattsDbRegister(cyBle_gattDB, CYBLE_GATT_DB_INDEX_COUNT, CYBLE_GATT_DB_MAX_VALUE_LEN);
    }
    else
    {
        apiResult = CYBLE_ERROR_INVALID_STATE;
    }
    
    return(apiResult);
}


/****************************************************************************** 
* Function Name: CyBle_GattsWriteEventHandler
***************************************************************************//**
* 
*  Handles the Write Request Event for GATT service.
* 
*  \param eventParam: The pointer to the data structure specified by the event.
* 
*  \return
*  CYBLE_GATT_ERR_CODE_T: An API result returns one of the following status 
*  values.

*  Errors codes                          | Description
*  --------------------                  | -----------
*  CYBLE_GATT_ERR_NONE                   | Write is successful.
* 
******************************************************************************/
CYBLE_GATT_ERR_CODE_T CyBle_GattsWriteEventHandler(CYBLE_GATTS_WRITE_REQ_PARAM_T *eventParam)
{
    CYBLE_GATT_ERR_CODE_T gattErr = CYBLE_GATT_ERR_NONE;
    
    /* Client Characteristic Configuration descriptor write request */
    if(eventParam->handleValPair.attrHandle == cyBle_gatts.cccdHandle)
    {
        /* Store value to database */
        gattErr = CyBle_GattsWriteAttributeValue(&eventParam->handleValPair, 0u, 
                        &eventParam->connHandle, CYBLE_GATT_DB_PEER_INITIATED);
        
        if(CYBLE_GATT_ERR_NONE == gattErr)
        {
            if(CYBLE_IS_INDICATION_ENABLED_IN_PTR(eventParam->handleValPair.value.val))
            {
                CyBle_ApplCallback((uint32)CYBLE_EVT_GATTS_INDICATION_ENABLED, eventParam);
            }
            else
            {
                CyBle_ApplCallback((uint32)CYBLE_EVT_GATTS_INDICATION_DISABLED, eventParam);
            }
        }
        cyBle_eventHandlerFlag &= (uint8)~CYBLE_CALLBACK;
    }
    return (gattErr);
}


#endif /* (CYBLE_GATT_ROLE_SERVER) */

#if(CYBLE_GATT_ROLE_CLIENT)


/****************************************************************************** 
* Function Name: CyBle_GattcStartDiscovery
***************************************************************************//**
* 
*  Starts the automatic server discovery process. Two events may be generated 
*  after calling this function - CYBLE_EVT_GATTC_DISCOVERY_COMPLETE or 
*  CYBLE_EVT_GATTC_ERROR_RSP. The CYBLE_EVT_GATTC_DISCOVERY_COMPLETE event is 
*  generated when the remote device was successfully discovered. The
*  CYBLE_EVT_GATTC_ERROR_RSP is generated if the device discovery is failed.
* 
*  \param connHandle: The handle which consists of the device ID and ATT connection ID.
* 
* \return
*	CYBLE_API_RESULT_T : Return value indicates if the function succeeded or
*                        failed. Following are the possible error codes.
*
*   <table>	
*   <tr>
*	  <th>Errors codes</th>
*	  <th>Description</th>
*	</tr>
*	<tr>
*	  <td>CYBLE_ERROR_OK</td>
*	  <td>On successful operation</td>
*	</tr>
*	<tr>
*	  <td>CYBLE_ERROR_INVALID_PARAMETER</td>
*	  <td>'connHandle' value does not represent any existing entry.</td>
*	</tr>
*	<tr>
*	  <td>CYBLE_ERROR_INVALID_OPERATION</td>
*	  <td>The operation is not permitted</td>
*	</tr>
*   <tr>
*	  <td>CYBLE_ERROR_MEMORY_ALLOCATION_FAILED</td>
*	  <td>Memory allocation failed</td>
*	</tr>
*   <tr>
*	  <td>CYBLE_ERROR_INVALID_STATE</td>
*	  <td>If the function is called in any state except connected or discovered</td>
*	</tr>
*   </table>
* 
******************************************************************************/
CYBLE_API_RESULT_T CyBle_GattcStartDiscovery(CYBLE_CONN_HANDLE_T connHandle)
{
    uint8 j;
    CYBLE_API_RESULT_T apiResult;
    
    if((CyBle_GetState() != CYBLE_STATE_CONNECTED) || 
       ((CyBle_GetClientState() != CYBLE_CLIENT_STATE_CONNECTED) && 
        (CyBle_GetClientState() != CYBLE_CLIENT_STATE_DISCOVERED))) 
    {
        apiResult = CYBLE_ERROR_INVALID_STATE;
    }
    else
    {
        /* Clean old discovery information */
        for(j = 0u; j < (uint8) CYBLE_SRVI_COUNT; j++)
        {
            (void)memset(&cyBle_serverInfo[j].range, 0, sizeof(cyBle_serverInfo[0].range));
        }

        cyBle_connHandle = connHandle;
        cyBle_gattcDiscoveryRange.startHandle = CYBLE_GATT_ATTR_HANDLE_START_RANGE;
        cyBle_gattcDiscoveryRange.endHandle = CYBLE_GATT_ATTR_HANDLE_END_RANGE;
        
        CyBle_ServiceInit();
        
        apiResult = CyBle_GattcDiscoverAllPrimaryServices(connHandle);

        if(CYBLE_ERROR_OK == apiResult)
        {
            CyBle_SetClientState(CYBLE_CLIENT_STATE_SRVC_DISCOVERING);
            cyBle_eventHandlerFlag |= CYBLE_AUTO_DISCOVERY;
        }
    }
    
    return (apiResult);
}


/****************************************************************************** 
* Function Name: CyBle_GattcStartPartialDiscovery
***************************************************************************//**
* 
*  Starts the automatic server discovery process as per the range provided
*  on a GATT Server to which it is connected. This API could be used for 
*  partial server discovery after indication received to the Service Changed
*  Characteristic Value. Two events may be generated 
*  after calling this function - CYBLE_EVT_GATTC_DISCOVERY_COMPLETE or 
*  CYBLE_EVT_GATTC_ERROR_RSP. The CYBLE_EVT_GATTC_DISCOVERY_COMPLETE event is 
*  generated when the remote device was successfully discovered. The
*  CYBLE_EVT_GATTC_ERROR_RSP is generated if the device discovery is failed.
* 
*  \param connHandle: The handle which consists of the device ID and ATT connection ID.
*  \param startHandle: Start of affected attribute handle range.
*  \param endHandle: End of affected attribute handle range.
* 
*  \return
*	CYBLE_API_RESULT_T : Return value indicates if the function succeeded or
*                        failed. Following are the possible error codes.
*
*   <table>	
*   <tr>
*	  <th>Errors codes</th>
*	  <th>Description</th>
*	</tr>
*	<tr>
*	  <td>CYBLE_ERROR_OK</td>
*	  <td>On successful operation</td>
*	</tr>
*	<tr>
*	  <td>CYBLE_ERROR_INVALID_PARAMETER</td>
*	  <td>'connHandle' value does not represent any existing entry.</td>
*	</tr>
*	<tr>
*	  <td>CYBLE_ERROR_INVALID_OPERATION</td>
*	  <td>The operation is not permitted</td>
*	</tr>
*   <tr>
*	  <td>CYBLE_ERROR_MEMORY_ALLOCATION_FAILED</td>
*	  <td>Memory allocation failed</td>
*	</tr>
*   <tr>
*	  <td>CYBLE_ERROR_INVALID_STATE</td>
*	  <td>If the function is called in any state except connected or discovered</td>
*	</tr>
*   </table>
* 
******************************************************************************/
CYBLE_API_RESULT_T CyBle_GattcStartPartialDiscovery(CYBLE_CONN_HANDLE_T connHandle,
                        CYBLE_GATT_DB_ATTR_HANDLE_T startHandle, CYBLE_GATT_DB_ATTR_HANDLE_T endHandle)
{
    uint8 j;
    CYBLE_API_RESULT_T apiResult;
    
    if((CyBle_GetState() != CYBLE_STATE_CONNECTED) || 
       ((CyBle_GetClientState() != CYBLE_CLIENT_STATE_CONNECTED) && 
        (CyBle_GetClientState() != CYBLE_CLIENT_STATE_DISCOVERED))) 
    {
        apiResult = CYBLE_ERROR_INVALID_STATE;
    }
    else
    {
        /* Clean old discovery information of affected attribute range */
        for(j = 0u; j < (uint8) CYBLE_SRVI_COUNT; j++)
        {
            if((cyBle_serverInfo[j].range.startHandle >= startHandle) &&
               (cyBle_serverInfo[j].range.startHandle <= endHandle))
            {
                (void)memset(&cyBle_serverInfo[j].range, 0, sizeof(cyBle_serverInfo[0].range));
            }
        }

        cyBle_connHandle = connHandle;
        cyBle_gattcDiscoveryRange.startHandle = startHandle;
        cyBle_gattcDiscoveryRange.endHandle = endHandle;

        CyBle_ServiceInit();

        apiResult = CyBle_GattcDiscoverPrimaryServices(connHandle, &cyBle_gattcDiscoveryRange);

        if(CYBLE_ERROR_OK == apiResult)
        {
            CyBle_SetClientState(CYBLE_CLIENT_STATE_SRVC_DISCOVERING);
            cyBle_eventHandlerFlag |= CYBLE_AUTO_DISCOVERY;
        }
    }
    
    return (apiResult);
}


/******************************************************************************
* Function Name: CyBle_GattcDiscoverCharacteristicsEventHandler
***************************************************************************//**
* 
*  This function is called on receiving a "CYBLE_EVT_GATTC_READ_BY_TYPE_RSP"
*  event. Based on the service UUID, an appropriate data structure is populated
*  using the data received as part of the callback.
* 
*  \param *discCharInfo: The pointer to a characteristic information structure.
* 
* \return
*  None
* 
******************************************************************************/
void CyBle_GattcDiscoverCharacteristicsEventHandler(CYBLE_DISC_CHAR_INFO_T *discCharInfo)
{
    if(discCharInfo->uuid.uuid16 == CYBLE_UUID_CHAR_SERVICE_CHANGED)
    {
        CyBle_CheckStoreCharHandle(cyBle_gattc.serviceChanged);
    }
}


/******************************************************************************
* Function Name: CyBle_GattcDiscoverCharDescriptorsEventHandler
***************************************************************************//**
* 
*  This function is called on receiving a "CYBLE_EVT_GATTC_FIND_INFO_RSP" event.
*  Based on the descriptor UUID, an appropriate data structure is populated 
*  using the data received as part of the callback.
* 
*  \param *discDescrInfo: The pointer to a descriptor information structure.
*  \param discoveryService: The index of the service instance
* 
* \return
*  None
* 
******************************************************************************/
void CyBle_GattcDiscoverCharDescriptorsEventHandler(CYBLE_DISC_DESCR_INFO_T *discDescrInfo)
{
    if(discDescrInfo->uuid.uuid16 == CYBLE_UUID_CHAR_CLIENT_CONFIG)
    {
        CyBle_CheckStoreCharDescrHandle(cyBle_gattc.cccdHandle);
    }
}


/******************************************************************************
* Function Name: CyBle_GattcIndicationEventHandler
***************************************************************************//**
* 
*  Handles the Indication Event.
* 
*  \param *eventParam: The pointer to the data structure specified by the event.
* 
* \return
*  None.
* 
******************************************************************************/
void CyBle_GattcIndicationEventHandler(CYBLE_GATTC_HANDLE_VALUE_IND_PARAM_T *eventParam)
{
    if(cyBle_gattc.serviceChanged.valueHandle == eventParam->handleValPair.attrHandle)
    {
        CyBle_ApplCallback((uint32)CYBLE_EVT_GATTC_INDICATION, eventParam);
        cyBle_eventHandlerFlag &= (uint8)~CYBLE_CALLBACK;
    }
}


#endif /* (CYBLE_GATT_ROLE_CLIENT) */


/* [] END OF FILE */
