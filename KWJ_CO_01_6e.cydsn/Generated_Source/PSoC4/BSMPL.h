/*******************************************************************************
* File Name: BSMPL.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_BSMPL_H) /* Pins BSMPL_H */
#define CY_PINS_BSMPL_H

#include "cytypes.h"
#include "cyfitter.h"
#include "BSMPL_aliases.h"


/***************************************
*     Data Struct Definitions
***************************************/

/**
* \addtogroup group_structures
* @{
*/
    
/* Structure for sleep mode support */
typedef struct
{
    uint32 pcState; /**< State of the port control register */
    uint32 sioState; /**< State of the SIO configuration */
    uint32 usbState; /**< State of the USBIO regulator */
} BSMPL_BACKUP_STRUCT;

/** @} structures */


/***************************************
*        Function Prototypes             
***************************************/
/**
* \addtogroup group_general
* @{
*/
uint8   BSMPL_Read(void);
void    BSMPL_Write(uint8 value);
uint8   BSMPL_ReadDataReg(void);
#if defined(BSMPL__PC) || (CY_PSOC4_4200L) 
    void    BSMPL_SetDriveMode(uint8 mode);
#endif
void    BSMPL_SetInterruptMode(uint16 position, uint16 mode);
uint8   BSMPL_ClearInterrupt(void);
/** @} general */

/**
* \addtogroup group_power
* @{
*/
void BSMPL_Sleep(void); 
void BSMPL_Wakeup(void);
/** @} power */


/***************************************
*           API Constants        
***************************************/
#if defined(BSMPL__PC) || (CY_PSOC4_4200L) 
    /* Drive Modes */
    #define BSMPL_DRIVE_MODE_BITS        (3)
    #define BSMPL_DRIVE_MODE_IND_MASK    (0xFFFFFFFFu >> (32 - BSMPL_DRIVE_MODE_BITS))

    /**
    * \addtogroup group_constants
    * @{
    */
        /** \addtogroup driveMode Drive mode constants
         * \brief Constants to be passed as "mode" parameter in the BSMPL_SetDriveMode() function.
         *  @{
         */
        #define BSMPL_DM_ALG_HIZ         (0x00u) /**< \brief High Impedance Analog   */
        #define BSMPL_DM_DIG_HIZ         (0x01u) /**< \brief High Impedance Digital  */
        #define BSMPL_DM_RES_UP          (0x02u) /**< \brief Resistive Pull Up       */
        #define BSMPL_DM_RES_DWN         (0x03u) /**< \brief Resistive Pull Down     */
        #define BSMPL_DM_OD_LO           (0x04u) /**< \brief Open Drain, Drives Low  */
        #define BSMPL_DM_OD_HI           (0x05u) /**< \brief Open Drain, Drives High */
        #define BSMPL_DM_STRONG          (0x06u) /**< \brief Strong Drive            */
        #define BSMPL_DM_RES_UPDWN       (0x07u) /**< \brief Resistive Pull Up/Down  */
        /** @} driveMode */
    /** @} group_constants */
#endif

/* Digital Port Constants */
#define BSMPL_MASK               BSMPL__MASK
#define BSMPL_SHIFT              BSMPL__SHIFT
#define BSMPL_WIDTH              1u

/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in BSMPL_SetInterruptMode() function.
     *  @{
     */
        #define BSMPL_INTR_NONE      ((uint16)(0x0000u)) /**< \brief Disabled             */
        #define BSMPL_INTR_RISING    ((uint16)(0x5555u)) /**< \brief Rising edge trigger  */
        #define BSMPL_INTR_FALLING   ((uint16)(0xaaaau)) /**< \brief Falling edge trigger */
        #define BSMPL_INTR_BOTH      ((uint16)(0xffffu)) /**< \brief Both edge trigger    */
    /** @} intrMode */
/** @} group_constants */

/* SIO LPM definition */
#if defined(BSMPL__SIO)
    #define BSMPL_SIO_LPM_MASK       (0x03u)
#endif

/* USBIO definitions */
#if !defined(BSMPL__PC) && (CY_PSOC4_4200L)
    #define BSMPL_USBIO_ENABLE               ((uint32)0x80000000u)
    #define BSMPL_USBIO_DISABLE              ((uint32)(~BSMPL_USBIO_ENABLE))
    #define BSMPL_USBIO_SUSPEND_SHIFT        CYFLD_USBDEVv2_USB_SUSPEND__OFFSET
    #define BSMPL_USBIO_SUSPEND_DEL_SHIFT    CYFLD_USBDEVv2_USB_SUSPEND_DEL__OFFSET
    #define BSMPL_USBIO_ENTER_SLEEP          ((uint32)((1u << BSMPL_USBIO_SUSPEND_SHIFT) \
                                                        | (1u << BSMPL_USBIO_SUSPEND_DEL_SHIFT)))
    #define BSMPL_USBIO_EXIT_SLEEP_PH1       ((uint32)~((uint32)(1u << BSMPL_USBIO_SUSPEND_SHIFT)))
    #define BSMPL_USBIO_EXIT_SLEEP_PH2       ((uint32)~((uint32)(1u << BSMPL_USBIO_SUSPEND_DEL_SHIFT)))
    #define BSMPL_USBIO_CR1_OFF              ((uint32)0xfffffffeu)
#endif


/***************************************
*             Registers        
***************************************/
/* Main Port Registers */
#if defined(BSMPL__PC)
    /* Port Configuration */
    #define BSMPL_PC                 (* (reg32 *) BSMPL__PC)
#endif
/* Pin State */
#define BSMPL_PS                     (* (reg32 *) BSMPL__PS)
/* Data Register */
#define BSMPL_DR                     (* (reg32 *) BSMPL__DR)
/* Input Buffer Disable Override */
#define BSMPL_INP_DIS                (* (reg32 *) BSMPL__PC2)

/* Interrupt configuration Registers */
#define BSMPL_INTCFG                 (* (reg32 *) BSMPL__INTCFG)
#define BSMPL_INTSTAT                (* (reg32 *) BSMPL__INTSTAT)

/* "Interrupt cause" register for Combined Port Interrupt (AllPortInt) in GSRef component */
#if defined (CYREG_GPIO_INTR_CAUSE)
    #define BSMPL_INTR_CAUSE         (* (reg32 *) CYREG_GPIO_INTR_CAUSE)
#endif

/* SIO register */
#if defined(BSMPL__SIO)
    #define BSMPL_SIO_REG            (* (reg32 *) BSMPL__SIO)
#endif /* (BSMPL__SIO_CFG) */

/* USBIO registers */
#if !defined(BSMPL__PC) && (CY_PSOC4_4200L)
    #define BSMPL_USB_POWER_REG       (* (reg32 *) CYREG_USBDEVv2_USB_POWER_CTRL)
    #define BSMPL_CR1_REG             (* (reg32 *) CYREG_USBDEVv2_CR1)
    #define BSMPL_USBIO_CTRL_REG      (* (reg32 *) CYREG_USBDEVv2_USB_USBIO_CTRL)
#endif    
    
    
/***************************************
* The following code is DEPRECATED and 
* must not be used in new designs.
***************************************/
/**
* \addtogroup group_deprecated
* @{
*/
#define BSMPL_DRIVE_MODE_SHIFT       (0x00u)
#define BSMPL_DRIVE_MODE_MASK        (0x07u << BSMPL_DRIVE_MODE_SHIFT)
/** @} deprecated */

#endif /* End Pins BSMPL_H */


/* [] END OF FILE */
