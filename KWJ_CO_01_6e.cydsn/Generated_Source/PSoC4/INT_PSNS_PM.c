/*******************************************************************************
* File Name: INT_PSNS.c  
* Version 2.20
*
* Description:
*  This file contains APIs to set up the Pins component for low power modes.
*
* Note:
*
********************************************************************************
* Copyright 2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#include "cytypes.h"
#include "INT_PSNS.h"

static INT_PSNS_BACKUP_STRUCT  INT_PSNS_backup = {0u, 0u, 0u};


/*******************************************************************************
* Function Name: INT_PSNS_Sleep
****************************************************************************//**
*
* \brief Stores the pin configuration and prepares the pin for entering chip 
*  deep-sleep/hibernate modes. This function must be called for SIO and USBIO
*  pins. It is not essential if using GPIO or GPIO_OVT pins.
*
* <b>Note</b> This function is available in PSoC 4 only.
*
* \return 
*  None 
*  
* \sideeffect
*  For SIO pins, this function configures the pin input threshold to CMOS and
*  drive level to Vddio. This is needed for SIO pins when in device 
*  deep-sleep/hibernate modes.
*
* \funcusage
*  \snippet INT_PSNS_SUT.c usage_INT_PSNS_Sleep_Wakeup
*******************************************************************************/
void INT_PSNS_Sleep(void)
{
    #if defined(INT_PSNS__PC)
        INT_PSNS_backup.pcState = INT_PSNS_PC;
    #else
        #if (CY_PSOC4_4200L)
            /* Save the regulator state and put the PHY into suspend mode */
            INT_PSNS_backup.usbState = INT_PSNS_CR1_REG;
            INT_PSNS_USB_POWER_REG |= INT_PSNS_USBIO_ENTER_SLEEP;
            INT_PSNS_CR1_REG &= INT_PSNS_USBIO_CR1_OFF;
        #endif
    #endif
    #if defined(CYIPBLOCK_m0s8ioss_VERSION) && defined(INT_PSNS__SIO)
        INT_PSNS_backup.sioState = INT_PSNS_SIO_REG;
        /* SIO requires unregulated output buffer and single ended input buffer */
        INT_PSNS_SIO_REG &= (uint32)(~INT_PSNS_SIO_LPM_MASK);
    #endif  
}


/*******************************************************************************
* Function Name: INT_PSNS_Wakeup
****************************************************************************//**
*
* \brief Restores the pin configuration that was saved during Pin_Sleep().
*
* For USBIO pins, the wakeup is only triggered for falling edge interrupts.
*
* <b>Note</b> This function is available in PSoC 4 only.
*
* \return 
*  None
*  
* \funcusage
*  Refer to INT_PSNS_Sleep() for an example usage.
*******************************************************************************/
void INT_PSNS_Wakeup(void)
{
    #if defined(INT_PSNS__PC)
        INT_PSNS_PC = INT_PSNS_backup.pcState;
    #else
        #if (CY_PSOC4_4200L)
            /* Restore the regulator state and come out of suspend mode */
            INT_PSNS_USB_POWER_REG &= INT_PSNS_USBIO_EXIT_SLEEP_PH1;
            INT_PSNS_CR1_REG = INT_PSNS_backup.usbState;
            INT_PSNS_USB_POWER_REG &= INT_PSNS_USBIO_EXIT_SLEEP_PH2;
        #endif
    #endif
    #if defined(CYIPBLOCK_m0s8ioss_VERSION) && defined(INT_PSNS__SIO)
        INT_PSNS_SIO_REG = INT_PSNS_backup.sioState;
    #endif
}


/* [] END OF FILE */
