/***************************************************************************//**
* \file CYBLE_gatt.c
* \version 3.30
* 
* \brief
*  This file contains the source code for the GATT API of the BLE Component.
* 
********************************************************************************
* \copyright
* Copyright 2014-2016, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/


#include "BLE_eventHandler.h"


/***************************************
* Global variables
***************************************/

CYBLE_STATE_T cyBle_state;

#if ((CYBLE_MODE_PROFILE) && (CYBLE_BONDING_REQUIREMENT == CYBLE_BONDING_YES))
    
#if(CYBLE_MODE_PROFILE)
    #if defined(__ARMCC_VERSION)
        CY_ALIGN(CYDEV_FLS_ROW_SIZE) const CY_BLE_FLASH_STORAGE cyBle_flashStorage CY_SECTION(".cy_checksum_exclude") =
    #elif defined (__GNUC__)
        const CY_BLE_FLASH_STORAGE cyBle_flashStorage CY_SECTION(".cy_checksum_exclude")
            CY_ALIGN(CYDEV_FLS_ROW_SIZE) =
    #elif defined (__ICCARM__)
        #pragma data_alignment=CY_FLASH_SIZEOF_ROW
        #pragma location=".cy_checksum_exclude"
        const CY_BLE_FLASH_STORAGE cyBle_flashStorage =
    #endif  /* (__ARMCC_VERSION) */
    {
        { 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
        0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u }, 
        {{
            0x00u, 0x00u,
            0x01u, 0x00u,
            0x01u, 0x00u,
            0x00u, 0x00u,
            0x02u, 0x00u,
            0x01u, 0x00u,
            0x01u, 0x00u,
            0x01u, 0x00u,
            0x01u, 0x00u,
            0x01u, 0x00u,
        },
        {
            0x00u, 0x00u,
            0x01u, 0x00u,
            0x01u, 0x00u,
            0x00u, 0x00u,
            0x02u, 0x00u,
            0x01u, 0x00u,
            0x01u, 0x00u,
            0x01u, 0x00u,
            0x01u, 0x00u,
            0x01u, 0x00u,
        },
        {
            0x00u, 0x00u,
            0x01u, 0x00u,
            0x01u, 0x00u,
            0x00u, 0x00u,
            0x02u, 0x00u,
            0x01u, 0x00u,
            0x01u, 0x00u,
            0x01u, 0x00u,
            0x01u, 0x00u,
            0x01u, 0x00u,
        },
        {
            0x00u, 0x00u,
            0x01u, 0x00u,
            0x01u, 0x00u,
            0x00u, 0x00u,
            0x02u, 0x00u,
            0x01u, 0x00u,
            0x01u, 0x00u,
            0x01u, 0x00u,
            0x01u, 0x00u,
            0x01u, 0x00u,
        },
        {
            0x00u, 0x00u,
            0x01u, 0x00u,
            0x01u, 0x00u,
            0x00u, 0x00u,
            0x02u, 0x00u,
            0x01u, 0x00u,
            0x01u, 0x00u,
            0x01u, 0x00u,
            0x01u, 0x00u,
            0x01u, 0x00u,
        }}, 
        0x14u, /* CYBLE_GATT_DB_CCCD_COUNT */ 
        0x05u, /* CYBLE_GAP_MAX_BONDED_DEVICE */ 
    };
#endif /* (CYBLE_MODE_PROFILE) */

#endif  /* (CYBLE_MODE_PROFILE) && (CYBLE_BONDING_REQUIREMENT == CYBLE_BONDING_YES) */

#if(CYBLE_GATT_ROLE_SERVER)
    
    const CYBLE_GATTS_T cyBle_gatts =
{
    0x000Au,    /* Handle of the GATT service */
    0x000Cu,    /* Handle of the Service Changed characteristic */
    0x000Du,    /* Handle of the Client Characteristic Configuration descriptor */
};
    
    static uint8 cyBle_attValues[0x045Eu] = {
    /* Device Name */
    (uint8)'K', (uint8)'W', (uint8)'J', (uint8)' ', (uint8)'B', (uint8)'e', (uint8)'t', (uint8)'a', (uint8)' ',
    (uint8)'1',

    /* Appearance */
    0x00u, 0x00u,

    /* Peripheral Preferred Connection Parameters */
    0x28u, 0x00u, 0x50u, 0x00u, 0x01u, 0x00u, 0x58u, 0x02u,

    /* Central Address Resolution */
    0x00u,

    /* Service Changed */
    0x00u, 0x00u, 0x00u, 0x00u,

    /* CO */
    0x00u,

    /* Measurement */
    0x00u, 0x00u, 0xFBu, 0x34u, 0x9Bu, 0x5Fu, 0x80u, 0x00u, 0x00u, 0x80u, 0x00u, 0x10u, 0x01u, 0x00u, 0x01u, 0x00u,
    0x03u, 0xC0u,

    /* Units */
    (uint8)'p', (uint8)'p', (uint8)'m', 0xFBu, 0x34u, 0x9Bu, 0x5Fu, 0x80u, 0x00u, 0x00u, 0x80u, 0x00u, 0x10u, 0x02u,
    0x00u, 0x01u, 0x00u, 0x03u, 0xC0u,

    /* Characteristic User Description */
    (uint8)'C', (uint8)'O',

    /* O3 */
    0x00u,

    /* Measurement */
    0x00u, 0x00u, 0xFBu, 0x34u, 0x9Bu, 0x5Fu, 0x80u, 0x00u, 0x00u, 0x80u, 0x00u, 0x10u, 0x01u, 0x00u, 0x02u, 0x00u,
    0x03u, 0xC0u,

    /* Units */
    (uint8)'p', (uint8)'p', (uint8)'b', 0xFBu, 0x34u, 0x9Bu, 0x5Fu, 0x80u, 0x00u, 0x00u, 0x80u, 0x00u, 0x10u, 0x02u,
    0x00u, 0x02u, 0x00u, 0x03u, 0xC0u,

    /* Characteristic User Description */
    (uint8)'O', (uint8)'3',

    /* LOG */
    0x00u,

    /* Timestamp */
    0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0xFBu, 0x34u, 0x9Bu, 0x5Fu, 0x80u, 0x00u, 0x00u, 0x80u, 0x00u, 0x10u, 0x01u,
    0x00u, 0x03u, 0x00u, 0x03u, 0xC0u,

    /* Base16 */
    0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0xFBu, 0x34u, 0x9Bu, 0x5Fu, 0x80u, 0x00u, 0x00u, 0x80u,
    0x00u, 0x10u, 0x02u, 0x00u, 0x03u, 0x00u, 0x03u, 0xC0u,

    /* Base32 */
    0x00u, 0x00u, 0x00u, 0x00u, 0xFBu, 0x34u, 0x9Bu, 0x5Fu, 0x80u, 0x00u, 0x00u, 0x80u, 0x00u, 0x10u, 0x03u, 0x00u,
    0x03u, 0x00u, 0x03u, 0xC0u,

    /* Type */
    0x00u, 0x00u, 0xFBu, 0x34u, 0x9Bu, 0x5Fu, 0x80u, 0x00u, 0x00u, 0x80u, 0x00u, 0x10u, 0x04u, 0x00u, 0x03u, 0x00u,
    0x03u, 0xC0u,

    /* Length */
    0x00u, 0x00u, 0xFBu, 0x34u, 0x9Bu, 0x5Fu, 0x80u, 0x00u, 0x00u, 0x80u, 0x00u, 0x10u, 0x05u, 0x00u, 0x03u, 0x00u,
    0x03u, 0xC0u,

    /* Data */
    0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
    0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
    0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
    0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
    0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
    0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
    0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
    0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,
    0x00u, 0x00u, 0x00u, 0x00u, 0xFBu, 0x34u, 0x9Bu, 0x5Fu, 0x80u, 0x00u, 0x00u, 0x80u, 0x00u, 0x10u, 0x06u, 0x00u,
    0x03u, 0x00u, 0x03u, 0xC0u,

    /* Characteristic User Description */
    (uint8)'L', (uint8)'O', (uint8)'G',

    /* CAL */
    0x00u,

    /* CO Raw */
    0x00u, 0x00u, 0xFBu, 0x34u, 0x9Bu, 0x5Fu, 0x80u, 0x00u, 0x00u, 0x80u, 0x00u, 0x10u, 0x01u, 0x00u, 0x04u, 0x00u,
    0x03u, 0xC0u,

    /* CO Gain */
    0x00u, 0x00u, 0x00u, 0x00u, 0xFBu, 0x34u, 0x9Bu, 0x5Fu, 0x80u, 0x00u, 0x00u, 0x80u, 0x00u, 0x10u, 0x02u, 0x00u,
    0x04u, 0x00u, 0x03u, 0xC0u,

    /* CO OS */
    0x00u, 0x00u, 0x00u, 0x00u, 0xFBu, 0x34u, 0x9Bu, 0x5Fu, 0x80u, 0x00u, 0x00u, 0x80u, 0x00u, 0x10u, 0x03u, 0x00u,
    0x04u, 0x00u, 0x03u, 0xC0u,

    /* CO TZero TC */
    0x00u, 0x00u, 0x00u, 0x00u, 0xFBu, 0x34u, 0x9Bu, 0x5Fu, 0x80u, 0x00u, 0x00u, 0x80u, 0x00u, 0x10u, 0x04u, 0x00u,
    0x04u, 0x00u, 0x03u, 0xC0u,

    /* CO SnsFac */
    0x00u, 0x00u, 0x00u, 0x00u, 0xFBu, 0x34u, 0x9Bu, 0x5Fu, 0x80u, 0x00u, 0x00u, 0x80u, 0x00u, 0x10u, 0x05u, 0x00u,
    0x04u, 0x00u, 0x03u, 0xC0u,

    /* CO MOD */
    0x00u, 0x00u, 0xFBu, 0x34u, 0x9Bu, 0x5Fu, 0x80u, 0x00u, 0x00u, 0x80u, 0x00u, 0x80u, 0x06u, 0x00u, 0x04u, 0x00u,
    0x03u, 0xC0u,

    /* CO UNH */
    0x00u, 0x00u, 0xFBu, 0x34u, 0x9Bu, 0x5Fu, 0x80u, 0x00u, 0x00u, 0x80u, 0x00u, 0x80u, 0x07u, 0x00u, 0x04u, 0x00u,
    0x03u, 0xC0u,

    /* CO HAZ */
    0x00u, 0x00u, 0xFBu, 0x34u, 0x9Bu, 0x5Fu, 0x80u, 0x00u, 0x00u, 0x80u, 0x00u, 0x10u, 0x08u, 0x00u, 0x04u, 0x00u,
    0x03u, 0xC0u,

    /* O3 Raw */
    0x00u, 0x00u, 0xFBu, 0x34u, 0x9Bu, 0x5Fu, 0x80u, 0x00u, 0x00u, 0x80u, 0x00u, 0x10u, 0x09u, 0x00u, 0x04u, 0x00u,
    0x03u, 0xC0u,

    /* O3 Gain */
    0x00u, 0x00u, 0x00u, 0x00u, 0xFBu, 0x34u, 0x9Bu, 0x5Fu, 0x80u, 0x00u, 0x00u, 0x80u, 0x00u, 0x10u, 0x0Au, 0x00u,
    0x04u, 0x00u, 0x03u, 0xC0u,

    /* O3 OS */
    0x00u, 0x00u, 0x00u, 0x00u, 0xFBu, 0x34u, 0x9Bu, 0x5Fu, 0x80u, 0x00u, 0x00u, 0x80u, 0x00u, 0x10u, 0x0Bu, 0x00u,
    0x04u, 0x00u, 0x03u, 0xC0u,

    /* O3 TZero TC */
    0x00u, 0x00u, 0x00u, 0x00u, 0xFBu, 0x34u, 0x9Bu, 0x5Fu, 0x80u, 0x00u, 0x00u, 0x80u, 0x00u, 0x10u, 0x0Cu, 0x00u,
    0x04u, 0x00u, 0x03u, 0xC0u,

    /* O3 SnsFac */
    0x00u, 0x00u, 0x00u, 0x00u, 0xFBu, 0x34u, 0x9Bu, 0x5Fu, 0x80u, 0x00u, 0x00u, 0x80u, 0x00u, 0x10u, 0x0Du, 0x00u,
    0x04u, 0x00u, 0x03u, 0xC0u,

    /* O3 MOD */
    0x00u, 0x00u, 0xFBu, 0x34u, 0x9Bu, 0x5Fu, 0x80u, 0x00u, 0x00u, 0x80u, 0x00u, 0x10u, 0x0Eu, 0x00u, 0x04u, 0x00u,
    0x03u, 0xC0u,

    /* O3 UNH */
    0x00u, 0x00u, 0xFBu, 0x34u, 0x9Bu, 0x5Fu, 0x80u, 0x00u, 0x00u, 0x80u, 0x00u, 0x10u, 0x0Fu, 0x00u, 0x04u, 0x00u,
    0x03u, 0xC0u,

    /* O3 HAZ */
    0x00u, 0x00u, 0xFBu, 0x34u, 0x9Bu, 0x5Fu, 0x80u, 0x00u, 0x00u, 0x80u, 0x00u, 0x10u, 0x10u, 0x00u, 0x04u, 0x00u,
    0x03u, 0xC0u,

    /* T Raw */
    0x00u, 0x00u, 0xFBu, 0x34u, 0x9Bu, 0x5Fu, 0x80u, 0x00u, 0x00u, 0x80u, 0x00u, 0x10u, 0x11u, 0x00u, 0x04u, 0x00u,
    0x03u, 0xC0u,

    /* T Gain */
    0x00u, 0x00u, 0x00u, 0x00u, 0xFBu, 0x34u, 0x9Bu, 0x5Fu, 0x80u, 0x00u, 0x00u, 0x80u, 0x00u, 0x10u, 0x12u, 0x00u,
    0x04u, 0x00u, 0x03u, 0xC0u,

    /* T OS */
    0x00u, 0x00u, 0x00u, 0x00u, 0xFBu, 0x34u, 0x9Bu, 0x5Fu, 0x80u, 0x00u, 0x00u, 0x80u, 0x00u, 0x10u, 0x13u, 0x00u,
    0x04u, 0x00u, 0x03u, 0xC0u,

    /* RH Raw */
    0x00u, 0x00u, 0xFBu, 0x34u, 0x9Bu, 0x5Fu, 0x80u, 0x00u, 0x00u, 0x80u, 0x00u, 0x10u, 0x14u, 0x00u, 0x04u, 0x00u,
    0x03u, 0xC0u,

    /* RH Gain */
    0x00u, 0x00u, 0x00u, 0x00u, 0xFBu, 0x34u, 0x9Bu, 0x5Fu, 0x80u, 0x00u, 0x00u, 0x80u, 0x00u, 0x10u, 0x15u, 0x00u,
    0x04u, 0x00u, 0x03u, 0xC0u,

    /* RH OS */
    0x00u, 0x00u, 0x00u, 0x00u, 0xFBu, 0x34u, 0x9Bu, 0x5Fu, 0x80u, 0x00u, 0x00u, 0x80u, 0x00u, 0x10u, 0x16u, 0x00u,
    0x04u, 0x00u, 0x03u, 0xC0u,

    /* P Raw */
    0x00u, 0x00u, 0x00u, 0x00u, 0xFBu, 0x34u, 0x9Bu, 0x5Fu, 0x80u, 0x00u, 0x00u, 0x80u, 0x00u, 0x10u, 0x17u, 0x00u,
    0x04u, 0x00u, 0x03u, 0xC0u,

    /* P Gain */
    0x00u, 0x00u, 0x00u, 0x00u, 0xFBu, 0x34u, 0x9Bu, 0x5Fu, 0x80u, 0x00u, 0x00u, 0x80u, 0x00u, 0x10u, 0x18u, 0x00u,
    0x04u, 0x00u, 0x03u, 0xC0u,

    /* P OS */
    0x00u, 0x00u, 0x00u, 0x00u, 0xFBu, 0x34u, 0x9Bu, 0x5Fu, 0x80u, 0x00u, 0x00u, 0x80u, 0x00u, 0x10u, 0x19u, 0x00u,
    0x04u, 0x00u, 0x03u, 0xC0u,

    /* BAT Raw */
    0x00u, 0x00u, 0xFBu, 0x34u, 0x9Bu, 0x5Fu, 0x80u, 0x00u, 0x00u, 0x80u, 0x00u, 0x10u, 0x1Au, 0x00u, 0x04u, 0x00u,
    0x03u, 0xC0u,

    /* BAT Gain */
    0x00u, 0x00u, 0x00u, 0x00u, 0xFBu, 0x34u, 0x9Bu, 0x5Fu, 0x80u, 0x00u, 0x00u, 0x80u, 0x00u, 0x10u, 0x1Bu, 0x00u,
    0x04u, 0x00u, 0x03u, 0xC0u,

    /* BAT OS */
    0x00u, 0x00u, 0x00u, 0x00u, 0xFBu, 0x34u, 0x9Bu, 0x5Fu, 0x80u, 0x00u, 0x00u, 0x80u, 0x00u, 0x10u, 0x1Cu, 0x00u,
    0x04u, 0x00u, 0x03u, 0xC0u,

    /* BAT LO */
    0x00u, 0xFBu, 0x34u, 0x9Bu, 0x5Fu, 0x80u, 0x00u, 0x00u, 0x80u, 0x00u, 0x10u, 0x1Du, 0x00u, 0x04u, 0x00u, 0x03u,
    0xC0u,

    /* BAT CRIT */
    0x00u, 0xFBu, 0x34u, 0x9Bu, 0x5Fu, 0x80u, 0x00u, 0x00u, 0x80u, 0x00u, 0x10u, 0x1Eu, 0x00u, 0x04u, 0x00u, 0x03u,
    0xC0u,

    /* Characteristic User Description */
    (uint8)'C', (uint8)'A', (uint8)'L',

    /* Current Time */
    0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0xFBu, 0x34u, 0x9Bu, 0x5Fu, 0x80u, 0x00u, 0x00u, 0x80u, 0x00u, 0x10u,
    0x1Fu, 0x00u, 0x04u, 0x00u, 0x03u, 0xC0u,

    /* Descriptor Value Changed */
    0x00u, 0x00u,

    /* Humidity */
    0x00u, 0x00u,

    /* Environmental Sensing Measurement */
    0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,

    /* Environmental Sensing Trigger Setting */
    0x00u, 0x00u,

    /* Characteristic User Description */
    

    /* Valid Range */
    0x00u, 0x00u, 0xFFu, 0xFFu,

    /* Pressure */
    0x00u, 0x00u, 0x00u, 0x00u,

    /* Environmental Sensing Measurement */
    0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,

    /* Environmental Sensing Trigger Setting */
    0x00u, 0x00u,

    /* Characteristic User Description */
    

    /* Valid Range */
    0x00u, 0x00u, 0x00u, 0x00u, 0xFFu, 0xFFu, 0xFFu, 0xFFu,

    /* Temperature */
    0x00u, 0x00u,

    /* Environmental Sensing Measurement */
    0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,

    /* Environmental Sensing Trigger Setting */
    0x00u, 0x00u,

    /* Characteristic User Description */
    

    /* Valid Range */
    0x00u, 0x80u, 0xFFu, 0x7Fu,

    /* Battery Level */
    0x00u,

    /* Characteristic Presentation Format */
    0x00u, 0x00u, 0xADu, 0x27u, 0x01u, 0x00u, 0x00u,

    /* Manufacturer Name String */
    (uint8)'\0', (uint8)'\0', (uint8)'\0', (uint8)'\0', (uint8)'\0', (uint8)'\0', (uint8)'\0',

    /* Model Number String */
    (uint8)'\0', (uint8)'\0', (uint8)'\0', (uint8)'\0', (uint8)'\0', (uint8)'\0', (uint8)'\0', (uint8)'\0', (uint8)'\0',
    (uint8)'\0', (uint8)'\0', (uint8)'\0', (uint8)'\0', (uint8)'\0', (uint8)'\0',

    /* Serial Number String */
    (uint8)'\0', (uint8)'\0', (uint8)'\0', (uint8)'\0', (uint8)'\0', (uint8)'\0',

    /* Hardware Revision String */
    (uint8)'\0', (uint8)'\0', (uint8)'\0', (uint8)'\0', (uint8)'\0', (uint8)'\0', (uint8)'\0', (uint8)'\0', (uint8)'\0',
    (uint8)'\0', (uint8)'\0', (uint8)'\0', (uint8)'\0', (uint8)'\0', (uint8)'\0',

    /* Firmware Revision String */
    (uint8)'\0', (uint8)'\0', (uint8)'\0', (uint8)'\0', (uint8)'\0', (uint8)'\0', (uint8)'\0', (uint8)'\0', (uint8)'\0',
    (uint8)'\0', (uint8)'\0', (uint8)'\0', (uint8)'\0', (uint8)'\0', (uint8)'\0',

    /* Software Revision String */
    (uint8)'\0', (uint8)'\0', (uint8)'\0', (uint8)'\0', (uint8)'\0', (uint8)'\0', (uint8)'\0', (uint8)'\0', (uint8)'\0',
    (uint8)'\0', (uint8)'\0', (uint8)'\0', (uint8)'\0', (uint8)'\0', (uint8)'\0',

    /* System ID */
    0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,

    /* IEEE 11073-20601 Regulatory Certification Data List */
    0x00u,

    /* PnP ID */
    0x01u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u,

};
#if(CYBLE_GATT_DB_CCCD_COUNT != 0u)
uint8 cyBle_attValuesCCCD[CYBLE_GATT_DB_CCCD_COUNT];
#endif /* CYBLE_GATT_DB_CCCD_COUNT != 0u */

const uint8 cyBle_attUuid128[][16u] = {
    /* KWJ */
    { 0xFBu, 0x34u, 0x9Bu, 0x5Fu, 0x80u, 0x00u, 0x00u, 0x80u, 0x00u, 0x10u, 0x00u, 0x00u, 0x00u, 0x00u, 0x03u, 0xC0u },
    /* CO */
    { 0xFBu, 0x34u, 0x9Bu, 0x5Fu, 0x80u, 0x00u, 0x00u, 0x80u, 0x00u, 0x10u, 0x00u, 0x00u, 0x01u, 0x00u, 0x03u, 0xC0u },
    /* O3 */
    { 0xFBu, 0x34u, 0x9Bu, 0x5Fu, 0x80u, 0x00u, 0x00u, 0x80u, 0x00u, 0x10u, 0x00u, 0x00u, 0x02u, 0x00u, 0x03u, 0xC0u },
    /* LOG */
    { 0xFBu, 0x34u, 0x9Bu, 0x5Fu, 0x80u, 0x00u, 0x00u, 0x80u, 0x00u, 0x10u, 0x00u, 0x00u, 0x03u, 0x00u, 0x03u, 0xC0u },
    /* CAL */
    { 0xFBu, 0x34u, 0x9Bu, 0x5Fu, 0x80u, 0x00u, 0x00u, 0x80u, 0x00u, 0x10u, 0x00u, 0x00u, 0x04u, 0x00u, 0x03u, 0xC0u },
};

CYBLE_GATTS_ATT_GEN_VAL_LEN_T cyBle_attValuesLen[CYBLE_GATT_DB_ATT_VAL_COUNT] = {
    { 0x000Au, (void *)&cyBle_attValues[0] }, /* Device Name */
    { 0x0002u, (void *)&cyBle_attValues[10] }, /* Appearance */
    { 0x0008u, (void *)&cyBle_attValues[12] }, /* Peripheral Preferred Connection Parameters */
    { 0x0001u, (void *)&cyBle_attValues[20] }, /* Central Address Resolution */
    { 0x0004u, (void *)&cyBle_attValues[21] }, /* Service Changed */
    { 0x0002u, (void *)&cyBle_attValuesCCCD[0] }, /* Client Characteristic Configuration */
    { 0x0010u, (void *)&cyBle_attUuid128[0] }, /* KWJ UUID */
    { 0x0010u, (void *)&cyBle_attUuid128[1] }, /* CO UUID */
    { 0x0001u, (void *)&cyBle_attValues[25] }, /* CO */
    { 0x0002u, (void *)&cyBle_attValues[26] }, /* Measurement */
    { 0x0002u, (void *)&cyBle_attValuesCCCD[2] }, /* Client Characteristic Configuration */
    { 0x0003u, (void *)&cyBle_attValues[44] }, /* Units */
    { 0x0002u, (void *)&cyBle_attValues[63] }, /* Characteristic User Description */
    { 0x0010u, (void *)&cyBle_attUuid128[2] }, /* O3 UUID */
    { 0x0001u, (void *)&cyBle_attValues[65] }, /* O3 */
    { 0x0002u, (void *)&cyBle_attValues[66] }, /* Measurement */
    { 0x0002u, (void *)&cyBle_attValuesCCCD[4] }, /* Client Characteristic Configuration */
    { 0x0003u, (void *)&cyBle_attValues[84] }, /* Units */
    { 0x0002u, (void *)&cyBle_attValues[103] }, /* Characteristic User Description */
    { 0x0010u, (void *)&cyBle_attUuid128[3] }, /* LOG UUID */
    { 0x0001u, (void *)&cyBle_attValues[105] }, /* LOG */
    { 0x0002u, (void *)&cyBle_attValuesCCCD[6] }, /* Client Characteristic Configuration */
    { 0x0005u, (void *)&cyBle_attValues[106] }, /* Timestamp */
    { 0x0008u, (void *)&cyBle_attValues[127] }, /* Base16 */
    { 0x0004u, (void *)&cyBle_attValues[151] }, /* Base32 */
    { 0x0002u, (void *)&cyBle_attValues[171] }, /* Type */
    { 0x0002u, (void *)&cyBle_attValues[189] }, /* Length */
    { 0x0084u, (void *)&cyBle_attValues[207] }, /* Data */
    { 0x0003u, (void *)&cyBle_attValues[355] }, /* Characteristic User Description */
    { 0x0010u, (void *)&cyBle_attUuid128[4] }, /* CAL UUID */
    { 0x0001u, (void *)&cyBle_attValues[358] }, /* CAL */
    { 0x0002u, (void *)&cyBle_attValues[359] }, /* CO Raw */
    { 0x0004u, (void *)&cyBle_attValues[377] }, /* CO Gain */
    { 0x0004u, (void *)&cyBle_attValues[397] }, /* CO OS */
    { 0x0004u, (void *)&cyBle_attValues[417] }, /* CO TZero TC */
    { 0x0004u, (void *)&cyBle_attValues[437] }, /* CO SnsFac */
    { 0x0002u, (void *)&cyBle_attValues[457] }, /* CO MOD */
    { 0x0002u, (void *)&cyBle_attValues[475] }, /* CO UNH */
    { 0x0002u, (void *)&cyBle_attValues[493] }, /* CO HAZ */
    { 0x0002u, (void *)&cyBle_attValues[511] }, /* O3 Raw */
    { 0x0004u, (void *)&cyBle_attValues[529] }, /* O3 Gain */
    { 0x0004u, (void *)&cyBle_attValues[549] }, /* O3 OS */
    { 0x0004u, (void *)&cyBle_attValues[569] }, /* O3 TZero TC */
    { 0x0004u, (void *)&cyBle_attValues[589] }, /* O3 SnsFac */
    { 0x0002u, (void *)&cyBle_attValues[609] }, /* O3 MOD */
    { 0x0002u, (void *)&cyBle_attValues[627] }, /* O3 UNH */
    { 0x0002u, (void *)&cyBle_attValues[645] }, /* O3 HAZ */
    { 0x0002u, (void *)&cyBle_attValues[663] }, /* T Raw */
    { 0x0004u, (void *)&cyBle_attValues[681] }, /* T Gain */
    { 0x0004u, (void *)&cyBle_attValues[701] }, /* T OS */
    { 0x0002u, (void *)&cyBle_attValues[721] }, /* RH Raw */
    { 0x0004u, (void *)&cyBle_attValues[739] }, /* RH Gain */
    { 0x0004u, (void *)&cyBle_attValues[759] }, /* RH OS */
    { 0x0004u, (void *)&cyBle_attValues[779] }, /* P Raw */
    { 0x0004u, (void *)&cyBle_attValues[799] }, /* P Gain */
    { 0x0004u, (void *)&cyBle_attValues[819] }, /* P OS */
    { 0x0002u, (void *)&cyBle_attValues[839] }, /* BAT Raw */
    { 0x0004u, (void *)&cyBle_attValues[857] }, /* BAT Gain */
    { 0x0004u, (void *)&cyBle_attValues[877] }, /* BAT OS */
    { 0x0001u, (void *)&cyBle_attValues[897] }, /* BAT LO */
    { 0x0001u, (void *)&cyBle_attValues[914] }, /* BAT CRIT */
    { 0x0002u, (void *)&cyBle_attValuesCCCD[8] }, /* Client Characteristic Configuration */
    { 0x0003u, (void *)&cyBle_attValues[931] }, /* Characteristic User Description */
    { 0x0006u, (void *)&cyBle_attValues[934] }, /* Current Time */
    { 0x0002u, (void *)&cyBle_attValues[956] }, /* Descriptor Value Changed */
    { 0x0002u, (void *)&cyBle_attValuesCCCD[10] }, /* Client Characteristic Configuration */
    { 0x0002u, (void *)&cyBle_attValues[958] }, /* Humidity */
    { 0x000Bu, (void *)&cyBle_attValues[960] }, /* Environmental Sensing Measurement */
    { 0x0002u, (void *)&cyBle_attValues[971] }, /* Environmental Sensing Trigger Setting */
    { 0x0000u, (void *)&cyBle_attValues[973] }, /* Characteristic User Description */
    { 0x0004u, (void *)&cyBle_attValues[973] }, /* Valid Range */
    { 0x0002u, (void *)&cyBle_attValuesCCCD[12] }, /* Client Characteristic Configuration */
    { 0x0004u, (void *)&cyBle_attValues[977] }, /* Pressure */
    { 0x000Bu, (void *)&cyBle_attValues[981] }, /* Environmental Sensing Measurement */
    { 0x0002u, (void *)&cyBle_attValues[992] }, /* Environmental Sensing Trigger Setting */
    { 0x0000u, (void *)&cyBle_attValues[994] }, /* Characteristic User Description */
    { 0x0008u, (void *)&cyBle_attValues[994] }, /* Valid Range */
    { 0x0002u, (void *)&cyBle_attValuesCCCD[14] }, /* Client Characteristic Configuration */
    { 0x0002u, (void *)&cyBle_attValues[1002] }, /* Temperature */
    { 0x000Bu, (void *)&cyBle_attValues[1004] }, /* Environmental Sensing Measurement */
    { 0x0002u, (void *)&cyBle_attValues[1015] }, /* Environmental Sensing Trigger Setting */
    { 0x0000u, (void *)&cyBle_attValues[1017] }, /* Characteristic User Description */
    { 0x0004u, (void *)&cyBle_attValues[1017] }, /* Valid Range */
    { 0x0002u, (void *)&cyBle_attValuesCCCD[16] }, /* Client Characteristic Configuration */
    { 0x0001u, (void *)&cyBle_attValues[1021] }, /* Battery Level */
    { 0x0007u, (void *)&cyBle_attValues[1022] }, /* Characteristic Presentation Format */
    { 0x0002u, (void *)&cyBle_attValuesCCCD[18] }, /* Client Characteristic Configuration */
    { 0x0007u, (void *)&cyBle_attValues[1029] }, /* Manufacturer Name String */
    { 0x000Fu, (void *)&cyBle_attValues[1036] }, /* Model Number String */
    { 0x0006u, (void *)&cyBle_attValues[1051] }, /* Serial Number String */
    { 0x000Fu, (void *)&cyBle_attValues[1057] }, /* Hardware Revision String */
    { 0x000Fu, (void *)&cyBle_attValues[1072] }, /* Firmware Revision String */
    { 0x000Fu, (void *)&cyBle_attValues[1087] }, /* Software Revision String */
    { 0x0008u, (void *)&cyBle_attValues[1102] }, /* System ID */
    { 0x0001u, (void *)&cyBle_attValues[1110] }, /* IEEE 11073-20601 Regulatory Certification Data List */
    { 0x0007u, (void *)&cyBle_attValues[1111] }, /* PnP ID */
};

const CYBLE_GATTS_DB_T cyBle_gattDB[0x79u] = {
    { 0x0001u, 0x2800u /* Primary service                     */, 0x00000001u /*           */, 0x0009u, {{0x1800u, NULL}}                           },
    { 0x0002u, 0x2803u /* Characteristic                      */, 0x00020001u /* rd        */, 0x0003u, {{0x2A00u, NULL}}                           },
    { 0x0003u, 0x2A00u /* Device Name                         */, 0x01020001u /* rd        */, 0x0003u, {{0x000Au, (void *)&cyBle_attValuesLen[0]}} },
    { 0x0004u, 0x2803u /* Characteristic                      */, 0x00020001u /* rd        */, 0x0005u, {{0x2A01u, NULL}}                           },
    { 0x0005u, 0x2A01u /* Appearance                          */, 0x01020001u /* rd        */, 0x0005u, {{0x0002u, (void *)&cyBle_attValuesLen[1]}} },
    { 0x0006u, 0x2803u /* Characteristic                      */, 0x00020001u /* rd        */, 0x0007u, {{0x2A04u, NULL}}                           },
    { 0x0007u, 0x2A04u /* Peripheral Preferred Connection Par */, 0x01020001u /* rd        */, 0x0007u, {{0x0008u, (void *)&cyBle_attValuesLen[2]}} },
    { 0x0008u, 0x2803u /* Characteristic                      */, 0x00020001u /* rd        */, 0x0009u, {{0x2AA6u, NULL}}                           },
    { 0x0009u, 0x2AA6u /* Central Address Resolution          */, 0x01020001u /* rd        */, 0x0009u, {{0x0001u, (void *)&cyBle_attValuesLen[3]}} },
    { 0x000Au, 0x2800u /* Primary service                     */, 0x00000001u /*           */, 0x000Du, {{0x1801u, NULL}}                           },
    { 0x000Bu, 0x2803u /* Characteristic                      */, 0x00200001u /* ind       */, 0x000Du, {{0x2A05u, NULL}}                           },
    { 0x000Cu, 0x2A05u /* Service Changed                     */, 0x01200000u /* ind       */, 0x000Du, {{0x0004u, (void *)&cyBle_attValuesLen[4]}} },
    { 0x000Du, 0x2902u /* Client Characteristic Configuration */, 0x010A0101u /* rd,wr     */, 0x000Du, {{0x0002u, (void *)&cyBle_attValuesLen[5]}} },
    { 0x000Eu, 0x2800u /* Primary service                     */, 0x08000001u /*           */, 0x0048u, {{0x0010u, (void *)&cyBle_attValuesLen[6]}} },
    { 0x000Fu, 0x2802u /* Included service                    */, 0x00000001u /*           */, 0x000Fu, {{0x0067u, NULL}}                           },
    { 0x0010u, 0x2803u /* Characteristic                      */, 0x00120001u /* rd,ntf    */, 0x0015u, {{0x0010u, (void *)&cyBle_attValuesLen[7]}} },
    { 0x0011u, 0x0001u /* CO                                  */, 0x09120001u /* rd,ntf    */, 0x0015u, {{0x0001u, (void *)&cyBle_attValuesLen[8]}} },
    { 0x0012u, 0x0001u /* Measurement                         */, 0x09000001u /*           */, 0x0012u, {{0x0002u, (void *)&cyBle_attValuesLen[9]}} },
    { 0x0013u, 0x2902u /* Client Characteristic Configuration */, 0x010A0101u /* rd,wr     */, 0x0013u, {{0x0002u, (void *)&cyBle_attValuesLen[10]}} },
    { 0x0014u, 0x0001u /* Units                               */, 0x09000101u /*           */, 0x0014u, {{0x0003u, (void *)&cyBle_attValuesLen[11]}} },
    { 0x0015u, 0x2901u /* Characteristic User Description     */, 0x01020001u /* rd        */, 0x0015u, {{0x0002u, (void *)&cyBle_attValuesLen[12]}} },
    { 0x0016u, 0x2803u /* Characteristic                      */, 0x00120001u /* rd,ntf    */, 0x001Bu, {{0x0010u, (void *)&cyBle_attValuesLen[13]}} },
    { 0x0017u, 0x0002u /* O3                                  */, 0x09120001u /* rd,ntf    */, 0x001Bu, {{0x0001u, (void *)&cyBle_attValuesLen[14]}} },
    { 0x0018u, 0x0002u /* Measurement                         */, 0x09000001u /*           */, 0x0018u, {{0x0002u, (void *)&cyBle_attValuesLen[15]}} },
    { 0x0019u, 0x2902u /* Client Characteristic Configuration */, 0x010A0101u /* rd,wr     */, 0x0019u, {{0x0002u, (void *)&cyBle_attValuesLen[16]}} },
    { 0x001Au, 0x0002u /* Units                               */, 0x09000101u /*           */, 0x001Au, {{0x0003u, (void *)&cyBle_attValuesLen[17]}} },
    { 0x001Bu, 0x2901u /* Characteristic User Description     */, 0x01020001u /* rd        */, 0x001Bu, {{0x0002u, (void *)&cyBle_attValuesLen[18]}} },
    { 0x001Cu, 0x2803u /* Characteristic                      */, 0x001A0001u /* rd,wr,ntf */, 0x0025u, {{0x0010u, (void *)&cyBle_attValuesLen[19]}} },
    { 0x001Du, 0x0003u /* LOG                                 */, 0x091A0101u /* rd,wr,ntf */, 0x0025u, {{0x0001u, (void *)&cyBle_attValuesLen[20]}} },
    { 0x001Eu, 0x2902u /* Client Characteristic Configuration */, 0x010A0101u /* rd,wr     */, 0x001Eu, {{0x0002u, (void *)&cyBle_attValuesLen[21]}} },
    { 0x001Fu, 0x0003u /* Timestamp                           */, 0x09000001u /*           */, 0x001Fu, {{0x0005u, (void *)&cyBle_attValuesLen[22]}} },
    { 0x0020u, 0x0003u /* Base16                              */, 0x09000001u /*           */, 0x0020u, {{0x0008u, (void *)&cyBle_attValuesLen[23]}} },
    { 0x0021u, 0x0003u /* Base32                              */, 0x09000001u /*           */, 0x0021u, {{0x0004u, (void *)&cyBle_attValuesLen[24]}} },
    { 0x0022u, 0x0003u /* Type                                */, 0x09000101u /*           */, 0x0022u, {{0x0002u, (void *)&cyBle_attValuesLen[25]}} },
    { 0x0023u, 0x0003u /* Length                              */, 0x09000001u /*           */, 0x0023u, {{0x0002u, (void *)&cyBle_attValuesLen[26]}} },
    { 0x0024u, 0x0003u /* Data                                */, 0x09000001u /*           */, 0x0024u, {{0x0084u, (void *)&cyBle_attValuesLen[27]}} },
    { 0x0025u, 0x2901u /* Characteristic User Description     */, 0x01020001u /* rd        */, 0x0025u, {{0x0003u, (void *)&cyBle_attValuesLen[28]}} },
    { 0x0026u, 0x2803u /* Characteristic                      */, 0x001A0001u /* rd,wr,ntf */, 0x0048u, {{0x0010u, (void *)&cyBle_attValuesLen[29]}} },
    { 0x0027u, 0x0004u /* CAL                                 */, 0x091A0101u /* rd,wr,ntf */, 0x0048u, {{0x0001u, (void *)&cyBle_attValuesLen[30]}} },
    { 0x0028u, 0x0004u /* CO Raw                              */, 0x09000001u /*           */, 0x0028u, {{0x0002u, (void *)&cyBle_attValuesLen[31]}} },
    { 0x0029u, 0x0004u /* CO Gain                             */, 0x09000101u /*           */, 0x0029u, {{0x0004u, (void *)&cyBle_attValuesLen[32]}} },
    { 0x002Au, 0x0004u /* CO OS                               */, 0x09000101u /*           */, 0x002Au, {{0x0004u, (void *)&cyBle_attValuesLen[33]}} },
    { 0x002Bu, 0x0004u /* CO TZero TC                         */, 0x09000101u /*           */, 0x002Bu, {{0x0004u, (void *)&cyBle_attValuesLen[34]}} },
    { 0x002Cu, 0x0004u /* CO SnsFac                           */, 0x09000101u /*           */, 0x002Cu, {{0x0004u, (void *)&cyBle_attValuesLen[35]}} },
    { 0x002Du, 0x0004u /* CO MOD                              */, 0x09000101u /*           */, 0x002Du, {{0x0002u, (void *)&cyBle_attValuesLen[36]}} },
    { 0x002Eu, 0x0004u /* CO UNH                              */, 0x09000101u /*           */, 0x002Eu, {{0x0002u, (void *)&cyBle_attValuesLen[37]}} },
    { 0x002Fu, 0x0004u /* CO HAZ                              */, 0x09000101u /*           */, 0x002Fu, {{0x0002u, (void *)&cyBle_attValuesLen[38]}} },
    { 0x0030u, 0x0004u /* O3 Raw                              */, 0x09000001u /*           */, 0x0030u, {{0x0002u, (void *)&cyBle_attValuesLen[39]}} },
    { 0x0031u, 0x0004u /* O3 Gain                             */, 0x09000101u /*           */, 0x0031u, {{0x0004u, (void *)&cyBle_attValuesLen[40]}} },
    { 0x0032u, 0x0004u /* O3 OS                               */, 0x09000101u /*           */, 0x0032u, {{0x0004u, (void *)&cyBle_attValuesLen[41]}} },
    { 0x0033u, 0x0004u /* O3 TZero TC                         */, 0x09000101u /*           */, 0x0033u, {{0x0004u, (void *)&cyBle_attValuesLen[42]}} },
    { 0x0034u, 0x0004u /* O3 SnsFac                           */, 0x09000101u /*           */, 0x0034u, {{0x0004u, (void *)&cyBle_attValuesLen[43]}} },
    { 0x0035u, 0x0004u /* O3 MOD                              */, 0x09000101u /*           */, 0x0035u, {{0x0002u, (void *)&cyBle_attValuesLen[44]}} },
    { 0x0036u, 0x0004u /* O3 UNH                              */, 0x09000101u /*           */, 0x0036u, {{0x0002u, (void *)&cyBle_attValuesLen[45]}} },
    { 0x0037u, 0x0004u /* O3 HAZ                              */, 0x09000101u /*           */, 0x0037u, {{0x0002u, (void *)&cyBle_attValuesLen[46]}} },
    { 0x0038u, 0x0004u /* T Raw                               */, 0x09000001u /*           */, 0x0038u, {{0x0002u, (void *)&cyBle_attValuesLen[47]}} },
    { 0x0039u, 0x0004u /* T Gain                              */, 0x09000101u /*           */, 0x0039u, {{0x0004u, (void *)&cyBle_attValuesLen[48]}} },
    { 0x003Au, 0x0004u /* T OS                                */, 0x09000101u /*           */, 0x003Au, {{0x0004u, (void *)&cyBle_attValuesLen[49]}} },
    { 0x003Bu, 0x0004u /* RH Raw                              */, 0x09000001u /*           */, 0x003Bu, {{0x0002u, (void *)&cyBle_attValuesLen[50]}} },
    { 0x003Cu, 0x0004u /* RH Gain                             */, 0x09000101u /*           */, 0x003Cu, {{0x0004u, (void *)&cyBle_attValuesLen[51]}} },
    { 0x003Du, 0x0004u /* RH OS                               */, 0x09000101u /*           */, 0x003Du, {{0x0004u, (void *)&cyBle_attValuesLen[52]}} },
    { 0x003Eu, 0x0004u /* P Raw                               */, 0x09000001u /*           */, 0x003Eu, {{0x0004u, (void *)&cyBle_attValuesLen[53]}} },
    { 0x003Fu, 0x0004u /* P Gain                              */, 0x09000101u /*           */, 0x003Fu, {{0x0004u, (void *)&cyBle_attValuesLen[54]}} },
    { 0x0040u, 0x0004u /* P OS                                */, 0x09000101u /*           */, 0x0040u, {{0x0004u, (void *)&cyBle_attValuesLen[55]}} },
    { 0x0041u, 0x0004u /* BAT Raw                             */, 0x09000001u /*           */, 0x0041u, {{0x0002u, (void *)&cyBle_attValuesLen[56]}} },
    { 0x0042u, 0x0004u /* BAT Gain                            */, 0x09000101u /*           */, 0x0042u, {{0x0004u, (void *)&cyBle_attValuesLen[57]}} },
    { 0x0043u, 0x0004u /* BAT OS                              */, 0x09000101u /*           */, 0x0043u, {{0x0004u, (void *)&cyBle_attValuesLen[58]}} },
    { 0x0044u, 0x0004u /* BAT LO                              */, 0x09000101u /*           */, 0x0044u, {{0x0001u, (void *)&cyBle_attValuesLen[59]}} },
    { 0x0045u, 0x0004u /* BAT CRIT                            */, 0x09000101u /*           */, 0x0045u, {{0x0001u, (void *)&cyBle_attValuesLen[60]}} },
    { 0x0046u, 0x2902u /* Client Characteristic Configuration */, 0x010A0101u /* rd,wr     */, 0x0046u, {{0x0002u, (void *)&cyBle_attValuesLen[61]}} },
    { 0x0047u, 0x2901u /* Characteristic User Description     */, 0x01020001u /* rd        */, 0x0047u, {{0x0003u, (void *)&cyBle_attValuesLen[62]}} },
    { 0x0048u, 0x0004u /* Current Time                        */, 0x09000101u /*           */, 0x0048u, {{0x0006u, (void *)&cyBle_attValuesLen[63]}} },
    { 0x0049u, 0x2800u /* Primary service                     */, 0x00000001u /*           */, 0x0061u, {{0x181Au, NULL}}                           },
    { 0x004Au, 0x2803u /* Characteristic                      */, 0x00200001u /* ind       */, 0x004Cu, {{0x2A7Du, NULL}}                           },
    { 0x004Bu, 0x2A7Du /* Descriptor Value Changed            */, 0x01200000u /* ind       */, 0x004Cu, {{0x0002u, (void *)&cyBle_attValuesLen[64]}} },
    { 0x004Cu, 0x2902u /* Client Characteristic Configuration */, 0x010A0101u /* rd,wr     */, 0x004Cu, {{0x0002u, (void *)&cyBle_attValuesLen[65]}} },
    { 0x004Du, 0x2803u /* Characteristic                      */, 0x00120001u /* rd,ntf    */, 0x0053u, {{0x2A6Fu, NULL}}                           },
    { 0x004Eu, 0x2A6Fu /* Humidity                            */, 0x01120001u /* rd,ntf    */, 0x0053u, {{0x0002u, (void *)&cyBle_attValuesLen[66]}} },
    { 0x004Fu, 0x290Cu /* Environmental Sensing Measurement   */, 0x01020001u /* rd        */, 0x004Fu, {{0x000Bu, (void *)&cyBle_attValuesLen[67]}} },
    { 0x0050u, 0x290Du /* Environmental Sensing Trigger Setti */, 0x01020001u /* rd        */, 0x0050u, {{0x0002u, (void *)&cyBle_attValuesLen[68]}} },
    { 0x0051u, 0x2901u /* Characteristic User Description     */, 0x01020001u /* rd        */, 0x0051u, {{0x0000u, (void *)&cyBle_attValuesLen[69]}} },
    { 0x0052u, 0x2906u /* Valid Range                         */, 0x01020001u /* rd        */, 0x0052u, {{0x0004u, (void *)&cyBle_attValuesLen[70]}} },
    { 0x0053u, 0x2902u /* Client Characteristic Configuration */, 0x010A0101u /* rd,wr     */, 0x0053u, {{0x0002u, (void *)&cyBle_attValuesLen[71]}} },
    { 0x0054u, 0x2803u /* Characteristic                      */, 0x00120001u /* rd,ntf    */, 0x005Au, {{0x2A6Du, NULL}}                           },
    { 0x0055u, 0x2A6Du /* Pressure                            */, 0x01120001u /* rd,ntf    */, 0x005Au, {{0x0004u, (void *)&cyBle_attValuesLen[72]}} },
    { 0x0056u, 0x290Cu /* Environmental Sensing Measurement   */, 0x01020001u /* rd        */, 0x0056u, {{0x000Bu, (void *)&cyBle_attValuesLen[73]}} },
    { 0x0057u, 0x290Du /* Environmental Sensing Trigger Setti */, 0x01020001u /* rd        */, 0x0057u, {{0x0002u, (void *)&cyBle_attValuesLen[74]}} },
    { 0x0058u, 0x2901u /* Characteristic User Description     */, 0x01020001u /* rd        */, 0x0058u, {{0x0000u, (void *)&cyBle_attValuesLen[75]}} },
    { 0x0059u, 0x2906u /* Valid Range                         */, 0x01020001u /* rd        */, 0x0059u, {{0x0008u, (void *)&cyBle_attValuesLen[76]}} },
    { 0x005Au, 0x2902u /* Client Characteristic Configuration */, 0x010A0101u /* rd,wr     */, 0x005Au, {{0x0002u, (void *)&cyBle_attValuesLen[77]}} },
    { 0x005Bu, 0x2803u /* Characteristic                      */, 0x00120001u /* rd,ntf    */, 0x0061u, {{0x2A6Eu, NULL}}                           },
    { 0x005Cu, 0x2A6Eu /* Temperature                         */, 0x01120001u /* rd,ntf    */, 0x0061u, {{0x0002u, (void *)&cyBle_attValuesLen[78]}} },
    { 0x005Du, 0x290Cu /* Environmental Sensing Measurement   */, 0x01020001u /* rd        */, 0x005Du, {{0x000Bu, (void *)&cyBle_attValuesLen[79]}} },
    { 0x005Eu, 0x290Du /* Environmental Sensing Trigger Setti */, 0x01020001u /* rd        */, 0x005Eu, {{0x0002u, (void *)&cyBle_attValuesLen[80]}} },
    { 0x005Fu, 0x2901u /* Characteristic User Description     */, 0x01020001u /* rd        */, 0x005Fu, {{0x0000u, (void *)&cyBle_attValuesLen[81]}} },
    { 0x0060u, 0x2906u /* Valid Range                         */, 0x01020001u /* rd        */, 0x0060u, {{0x0004u, (void *)&cyBle_attValuesLen[82]}} },
    { 0x0061u, 0x2902u /* Client Characteristic Configuration */, 0x010A0101u /* rd,wr     */, 0x0061u, {{0x0002u, (void *)&cyBle_attValuesLen[83]}} },
    { 0x0062u, 0x2800u /* Primary service                     */, 0x00000001u /*           */, 0x0066u, {{0x180Fu, NULL}}                           },
    { 0x0063u, 0x2803u /* Characteristic                      */, 0x00120001u /* rd,ntf    */, 0x0066u, {{0x2A19u, NULL}}                           },
    { 0x0064u, 0x2A19u /* Battery Level                       */, 0x01120001u /* rd,ntf    */, 0x0066u, {{0x0001u, (void *)&cyBle_attValuesLen[84]}} },
    { 0x0065u, 0x2904u /* Characteristic Presentation Format  */, 0x01020001u /* rd        */, 0x0065u, {{0x0007u, (void *)&cyBle_attValuesLen[85]}} },
    { 0x0066u, 0x2902u /* Client Characteristic Configuration */, 0x010A0101u /* rd,wr     */, 0x0066u, {{0x0002u, (void *)&cyBle_attValuesLen[86]}} },
    { 0x0067u, 0x2801u /* Secondary service                   */, 0x00000001u /*           */, 0x0079u, {{0x180Au, NULL}}                           },
    { 0x0068u, 0x2803u /* Characteristic                      */, 0x00020001u /* rd        */, 0x0069u, {{0x2A29u, NULL}}                           },
    { 0x0069u, 0x2A29u /* Manufacturer Name String            */, 0x01020001u /* rd        */, 0x0069u, {{0x0007u, (void *)&cyBle_attValuesLen[87]}} },
    { 0x006Au, 0x2803u /* Characteristic                      */, 0x00020001u /* rd        */, 0x006Bu, {{0x2A24u, NULL}}                           },
    { 0x006Bu, 0x2A24u /* Model Number String                 */, 0x01020001u /* rd        */, 0x006Bu, {{0x000Fu, (void *)&cyBle_attValuesLen[88]}} },
    { 0x006Cu, 0x2803u /* Characteristic                      */, 0x00020001u /* rd        */, 0x006Du, {{0x2A25u, NULL}}                           },
    { 0x006Du, 0x2A25u /* Serial Number String                */, 0x01020001u /* rd        */, 0x006Du, {{0x0006u, (void *)&cyBle_attValuesLen[89]}} },
    { 0x006Eu, 0x2803u /* Characteristic                      */, 0x00020001u /* rd        */, 0x006Fu, {{0x2A27u, NULL}}                           },
    { 0x006Fu, 0x2A27u /* Hardware Revision String            */, 0x01020001u /* rd        */, 0x006Fu, {{0x000Fu, (void *)&cyBle_attValuesLen[90]}} },
    { 0x0070u, 0x2803u /* Characteristic                      */, 0x00020001u /* rd        */, 0x0071u, {{0x2A26u, NULL}}                           },
    { 0x0071u, 0x2A26u /* Firmware Revision String            */, 0x01020001u /* rd        */, 0x0071u, {{0x000Fu, (void *)&cyBle_attValuesLen[91]}} },
    { 0x0072u, 0x2803u /* Characteristic                      */, 0x00020001u /* rd        */, 0x0073u, {{0x2A28u, NULL}}                           },
    { 0x0073u, 0x2A28u /* Software Revision String            */, 0x01020001u /* rd        */, 0x0073u, {{0x000Fu, (void *)&cyBle_attValuesLen[92]}} },
    { 0x0074u, 0x2803u /* Characteristic                      */, 0x00020001u /* rd        */, 0x0075u, {{0x2A23u, NULL}}                           },
    { 0x0075u, 0x2A23u /* System ID                           */, 0x01020001u /* rd        */, 0x0075u, {{0x0008u, (void *)&cyBle_attValuesLen[93]}} },
    { 0x0076u, 0x2803u /* Characteristic                      */, 0x00020001u /* rd        */, 0x0077u, {{0x2A2Au, NULL}}                           },
    { 0x0077u, 0x2A2Au /* IEEE 11073-20601 Regulatory Certifi */, 0x01020001u /* rd        */, 0x0077u, {{0x0001u, (void *)&cyBle_attValuesLen[94]}} },
    { 0x0078u, 0x2803u /* Characteristic                      */, 0x00020001u /* rd        */, 0x0079u, {{0x2A50u, NULL}}                           },
    { 0x0079u, 0x2A50u /* PnP ID                              */, 0x01020001u /* rd        */, 0x0079u, {{0x0007u, (void *)&cyBle_attValuesLen[95]}} },
};


#endif /* (CYBLE_GATT_ROLE_SERVER) */

#if(CYBLE_GATT_ROLE_CLIENT)
    
CYBLE_CLIENT_STATE_T cyBle_clientState;
CYBLE_GATTC_T cyBle_gattc;
CYBLE_GATT_ATTR_HANDLE_RANGE_T cyBle_gattcDiscoveryRange;
    
#endif /* (CYBLE_GATT_ROLE_CLIENT) */


#if(CYBLE_GATT_ROLE_SERVER)

/****************************************************************************** 
* Function Name: CyBle_GattsReInitGattDb
***************************************************************************//**
* 
*  Reinitializes the GATT database.
* 
*  \return
*  CYBLE_API_RESULT_T: An API result states if the API succeeded or failed with
*  error codes:

*  Errors codes                          | Description
*  ------------                          | -----------
*  CYBLE_ERROR_OK						 | GATT database was reinitialized successfully.
*  CYBLE_ERROR_INVALID_STATE             | If the function is called in any state except CYBLE_STATE_DISCONNECTED.
*  CYBLE_ERROR_INVALID_PARAMETER         | If the Database has zero entries or is a NULL pointer.
* 
******************************************************************************/
CYBLE_API_RESULT_T CyBle_GattsReInitGattDb(void)
{
    CYBLE_API_RESULT_T apiResult;
    
    if(CyBle_GetState() == CYBLE_STATE_DISCONNECTED)
    {
        apiResult = CyBle_GattsDbRegister(cyBle_gattDB, CYBLE_GATT_DB_INDEX_COUNT, CYBLE_GATT_DB_MAX_VALUE_LEN);
    }
    else
    {
        apiResult = CYBLE_ERROR_INVALID_STATE;
    }
    
    return(apiResult);
}


/****************************************************************************** 
* Function Name: CyBle_GattsWriteEventHandler
***************************************************************************//**
* 
*  Handles the Write Request Event for GATT service.
* 
*  \param eventParam: The pointer to the data structure specified by the event.
* 
*  \return
*  CYBLE_GATT_ERR_CODE_T: An API result returns one of the following status 
*  values.

*  Errors codes                          | Description
*  --------------------                  | -----------
*  CYBLE_GATT_ERR_NONE                   | Write is successful.
* 
******************************************************************************/
CYBLE_GATT_ERR_CODE_T CyBle_GattsWriteEventHandler(CYBLE_GATTS_WRITE_REQ_PARAM_T *eventParam)
{
    CYBLE_GATT_ERR_CODE_T gattErr = CYBLE_GATT_ERR_NONE;
    
    /* Client Characteristic Configuration descriptor write request */
    if(eventParam->handleValPair.attrHandle == cyBle_gatts.cccdHandle)
    {
        /* Store value to database */
        gattErr = CyBle_GattsWriteAttributeValue(&eventParam->handleValPair, 0u, 
                        &eventParam->connHandle, CYBLE_GATT_DB_PEER_INITIATED);
        
        if(gattErr == CYBLE_GATT_ERR_NONE)
        {
            if(CYBLE_IS_INDICATION_ENABLED_IN_PTR(eventParam->handleValPair.value.val))
            {
                CyBle_ApplCallback((uint32)CYBLE_EVT_GATTS_INDICATION_ENABLED, eventParam);
            }
            else
            {
                CyBle_ApplCallback((uint32)CYBLE_EVT_GATTS_INDICATION_DISABLED, eventParam);
            }
        }
        cyBle_eventHandlerFlag &= (uint8)~CYBLE_CALLBACK;
    }
    return (gattErr);
}


#endif /* (CYBLE_GATT_ROLE_SERVER) */

#if(CYBLE_GATT_ROLE_CLIENT)


/****************************************************************************** 
* Function Name: CyBle_GattcStartDiscovery
***************************************************************************//**
* 
*  Starts the automatic server discovery process. Two events may be generated 
*  after calling this function - CYBLE_EVT_GATTC_DISCOVERY_COMPLETE or 
*  CYBLE_EVT_GATTC_ERROR_RSP. The CYBLE_EVT_GATTC_DISCOVERY_COMPLETE event is 
*  generated when the remote device was successfully discovered. The
*  CYBLE_EVT_GATTC_ERROR_RSP is generated if the device discovery is failed.
* 
*  \param connHandle: The handle which consists of the device ID and ATT connection ID.
* 
* \return
*	CYBLE_API_RESULT_T : Return value indicates if the function succeeded or
*                        failed. Following are the possible error codes.
*
*   <table>	
*   <tr>
*	  <th>Errors codes</th>
*	  <th>Description</th>
*	</tr>
*	<tr>
*	  <td>CYBLE_ERROR_OK</td>
*	  <td>On successful operation</td>
*	</tr>
*	<tr>
*	  <td>CYBLE_ERROR_INVALID_PARAMETER</td>
*	  <td>'connHandle' value does not represent any existing entry.</td>
*	</tr>
*	<tr>
*	  <td>CYBLE_ERROR_INVALID_OPERATION</td>
*	  <td>The operation is not permitted</td>
*	</tr>
*   <tr>
*	  <td>CYBLE_ERROR_MEMORY_ALLOCATION_FAILED</td>
*	  <td>Memory allocation failed</td>
*	</tr>
*   <tr>
*	  <td>CYBLE_ERROR_INVALID_STATE</td>
*	  <td>If the function is called in any state except connected or discovered</td>
*	</tr>
*   </table>
* 
******************************************************************************/
CYBLE_API_RESULT_T CyBle_GattcStartDiscovery(CYBLE_CONN_HANDLE_T connHandle)
{
    uint8 j;
    CYBLE_API_RESULT_T apiResult;
    
    if((CyBle_GetState() != CYBLE_STATE_CONNECTED) || 
       ((CyBle_GetClientState() != CYBLE_CLIENT_STATE_CONNECTED) && 
        (CyBle_GetClientState() != CYBLE_CLIENT_STATE_DISCOVERED))) 
    {
        apiResult = CYBLE_ERROR_INVALID_STATE;
    }
    else
    {
        /* Clean old discovery information */
        for(j = 0u; j < (uint8) CYBLE_SRVI_COUNT; j++)
        {
            (void)memset(&cyBle_serverInfo[j].range, 0, sizeof(cyBle_serverInfo[0].range));
        }

        cyBle_connHandle = connHandle;
        cyBle_gattcDiscoveryRange.startHandle = CYBLE_GATT_ATTR_HANDLE_START_RANGE;
        cyBle_gattcDiscoveryRange.endHandle = CYBLE_GATT_ATTR_HANDLE_END_RANGE;
        
        CyBle_ServiceInit();
        
        apiResult = CyBle_GattcDiscoverAllPrimaryServices(connHandle);

        if(apiResult == CYBLE_ERROR_OK)
        {
            CyBle_SetClientState(CYBLE_CLIENT_STATE_SRVC_DISCOVERING);
            cyBle_eventHandlerFlag |= CYBLE_AUTO_DISCOVERY;
        }
    }
    
    return (apiResult);
}


/****************************************************************************** 
* Function Name: CyBle_GattcStartPartialDiscovery
***************************************************************************//**
* 
*  Starts the automatic server discovery process as per the range provided
*  on a GATT Server to which it is connected. This API could be used for 
*  partial server discovery after indication received to the Service Changed
*  Characteristic Value. Two events may be generated 
*  after calling this function - CYBLE_EVT_GATTC_DISCOVERY_COMPLETE or 
*  CYBLE_EVT_GATTC_ERROR_RSP. The CYBLE_EVT_GATTC_DISCOVERY_COMPLETE event is 
*  generated when the remote device was successfully discovered. The
*  CYBLE_EVT_GATTC_ERROR_RSP is generated if the device discovery is failed.
* 
*  \param connHandle: The handle which consists of the device ID and ATT connection ID.
*  \param startHandle: Start of affected attribute handle range.
*  \param endHandle: End of affected attribute handle range.
* 
*  \return
*	CYBLE_API_RESULT_T : Return value indicates if the function succeeded or
*                        failed. Following are the possible error codes.
*
*   <table>	
*   <tr>
*	  <th>Errors codes</th>
*	  <th>Description</th>
*	</tr>
*	<tr>
*	  <td>CYBLE_ERROR_OK</td>
*	  <td>On successful operation</td>
*	</tr>
*	<tr>
*	  <td>CYBLE_ERROR_INVALID_PARAMETER</td>
*	  <td>'connHandle' value does not represent any existing entry.</td>
*	</tr>
*	<tr>
*	  <td>CYBLE_ERROR_INVALID_OPERATION</td>
*	  <td>The operation is not permitted</td>
*	</tr>
*   <tr>
*	  <td>CYBLE_ERROR_MEMORY_ALLOCATION_FAILED</td>
*	  <td>Memory allocation failed</td>
*	</tr>
*   <tr>
*	  <td>CYBLE_ERROR_INVALID_STATE</td>
*	  <td>If the function is called in any state except connected or discovered</td>
*	</tr>
*   </table>
* 
******************************************************************************/
CYBLE_API_RESULT_T CyBle_GattcStartPartialDiscovery(CYBLE_CONN_HANDLE_T connHandle,
                        CYBLE_GATT_DB_ATTR_HANDLE_T startHandle, CYBLE_GATT_DB_ATTR_HANDLE_T endHandle)
{
    uint8 j;
    CYBLE_API_RESULT_T apiResult;
    
    if((CyBle_GetState() != CYBLE_STATE_CONNECTED) || 
       ((CyBle_GetClientState() != CYBLE_CLIENT_STATE_CONNECTED) && 
        (CyBle_GetClientState() != CYBLE_CLIENT_STATE_DISCOVERED))) 
    {
        apiResult = CYBLE_ERROR_INVALID_STATE;
    }
    else
    {
        /* Clean old discovery information of affected attribute range */
        for(j = 0u; j < (uint8) CYBLE_SRVI_COUNT; j++)
        {
            if((cyBle_serverInfo[j].range.startHandle >= startHandle) &&
               (cyBle_serverInfo[j].range.startHandle <= endHandle))
            {
                (void)memset(&cyBle_serverInfo[j].range, 0, sizeof(cyBle_serverInfo[0].range));
            }
        }

        cyBle_connHandle = connHandle;
        cyBle_gattcDiscoveryRange.startHandle = startHandle;
        cyBle_gattcDiscoveryRange.endHandle = endHandle;

        CyBle_ServiceInit();

        apiResult = CyBle_GattcDiscoverPrimaryServices(connHandle, &cyBle_gattcDiscoveryRange);

        if(apiResult == CYBLE_ERROR_OK)
        {
            CyBle_SetClientState(CYBLE_CLIENT_STATE_SRVC_DISCOVERING);
            cyBle_eventHandlerFlag |= CYBLE_AUTO_DISCOVERY;
        }
    }
    
    return (apiResult);
}


/******************************************************************************
* Function Name: CyBle_GattcDiscoverCharacteristicsEventHandler
***************************************************************************//**
* 
*  This function is called on receiving a "CYBLE_EVT_GATTC_READ_BY_TYPE_RSP"
*  event. Based on the service UUID, an appropriate data structure is populated
*  using the data received as part of the callback.
* 
*  \param *discCharInfo: The pointer to a characteristic information structure.
* 
* \return
*  None
* 
******************************************************************************/
void CyBle_GattcDiscoverCharacteristicsEventHandler(CYBLE_DISC_CHAR_INFO_T *discCharInfo)
{
    if(discCharInfo->uuid.uuid16 == CYBLE_UUID_CHAR_SERVICE_CHANGED)
    {
        CyBle_CheckStoreCharHandle(cyBle_gattc.serviceChanged);
    }
}


/******************************************************************************
* Function Name: CyBle_GattcDiscoverCharDescriptorsEventHandler
***************************************************************************//**
* 
*  This function is called on receiving a "CYBLE_EVT_GATTC_FIND_INFO_RSP" event.
*  Based on the descriptor UUID, an appropriate data structure is populated 
*  using the data received as part of the callback.
* 
*  \param *discDescrInfo: The pointer to a descriptor information structure.
*  \param discoveryService: The index of the service instance
* 
* \return
*  None
* 
******************************************************************************/
void CyBle_GattcDiscoverCharDescriptorsEventHandler(CYBLE_DISC_DESCR_INFO_T *discDescrInfo)
{
    if(discDescrInfo->uuid.uuid16 == CYBLE_UUID_CHAR_CLIENT_CONFIG)
    {
        CyBle_CheckStoreCharDescrHandle(cyBle_gattc.cccdHandle);
    }
}


/******************************************************************************
* Function Name: CyBle_GattcIndicationEventHandler
***************************************************************************//**
* 
*  Handles the Indication Event.
* 
*  \param *eventParam: The pointer to the data structure specified by the event.
* 
* \return
*  None.
* 
******************************************************************************/
void CyBle_GattcIndicationEventHandler(CYBLE_GATTC_HANDLE_VALUE_IND_PARAM_T *eventParam)
{
    if(cyBle_gattc.serviceChanged.valueHandle == eventParam->handleValPair.attrHandle)
    {
        CyBle_ApplCallback((uint32)CYBLE_EVT_GATTC_INDICATION, eventParam);
        cyBle_eventHandlerFlag &= (uint8)~CYBLE_CALLBACK;
    }
}


#endif /* (CYBLE_GATT_ROLE_CLIENT) */


/* [] END OF FILE */
