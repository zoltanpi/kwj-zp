/*******************************************************************************
* File Name: CK2KHz.h
* Version 2.20
*
*  Description:
*   Provides the function and constant definitions for the clock component.
*
*  Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_CLOCK_CK2KHz_H)
#define CY_CLOCK_CK2KHz_H

#include <cytypes.h>
#include <cyfitter.h>


/***************************************
*        Function Prototypes
***************************************/
#if defined CYREG_PERI_DIV_CMD

void CK2KHz_StartEx(uint32 alignClkDiv);
#define CK2KHz_Start() \
    CK2KHz_StartEx(CK2KHz__PA_DIV_ID)

#else

void CK2KHz_Start(void);

#endif/* CYREG_PERI_DIV_CMD */

void CK2KHz_Stop(void);

void CK2KHz_SetFractionalDividerRegister(uint16 clkDivider, uint8 clkFractional);

uint16 CK2KHz_GetDividerRegister(void);
uint8  CK2KHz_GetFractionalDividerRegister(void);

#define CK2KHz_Enable()                         CK2KHz_Start()
#define CK2KHz_Disable()                        CK2KHz_Stop()
#define CK2KHz_SetDividerRegister(clkDivider, reset)  \
    CK2KHz_SetFractionalDividerRegister((clkDivider), 0u)
#define CK2KHz_SetDivider(clkDivider)           CK2KHz_SetDividerRegister((clkDivider), 1u)
#define CK2KHz_SetDividerValue(clkDivider)      CK2KHz_SetDividerRegister((clkDivider) - 1u, 1u)


/***************************************
*             Registers
***************************************/
#if defined CYREG_PERI_DIV_CMD

#define CK2KHz_DIV_ID     CK2KHz__DIV_ID

#define CK2KHz_CMD_REG    (*(reg32 *)CYREG_PERI_DIV_CMD)
#define CK2KHz_CTRL_REG   (*(reg32 *)CK2KHz__CTRL_REGISTER)
#define CK2KHz_DIV_REG    (*(reg32 *)CK2KHz__DIV_REGISTER)

#define CK2KHz_CMD_DIV_SHIFT          (0u)
#define CK2KHz_CMD_PA_DIV_SHIFT       (8u)
#define CK2KHz_CMD_DISABLE_SHIFT      (30u)
#define CK2KHz_CMD_ENABLE_SHIFT       (31u)

#define CK2KHz_CMD_DISABLE_MASK       ((uint32)((uint32)1u << CK2KHz_CMD_DISABLE_SHIFT))
#define CK2KHz_CMD_ENABLE_MASK        ((uint32)((uint32)1u << CK2KHz_CMD_ENABLE_SHIFT))

#define CK2KHz_DIV_FRAC_MASK  (0x000000F8u)
#define CK2KHz_DIV_FRAC_SHIFT (3u)
#define CK2KHz_DIV_INT_MASK   (0xFFFFFF00u)
#define CK2KHz_DIV_INT_SHIFT  (8u)

#else 

#define CK2KHz_DIV_REG        (*(reg32 *)CK2KHz__REGISTER)
#define CK2KHz_ENABLE_REG     CK2KHz_DIV_REG
#define CK2KHz_DIV_FRAC_MASK  CK2KHz__FRAC_MASK
#define CK2KHz_DIV_FRAC_SHIFT (16u)
#define CK2KHz_DIV_INT_MASK   CK2KHz__DIVIDER_MASK
#define CK2KHz_DIV_INT_SHIFT  (0u)

#endif/* CYREG_PERI_DIV_CMD */

#endif /* !defined(CY_CLOCK_CK2KHz_H) */

/* [] END OF FILE */
