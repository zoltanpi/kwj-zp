/* ========================================
 * File Name: KWJ_Sensor.c
 *
 * Version: 1.0
 *
 * Description:
 * This file implements the Custom BLE service for Gas Sensors
 *
 * Hardware Dependency:
 * CYBLE-014008
 *
 * Copyright ODI, 2016
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF ODI.
 * RCRegna - Author
 * ========================================
*/
#include "project.h"
#include "main.h"
#include "KWJ_BLE_Process.h"

/******************************************************************************
* Function Name: Init_Gas_DB
*******************************************************************************
* Summary:
*  Initializes the SAV_DB Environmental entries with factory defaults
*
* Parameters:
*  None
*
* Return:
*  None
*
******************************************************************************/
void Init_Gas_DB(void)
{
    
    rstFlag(KWJ_CO_NOTIFICATION);
    rstFlag(KWJ_O3_NOTIFICATION);
    
    SAV_DB.CO_MEAS =  0u;                        // CO Meas
        // Carbon Monoxide Factory Default
    if(false == (SAV_DB.CAL_DONE & CO_OS_CAL))
        SAV_DB.CO_OS   =  0.0;                   // ADC Offset
    if(false == (SAV_DB.CAL_DONE & CO_GAIN_CAL))
        SAV_DB.CO_GAIN =  0.5865;                // ADC Gain (nom 100ppm)
    if(false == (SAV_DB.CAL_DONE & CO_TZRO_TC_CAL))
        SAV_DB.CO_TZRO_TC   =  25;               // ADC Temp Comp T Zero
    if(false == (SAV_DB.CAL_DONE & CO_SNS_FAC_CAL))
        SAV_DB.CO_SNS_FAC =  1.0;               // CO Sensitivity Factor
    if(false == (SAV_DB.CAL_DONE & CO_MOD_CAL))
        CO_MOD = 10u;                            // CO >= 10 ppm
    if(false == (SAV_DB.CAL_DONE & CO_UNH_CAL))
        CO_UNH = 30u;                            // CO >= 30 ppm
    if(false == (SAV_DB.CAL_DONE & CO_HAZ_CAL))
        CO_HAZ = 70u;                            // CO >= 70 ppm

         
    SAV_DB.O3_MEAS =  0u;                        // O3 Meas
        // Ozone Factory Defaults
    if(false == (SAV_DB.CAL_DONE & O3_OS_CAL))
        SAV_DB.O3_OS   =  47.0;                  // ADC Offset
    if(false == (SAV_DB.CAL_DONE & O3_GAIN_CAL))
        SAV_DB.O3_GAIN =  0.5;                   // ADC Gain (nom 20ppm)
    if(false == (SAV_DB.CAL_DONE & O3_TZRO_TC_CAL))
        SAV_DB.O3_TZRO_TC   =  25;               // ADC Temp Comp T Zero
    if(false == (SAV_DB.CAL_DONE & O3_SNS_FAC_CAL))
        SAV_DB.O3_SNS_FAC =  1.0;               // O3 Sensitivity Factor
    if(false == (SAV_DB.CAL_DONE & O3_MOD_CAL))
        O3_MOD =  30u;                           // O3 >= 30 ppb
    if(false == (SAV_DB.CAL_DONE & O3_UNH_CAL))
        O3_UNH = 100u;                           // O3 >= 100 ppb
    if(false == (SAV_DB.CAL_DONE & O3_HAZ_CAL))
        O3_HAZ = 300u;                           // O3 >= 300 ppb

}

/***************************************************************************
* Function Name: Make_O3_Measurement
****************************************************************************
* Summary:
*  A 12-Bit Measurement of the O3 Sensor Takes place
*  Offset and Gain Factors are applied to determine the reading
*  The reading is returned to calling function
*
* Parameters:
*  None
*
* Return:
*  o3_reading
*
**************************************************************************/
int16 Make_O3_Measurement()
{
    int16 del_t = NOW_DB.T_MEAS - SAV_DB.O3_TZRO_TC;
    float zcf = ZCF_O3_LO;
    float scf = SCF_O3_LO;

    if(NOW_DB.T_MEAS > 25)
    {
        zcf = ZCF_O3_HI;
        scf = ZCF_O3_HI;
    }
    NOW_DB.O3_RAW = ADC_SAR_12Bit_GetResult16(1u);
    NOW_DB.O3_MEAS = NOW_DB.O3_RAW + SAV_DB.O3_OS;
    NOW_DB.O3_MEAS *= SAV_DB.O3_GAIN;
    
    // Temperature Compensation
    NOW_DB.O3_MEAS -= (int16)(scf*del_t/100);
    NOW_DB.O3_MEAS -= (int16)(zcf*del_t/100);
    
#ifndef KWJ_TEST
    if(NOW_DB.O3_MEAS < 0.0)
    {
        NOW_DB.O3_MEAS = 0.0;
    }
#endif    
    
    if(false == chkFlag( STAND_ALONE_MODE))
    {
        if((CYBLE_ERROR_OK == GattSetCharVal(O3_MEASD, 
            0x02u, VarToCharArray((int16*)&NOW_DB.O3_MEAS,SIZE_2_BYTES))) &&
        (CYBLE_ERROR_OK == GattSetCharVal(O3_RAW_INDEX, 
            0x02u, VarToCharArray((int16*)&NOW_DB.O3_RAW,SIZE_2_BYTES))) &&
        (SAV_DB.O3_RAW != NOW_DB.O3_RAW))
        {
            setFlag( KWJ_O3_NOTIFICATION);
        }
    }
    SAV_DB.O3_RAW = NOW_DB.O3_RAW;
    return NOW_DB.O3_MEAS;
}
/******************************************************************************
* Function Name: Make_CO_Measurement
*******************************************************************************
* Summary:
*  A 12-Bit Measurement of the CO Sensor Takes place
*  Offset and Gain Factors are applied to determine the reading
*  The reading is logged to calling function
*
* Parameters:
*  None
*
* Return:
*  co_reading
*
******************************************************************************/
int16 Make_CO_Measurement()
{
    int16 del_t = NOW_DB.T_MEAS - SAV_DB.CO_TZRO_TC;
    float zcf = ZCF_CO_LO;
    float scf = SCF_CO_LO;

    if(NOW_DB.T_MEAS > 25)
    {
        zcf = ZCF_CO_HI;
        scf = ZCF_CO_HI;
    }
    ADC_SAR_12Bit_StartConvert();
    ADC_SAR_12Bit_IsEndConversion(ADC_SAR_12Bit_WAIT_FOR_RESULT);
    ADC_SAR_12Bit_StopConvert();
    
    NOW_DB.CO_RAW = ADC_SAR_12Bit_GetResult16(0u);
    NOW_DB.CO_MEAS = NOW_DB.CO_RAW + SAV_DB.CO_OS;
    NOW_DB.CO_MEAS *= SAV_DB.CO_GAIN;
    
    // Temperature Compensation
    NOW_DB.CO_MEAS -= (int16)(scf*del_t/100);
    NOW_DB.CO_MEAS -= (int16)(zcf*del_t/100);
    
#ifndef KWJ_TEST
    if(NOW_DB.CO_MEAS < 0.0)
    {
        NOW_DB.CO_MEAS = 0.0;
    }    
#endif

    if(false == chkFlag( STAND_ALONE_MODE))
    {
        if((CYBLE_ERROR_OK == GattSetCharVal(CO_MEASD, 
            0x02u, VarToCharArray((int16*)&NOW_DB.CO_MEAS,SIZE_2_BYTES))) &&
        (CYBLE_ERROR_OK == GattSetCharVal(CO_RAW_INDEX, 
            0x02u, VarToCharArray((int16*)&NOW_DB.CO_RAW,SIZE_2_BYTES))) &&
        (SAV_DB.CO_RAW != NOW_DB.CO_RAW))
        {
            setFlag( KWJ_CO_NOTIFICATION);
        }
    }
    SAV_DB.CO_RAW = NOW_DB.CO_RAW;
    return NOW_DB.CO_MEAS;
}

/* [] END OF FILE */
