/* ========================================
 * File Name: KWJ_Custom.c
 *
 * Version: 1.0
 *
 * Description:
 * This file implements the BLE service for KWJ Custom Commands
 *
 * Hardware Dependency:
 * CYBLE-014008
 *
 * Copyright ODI, 2016
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF ODI.
 * RCRegna - Author
 * ========================================
*/
#include "project.h"
#include "main.h"
#include "KWJ_BLE_Process.h"

/* Convert uint8[] to uint16 *
******************************/
uint16 rtnLong(uint8 *gattPtr)
{
    uint8 i;
    union lng
    {
        uint8 tmpBfr[SIZE_2_BYTES];
        uint16 val;
    }lng;
    for(i = 0; i < SIZE_2_BYTES; i++) { lng.tmpBfr[i] = gattPtr[i]; }
    uint16 rtnVal = lng.val;
    
    return rtnVal;
}
/* Convert uint8[] to float  *
******************************/
float rtnFloat(uint8 *gattPtr)
{
    uint8 i;
    union flt
    {
        uint8 tmpBfr[SIZE_4_BYTES];
        float val;
    }flt;
    for(i = 0; i < SIZE_4_BYTES; i++) { flt.tmpBfr[i] = gattPtr[i]; }
    float rtnVal = flt.val;
    
    return rtnVal;
}
/* Convert uint8[] to 32 bit int  *
******************************/
int32 rtnInt32(uint8 *gattPtr)
{
    uint8 i;
    union in32
    {
        uint8 tmpBfr[SIZE_4_BYTES];
        int32 val;
    }in32;
    for(i = 0; i < SIZE_4_BYTES; i++) { in32.tmpBfr[i] = gattPtr[i]; }
    float rtnVal = in32.val;
    
    return rtnVal;
}

/*****************************************************************************
* Function Name: KwjEventHandler
******************************************************************************
* Summary:
* Event handler for the KWJ Custom Service specific events.
*
* Params:
*  event - KWJ Service Characteristic
*  eventParam - KWJ Service Characteristic Value
*
* Return:
*  None
*
*****************************************************************************/
void KwjEventHandler(CYBLE_GATTS_WRITE_REQ_PARAM_T *wrReqParam)
{
 
    uint32 calFlags = SAV_DB.CAL_DONE;
    CYBLE_API_RESULT_T apiResult;
    uint16  index = wrReqParam->handleValPair.attrHandle;
    uint8 * value = wrReqParam->handleValPair.value.val;
    uint8    size = wrReqParam->handleValPair.value.len;
    
    apiResult = CyBle_GattsWriteRsp(cyBle_connHandle); 
    if(apiResult == CYBLE_ERROR_OK)
    {

    /* Event handler switch statement for the KWJ service specific events. */
        switch(index)
    	{
            case LOG_TYPE:    /*< Handle of the Logging descriptor */
              LogHandler(value);
              calFlags |= LOG_TYPE_CAL;
            break;
            case LOG_CCCD:    /*< Handle of the Logging descriptor */
              LogUploader(value);
            break;

            case CO_GAIN_INDEX:    /*< Handle of the CO Gain descriptor */
              SAV_DB.CO_GAIN = rtnFloat(value);
              calFlags |= CO_GAIN_CAL;
            break;
            case CO_OS_INDEX:      /*< Handle of the CO OS descriptor */ 
              SAV_DB.CO_OS = rtnFloat(value);
              calFlags |= CO_OS_CAL;
    		    break;
            case CO_TZRO_TC_INDEX:    /*< Handle of the CO Gain TC descriptor */
              SAV_DB.CO_TZRO_TC = rtnInt32(value);
              calFlags |= CO_TZRO_TC_CAL;
            break;
            case CO_SNSFAC_INDEX:      /*< Handle of the CO OS TC descriptor */ 
              SAV_DB.CO_SNS_FAC = rtnFloat(value);
              calFlags |= CO_SNS_FAC_CAL;
    		    break;
            case CO_MOD_INDEX:     /*< Handle of the CO MOD descriptor */ 
              calFlags |= CO_MOD_CAL;
              CO_MOD = rtnLong(value);
    		    break;
            case CO_UNH_INDEX:     /*< Handle of the CO UNH descriptor */ 
              calFlags |= CO_UNH_CAL;
              CO_UNH = rtnLong(value);
    		    break;
            case CO_HAZ_INDEX:     /*< Handle of the CO HAZ descriptor */ 
              calFlags |= CO_HAZ_CAL;
              CO_HAZ = rtnLong(value);
    		    break;

            case O3_GAIN_INDEX:    /*< Handle of the O3 Gain descriptor */ 
              SAV_DB.O3_GAIN = rtnFloat(value);
              calFlags |= O3_GAIN_CAL;
    		    break;
            case O3_OS_INDEX:      /*< Handle of the O3 OS descriptor */ 
              SAV_DB.O3_OS = rtnFloat(value);
              calFlags |= O3_OS_CAL;
    		    break;
            case O3_TZRO_TC_INDEX:    /*< Handle of the O3 Gain TC descriptor */ 
              SAV_DB.O3_TZRO_TC = rtnInt32(value);
              calFlags |= O3_TZRO_TC_CAL;
    		    break;
            case O3_SNSFAC_INDEX:      /*< Handle of the O3 OS TC descriptor */ 
              SAV_DB.O3_SNS_FAC = rtnFloat(value);
              calFlags |= O3_SNS_FAC_CAL;
    		    break;
            case O3_MOD_INDEX:     /*< Handle of the O3 MOD descriptor */ 
              O3_MOD = rtnLong(value);
              calFlags |= O3_MOD_CAL;
    		    break;
            case O3_UNH_INDEX:     /*< Handle of the O3 UNH descriptor */ 
              O3_UNH = rtnLong(value);
              calFlags |= O3_UNH_CAL;
    		    break;
            case O3_HAZ_INDEX:     /*< Handle of the O3 HAZ descriptor */ 
              O3_HAZ = rtnLong(value);
              calFlags |= O3_HAZ_CAL;
    		    break;

            case T_GAIN_INDEX:     /*< Handle of the T Gain descriptor */ 
              SAV_DB.T_GAIN = rtnFloat(value);
              calFlags |= T_GAIN_CAL;
    		    break;
            case T_OS_INDEX:       /*< Handle of the T OS descriptor */ 
              SAV_DB.T_OS = rtnFloat(value);
              calFlags |= T_OS_CAL;
    		    break;

            case RH_GAIN_INDEX:    /*< Handle of the RH Gain descriptor */ 
              SAV_DB.RH_GAIN = rtnFloat(value);
              calFlags |= RH_GAIN_CAL;
    		    break;
            case RH_OS_INDEX:      /*< Handle of the RH OS descriptor */ 
              SAV_DB.RH_OS = rtnFloat(value);
              calFlags |= RH_OS_CAL;
    		    break;

            case P_GAIN_INDEX:     /*< Handle of the P Gain descriptor */ 
              SAV_DB.P_GAIN = rtnFloat(value);
              calFlags |= P_GAIN_CAL;
    		    break;
            case P_OS_INDEX:       /*< Handle of the P OS descriptor */ 
              SAV_DB.P_OS = rtnFloat(value);
              calFlags |= P_OS_CAL;
    		    break;

            case BAT_GAIN_INDEX:   /*< Handle of the BAT Gain descriptor */ 
              SAV_DB.BAT_GAIN = rtnFloat(value);
              calFlags |= BAT_GAIN_CAL;
    		    break;
            case BAT_OS_INDEX:     /*< Handle of the BAT OS descriptor */ 
              SAV_DB.BAT_OS = rtnFloat(value);
              calFlags |= BAT_OS_CAL;
    		    break;
            case BAT_LO_INDEX:     /*< Handle of the BAT LO descriptor */ 
              BAT_LO = (uint8)*value;
              calFlags |= BAT_LO_CAL;
    		    break;
            case BAT_CRIT_INDEX:   /*< Handle of the BAT CRIT descriptor */
              BAT_CRIT = (uint8)*value;
              calFlags |= BAT_CRIT_CAL;
    		    break;
            case CURRENT_TIME: /*< Handle of Date Time Service */
              Sys_RTC_Update(value);
                break;
    		
    		default:
        		break;
        
        }
        GattSetCharVal(index, size, value);
        /**** Save CAL_FLAGS **********/
        SAV_DB.CAL_DONE = calFlags;
        WriteUserSFlashRow(DB_SAVE,StructToFlash((PARAM_DB_T *)&SAV_DB, sizeof(SAV_DB)));
        
        if(index == CURRENT_TIME)
        {
            // set BLE to Slow rate
            setFlag(CHANGE_BLE);
            setFlag(LOW_RATE_BLE);
        }
	}
    
}
/*****************************************************************************
* Function Name: SendNotificationOverBLE
******************************************************************************
* Summary:
* Creates and sends the specific characteristic notification 
* packet.
*
* Parameters:
* notificationFlag
*
* Return:
* None
*
* Side Effects:
* Resets Notification flag when completed
*
*****************************************************************************/
void SendKwjOverBLE(void)
{
    CYBLE_GATTS_HANDLE_VALUE_NTF_T ntfReqParam;
    
    if(chkFlag( KWJ_CO_NOTIFICATION))
    {
        if(YES == CYBLE_IS_NOTIFICATION_ENABLED(CO_CCCD))
        {
            /* Fill all fields of write request structure ... */
            ntfReqParam.attrHandle = (CYBLE_GATT_DB_ATTR_HANDLE_T) CO_MEASD;
            ntfReqParam.value.val=VarToCharArray(&NOW_DB.CO_MEAS, SIZE_2_BYTES);
            ntfReqParam.value.len = SIZE_2_BYTES;

            /* Send notification to client using previously filled structure */
            if(CYBLE_ERROR_OK ==  CyBle_GattsNotification(cyBle_connHandle, &ntfReqParam))
            {
                rstFlag(KWJ_CO_NOTIFICATION);
            }
#ifdef KWJ_TEST
            if(YES == CYBLE_IS_NOTIFICATION_ENABLED(CAL_CCCD))
            {
                ntfReqParam.attrHandle = (CYBLE_GATT_DB_ATTR_HANDLE_T) CO_RAW_INDEX;
                ntfReqParam.value.val=VarToCharArray(&NOW_DB.CO_RAW, SIZE_2_BYTES);
                ntfReqParam.value.len = SIZE_2_BYTES;
                CyBle_GattsNotification(cyBle_connHandle, &ntfReqParam);
            }
#endif        
            
        }
    }
    if(chkFlag( KWJ_O3_NOTIFICATION))
    {
        if(YES == CYBLE_IS_NOTIFICATION_ENABLED(O3_CCCD))
        {
            /* Fill all fields of write request structure ... */
            ntfReqParam.attrHandle = O3_MEASD;
            ntfReqParam.value.val=VarToCharArray(&NOW_DB.O3_MEAS, SIZE_2_BYTES);
            ntfReqParam.value.len = SIZE_2_BYTES;

            /* Send notification to client using previously filled structure */
            if(CYBLE_ERROR_OK ==  CyBle_GattsNotification(cyBle_connHandle, &ntfReqParam))
            {
                rstFlag(KWJ_O3_NOTIFICATION);
            }
#ifdef KWJ_TEST
            if(YES == CYBLE_IS_NOTIFICATION_ENABLED(CAL_CCCD))
            {
                ntfReqParam.attrHandle = (CYBLE_GATT_DB_ATTR_HANDLE_T) O3_RAW_INDEX;
                ntfReqParam.value.val=VarToCharArray(&NOW_DB.O3_RAW, SIZE_2_BYTES);
                ntfReqParam.value.len = SIZE_2_BYTES;
                CyBle_GattsNotification(cyBle_connHandle, &ntfReqParam);
            }
#endif        
        }
    }
}

/* [] END OF FILE */
