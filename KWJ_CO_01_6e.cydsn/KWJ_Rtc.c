/* ========================================
 * File Name: KWJ_Rtc.c
 *
 * Version: 1.0
 *
 * Description:
 * This file implements the BLE service for Real Time Clock
 *
 * Hardware Dependency:
 * CYBLE-014008
 *
 * Copyright ODI, 2016
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF ODI.
 * RCRegna - Author
 * ========================================
*/
#include "project.h"
#include "main.h"
#include "KWJ_BLE_Process.h"

/******************************************************************************
* Function Name: RTC_Update
*******************************************************************************
* Summary:
*  The Real Time Clock is updated from the Host
*
* Parameters:
*  none
*
* Return:
*  none
*
******************************************************************************/
void   Sys_RTC_Update(uint8 * value)
{
    uint32 inputTime;
    uint32 inputDate;
    
    RTC_Start();
    // Date: MMDDYYYY
    inputDate = value[2]; //month
    inputDate <<= 8;
    inputDate |= value[3]; //day
    inputDate <<= 8;
    inputDate |= value[1]; //year hi
    inputDate <<= 8;
    inputDate |= value[0]; //year lo
    
    // Time: HH:MM:SS
    inputTime = value[4]; //hour
    inputTime <<= 8;
    inputTime |= value[5]; //minute
    inputTime <<= 8;
    
    RTC_SetDateAndTime(inputTime, inputDate);
}

/* [] END OF FILE */
