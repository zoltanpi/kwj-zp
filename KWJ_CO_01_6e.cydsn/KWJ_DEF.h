/* ========================================
 *
 * Copyright ODI, 2016
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF ODI.
 * RCRegna - Author
 * ========================================
*/
#ifndef KWJ_DEF_H
#define KWJ_DEF_H

#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "project.h"
    
/*************************************
* Sensor Temperature Coefficients
*************************************/
// CO Sensor T <= 25^C
#define ZCF_CO_LO           (2.375)
#define SCF_CO_LO           (0.4)
// CO Sensor T > 25^C
#define ZCF_CO_HI           (0.0)
#define SCF_CO_HI           (0.6)
// O3 Sensor T <= 25^C
#define ZCF_O3_LO           (0.0)
#define SCF_O3_LO           (0.0)
// O3 Sensor T > 25^C
#define ZCF_O3_HI           (0.0)
#define SCF_O3_HI           (0.0)

/*************************************
* System Tick Timer Params for Failsafe
*************************************/
#define SYSTICK_VECTOR_NUMBER           (15)
#define SYSTK_RELOAD                    (0x36ee80) // 3MHz SysClk 1.2Sec
    
/*************************************
* HF Clock Control Control
*************************************/
    /* System HF_CLOCK Constants */
#define CLOCK_CLR_MASK                  (~0x00380fffu)
#define HFCLK_SEL_ECO                   (0x00000492u)
#define HFCLK_ENA_ECO                   (1u << 15)
#define HFCLK_ENA_IMO                   (1u << 31)

#define IMO_DIS (CY_SET_REG32(CYREG_CLK_IMO_CONFIG, CY_GET_REG32(CYREG_CLK_IMO_CONFIG) & ~HFCLK_ENA_IMO))
#define IMO_ENA (CY_SET_REG32(CYREG_CLK_IMO_CONFIG, CY_GET_REG32(CYREG_CLK_IMO_CONFIG) | HFCLK_ENA_IMO))
#define IMO_SEL (CY_SET_REG32(CYREG_CLK_SELECT, CY_GET_REG32(CYREG_CLK_SELECT) & CLOCK_CLR_MASK))
#define ECO_SEL (CY_SET_REG32(CYREG_CLK_SELECT, CY_GET_REG32(CYREG_CLK_SELECT) | HFCLK_SEL_ECO))
#define ECO_ENA (CY_SET_REG32(CYREG_BLE_BLERD_DBUS, CY_GET_REG32(CYREG_BLE_BLERD_DBUS) | HFCLK_ENA_ECO))
#define ECO_DIS (CY_SET_REG32(CYREG_BLE_BLERD_DBUS, CY_GET_REG32(CYREG_BLE_BLERD_DBUS) & ~HFCLK_ENA_ECO))
    
/*************************************
* Pin Control
*************************************/
#define P0_CLR (CY_SET_REG32(0x40040008,CY_GET_REG32(0x40040008) & 0xfffff000u))
#define P1_CLR (CY_SET_REG32(0x40040108,CY_GET_REG32(0x40040108) & 0xff000000u))
#define P2_CLR (CY_SET_REG32(0x40040208,CY_GET_REG32(0x40040208) & 0xff000000u))
#define P3_CLR (CY_SET_REG32(0x40040308,CY_GET_REG32(0x40040308) & 0xff000000u))
    
#define P0_ENA (CY_SET_REG32(0x40040008,CY_GET_REG32(0x40040008) | 0x00da4010u))
#define P1_ENA (CY_SET_REG32(0x40040108,CY_GET_REG32(0x40040108) | 0x000245b3u))
#define P2_ENA (CY_SET_REG32(0x40040208,CY_GET_REG32(0x40040208) | 0x00180000u))
#define P3_ENA (CY_SET_REG32(0x40040308,CY_GET_REG32(0x40040308) | 0x00100005u))
    
#define P0_HIB (CY_SET_REG32(0x40040008,CY_GET_REG32(0x40040008) & 0xffd80010u))
#define P1_HIB (CY_SET_REG32(0x40040108,CY_GET_REG32(0x40040108) & 0xff000403u))
#define P2_HIB (CY_SET_REG32(0x40040208,CY_GET_REG32(0x40040208) & 0xff180000u))
#define P3_HIB (CY_SET_REG32(0x40040308,CY_GET_REG32(0x40040308) & 0xff000000u))
    
/*************************************
* Battery
*************************************/
#define BAT_FAULT_B  (FAULT_B_Read())
#define BAT_STATUS_B (STATUS_B_Read())
#define BAT_SMPL     (SMPLBAT_Write(1u))
#define BAT_NO_SMPL  (SMPLBAT_Write(0u))

/*************************************
* Regulator Control
*************************************/
#define REG_3V3_ON  (ENA_REG_Write(1u))
#define REG_3V3_OFF (ENA_REG_Write(0u))

/*************************************
* WDT System Timing Settings
*************************************/
/* Total warmup time (mins) = NUM_MINS_STRT * (INIT_WRM_UP - WARM_UP) */
#define NUM_MINS_STRT  (6u)      // Mins per warm_up duration at startup
#define INIT_WARM_UP   (10u)      // Initial # cycle(s) at startup
#define WARM_UP        (2u)      // cycle(s) prior to sample for amps turned on
#define WDT_1_SEC      (1u)      // 1  Sec WDT Heartbeat
#define LOG_1_MIN      (60u)     // 1  Min Logging Interval
#define SYS_TIME_FAST  (15u)     // 15 Sec Cycle
#define SYS_TIME_NORM  (60u)     //  1 Min Cycle
#define SYS_TIME_SLOW  (16u*60u) // 16 Min Cycle
#define WDT_INTERRUPT_NUM            (8)
#define WATCHDOG_DEFAULT_PERIOD_SEC  (WDT_1_SEC)
#define WATCHDOG_TICKS_PER_SEC       (32768)
//#define CLK_WCO_CONFIG_WCO_ENABLE    (1u << 30)
#define WDT_CONFIG_WDT_MODE0_RST     (1u << 1)
#define WDT_CONFIG_WDT_MODE0_INT     (1u << 0)    
#define WDT_CONFIG_WDT_MODE0_NO_INT  (0xFFFC)    
//#define WDT_CONTROL_WDT_ENABLE0      (1u << 0)
#define WDT_CONTROL_WDT_ENABLE0      (1u << 0 | 1u << 1) //reset if not reloaded
#define WDT_CONTROL_WDT_ENABLED0     (1u << 1)
#define WDT_CONTROL_WDT_INT0         (1u << 2)
#define CLK_SELECT_WDT_LOCK_SET01    ((1u << 14) | (1u << 15))
#define CLK_SELECT_WDT_LOCK_CLR0     (1u << 14)
#define CLK_SELECT_WDT_LOCK_CLR1     (1u << 15)
#define UPDATE_WDT_MATCH(value)      CY_SET_REG32(CYREG_WDT_MATCH, \
                                     (CY_GET_REG32(CYREG_WDT_MATCH) & 0xFFFF0000) + (uint16)value)


/*************************************
* LED Enable and Disable
*************************************/
#define OFF_LED  (0u)
#define GRN_LED  (1u)
#define ORG_LED  (2u)
#define RED_LED  (3u)
#define GRN_LED_ON  GRN_LED_N_Write(0)
#define GRN_LED_OFF GRN_LED_N_Write(1)
#define ORG_LED_ON  ORG_LED_N_Write(0)
#define ORG_LED_OFF ORG_LED_N_Write(1)
#define RED_LED_ON  RED_LED_N_Write(0)
#define RED_LED_OFF RED_LED_N_Write(1)

/*************************************
* BLE Advertizing LED Control
*************************************/
#define NUM_RETRY_BLE (5u) // Reconnect retry limit
#define NO  (0u)
#define YES (1u)

/* Constants for buttonState */
#define BUTTON_IS_PRESSED      (SW_Read()== 1u)
#define BUTTON_IS_NOT_PRESSED  (SW_Read()== 0u)

/* WDT related constants */

/* BLE related definitions */
#define CHARACTERISTIC_INSTANCE_1   (0u)
#define CHARACTERISTIC_INSTANCE_2   (1u)

/* Characteristic/Descriptor sizes */
#define SIZE_1_BYTES                (1u)
#define SIZE_2_BYTES                (2u)
#define SIZE_3_BYTES                (3u)
#define SIZE_4_BYTES                (4u)
#define SIZE_5_BYTES                (5u)

/***************************************
* External data references
***************************************/
#define KWJ_SAVE (CY_FLASH_NUMBER_ROWS - 1u)
#define DB_SAVE                      (1u)
#define LOG_SAVE                     (2u)
#define USER_SFLASH_ROW_SIZE         (128u) 
#define SFLASH_STARTING_VALUE        (0x00)
#define USER_SFLASH_ROWS             (4u)
#define USER_SFLASH_BASE_ADDRESS     (0x0FFFF200u)
#define LOAD_FLASH				     (0x80000004)
#define WRITE_USER_SFLASH_ROW	     (0x80000018)
#define USER_SFLASH_WRITE_SUCCESSFUL (0xA0000000)    
    
/***************************************
* Structures and enums
***************************************/
#define CO_LOG  (0u) // O3 Sensor
#define O3_LOG  (1u) // CO Sensor
#define T_LOG   (2u) // Temperature
#define H_LOG   (3u) // Humidity
#define P_LOG   (4u) // Pressure
#define BAT_LOG (5u) // Battery

/*************************************
* LED PWM Timing Constants
*************************************/
#define LED_OFF         (0u) // Off
#define LED_FLASH      (20u) // 20 mS
#define LED_BLINK     (500u) // 500 mS
#define LED_ON       (1000u) // On

/*************************************
* BUZZER PWM Timing Constants
*************************************/
#define BZR_OFF         (0u) // Off
#define BZR_PEEP       (50u) // 50mS
#define BZR_ALERT     (500u) // 500mS
#define BZR_ON       (1000u) // On

/*************************************
* BIAS_OP and MEAS_OP Gain Control
*************************************/
#define BIAS_HOLD  (AMux_1_FastSelect(0u)) // RE shorted to WE
#define BIAS_SENS (AMux_1_DisconnectAll()) // RE open
#define MEAS_1X   (AMux_2_FastSelect(0u))  // MEAS_OP = Follower
#define MEAS_AMP  (AMux_2_DisconnectAll()) // MEAS_OP = Amplifier

/*************************************
* CO & O3 Alert Levels
*************************************/
#define GOOD            (0u) // Level 0
#define MODERATE        (1u) // Level 1
#define UNHEALTHY       (2u) // Level 2
#define HAZARDOUS       (3u) // Level 3

/*************************************
* I2C Interface
*************************************/
#define I2C_tx     (SCB_I2CMasterWriteBuf)
#define I2C_rx     (SCB_I2CMasterReadBuf)
#define NO_I2C_ERR (0x0u) // I2C Success
#define T_RH_ADR   (0x40) // T/RH Sensor SAD
#define T_REQ      (0xE3) // Temperature Request
#define RH_REQ     (0xE5) // Rel Humidity Request
#define P_CTR1     (0x20) // P Control 1
//#define P_ENA      (0x84) // P Sensor Enable
#define P_ENA      (0x80) // P Sensor Enable
#define P_ADR      (0x5C) // P Sensor SAD
#define P_DAT      (0xA8) // Pressure Val Request
#define P_REQ_H    (0x2A) // P Sensor Hi packet
#define P_REQ_L    (0x29) // P Sensor Lo packet
#define P_DAT_XL   (0x28) // Pressure XLo packet

    #endif
/* [] END OF FILE */
