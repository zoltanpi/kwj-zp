/* ========================================
 * File Name: KWJ_Led_Bzr.c
 *
 * Version: 1.0
 *
 * Description:
 * This file implements the LED and Buzzer services
 *
 * Hardware Dependency:
 * CYBLE-014008
 *
 * Copyright ODI, 2016
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF ODI.
 * RCRegna - Author
 * ========================================
*/
#include "project.h"
#include "main.h"

int16 maxBat = 4;   // Initialise for 1st operation
int16 chgDrop = 20; // Delta BAT_RAW when charger is unplugged

/** AllOffLED() ***************************
*  Turn off all the LEDs
******************************************/
void AllOffLED(void)
{
    LED_PWM_Stop();
    LED_Interrupt_ClearPending();
    LED_PWM_ClearInterrupt(LED_PWM_INTR_MASK_TC);
    LED_Interrupt_Disable();
    rstFlag(LED_PWM_RUNNING);
    GRN_LED_OFF;
    ORG_LED_OFF;
    RED_LED_OFF;
}
/** HandleLED() ***************************
*  Determine LED and Mode and apply
******************************************/
void HandleLED(uint8 led, uint16 mode, bool ovride)
{
    if(BAT_FAULT_B && BAT_STATUS_B) // Not Charging
    {
        if(chkFlag(BAT_CHARGING) && (NOW_DB.BAT_RAW > (maxBat - chgDrop)))
        {
            GRN_LED_ON;
            mode = LED_ON; 
        }
        else
        {       
            rstFlag(BAT_CHARGING);
            maxBat = chgDrop;
            if((false == ovride) && chkFlag( MUTE_ALARM)) // Muted
            {
                led = 0u;
            }
            switch (led)
            {
                case GRN_LED:
                {
                    GRN_LED_ON;
                    break;
                }
                case ORG_LED:
                {
                    ORG_LED_ON;
                    break;
                }
                case RED_LED:
                {
                    RED_LED_ON;
                    break;
                }
                default:
                {
                    mode = LED_OFF;
                    break;
                }
            }
        }
    }
    else
    {   /* Battery Charger */
        setFlag(BAT_CHARGING);
        if(NOW_DB.BAT_RAW > maxBat) maxBat = NOW_DB.BAT_RAW;
        ~BAT_FAULT_B == true ? RED_LED_ON : ORG_LED_ON;
        mode = LED_ON;       
    }
    if(mode != LED_OFF)
    {
        setFlag(LED_PWM_RUNNING);
        LED_PWM_WritePeriod(mode);
        LED_PWM_Enable();
        LED_Interrupt_Enable();
    }
}
/** LedTimer_Isr() ***************************
*  Timer Interupt to turn off LEDs
******************************************/
CY_ISR(LedTimer_Isr)
{
    AllOffLED();
}
/** MuteBZR() *****************************
*  Turn off the Buzzer
******************************************/
void MuteBZR(void)
{
    BZR_FRQ_Stop();
    BZR_PWM_Stop();
    rstFlag(BZR_PWM_RUNNING);
    Piezo_P_SetDriveMode(Piezo_P_DM_DIG_HIZ);
    Piezo_N_SetDriveMode(Piezo_N_DM_DIG_HIZ);
}
/** HandleBZR() ***************************
*  Determine Buzzer Mode and apply
******************************************/
void HandleBZR(uint16 mode, bool ovride)
{
    MuteBZR();
    if(false == chkFlag( MUTE_ALARM) || ovride)
    {
        if(mode != BZR_OFF)
        {
            setFlag(BZR_PWM_RUNNING);
            Piezo_P_SetDriveMode(Piezo_P_DM_STRONG);
            Piezo_N_SetDriveMode(Piezo_N_DM_STRONG);
            BZR_PWM_WritePeriod(mode);
            BZR_PWM_Enable();
            BZR_FRQ_Start();
            BZR_Interrupt_Enable();
        }
    }
}
/** BzrTimer_Isr() ***************************
*  Timer Interupt to turn off Buzzer
******************************************/
CY_ISR(BzrTimer_Isr)
{
    BZR_Interrupt_ClearPending();
    BZR_PWM_ClearInterrupt(BZR_PWM_INTR_MASK_TC);
    MuteBZR();
    BZR_Interrupt_Disable();
}
/* [] END OF FILE */
