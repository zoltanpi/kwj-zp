/* ========================================
 * File Name: KWJ_Log.c
 *
 * Version: 1.0
 *
 * Description:
 * This file implements the BLE service for the Log Data
 *
 * Hardware Dependency:
 * CYBLE-014008
 *
 * Copyright ODI, 2016
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF ODI.
 * RCRegna - Author
 * ========================================
*/
#include "project.h"
#include "main.h"
#include "KWJ_BLE_Process.h"

#define LOG_PARAM_OS (0u)
#define LOG_OS       (1u)
#define NUM_LOG_ROWS (159u)
#define LOG_CO_ENA   (1u << 15)
#define LOG_O3_ENA   (1u << 12)
#define LOG_T_ENA    (1u << 9)
#define LOG_H_ENA    (1u << 6)
#define LOG_P_ENA    (1u << 3)

int16 MEAS_ACUMULATOR[5] = {0,0,0,0,0}; // CO, O3, T, RH, P acumulators
uint8 MEAS_FACTOR[5] = {1,1,1,1,3};     // CO, O3, T, RH, P factors
uint8 RAM_PTR;
uint16 FLASH_ROW = KWJ_SAVE - LOG_OS;
uint16 START_ROW = KWJ_SAVE - LOG_OS;
uint16 LOG_RAM[64]; // Logging buffer

typedef struct
{
    /** Used to create a database structure in RAM for logging parameters,
    *   in GATT for a GATT_DB exchange stack and FLASH for non-volatile storage
    ********************************************/
    uint8  TimeStamp[8]; // Y lo, Mo, D, Hr, Min, (Empty x 3)
    int16  Base16[4];    // CO, O3, T, RH Base Measurement
    uint32 Base32;       // P Base Measurement
    uint16 Type;         // Control for Logging on/off and factors
    uint16 Length;       // LogRecord Length
    uint8 *LogRecord;    // LogRecord pointer
}LOG_PARAM_T; // size = 28 bytes

static LOG_PARAM_T log_param;

/******************************************************************************
* Function Name: InitLogType
*******************************************************************************
* Summary:
*  Called to initialize the LogType register
*
******************************************************************************/
void InitLogType(void)
{
    // Restore LOG_PARAM from SFlash
//    FlashToStruct(LOG_SAVE, (LOG_PARAM_T *)&log_param);
    
    if(false == (SAV_DB.CAL_DONE & LOG_TYPE_CAL))
    {
        uint8 i;
        uint16 type = 0u;
        
        for(i = 0; i < 5; i++)
        {
            type <<= 3;
            type |= (4 + MEAS_FACTOR[i]); // set enable bit by default
        }
        type <<= 1;
//        type |= 0x0001u; //ts
        log_param.Type = type;
    }
}

/******************************************************************************
* Function Name: InitLog
*******************************************************************************
* Summary:
*  Called when logging function is initiated by app to provide starting point
*    for Log data
*
******************************************************************************/
void InitLog(void)
{
    uint8 i;
    for(i = 0; i < 5; i++)
    {
        MEAS_ACUMULATOR[i] = 0;
    }
    RAM_PTR = 0;
    FLASH_ROW = KWJ_SAVE - LOG_OS;
    START_ROW = KWJ_SAVE - LOG_OS;
    
    log_param.Type        |= 0x01u; // turn on logging
    
    uint32 theTime = RTC_GetTime();
    log_param.TimeStamp[0] = theTime >> 8;  //minutes
    log_param.TimeStamp[1] = theTime >> 16; //hours
    theTime = RTC_GetDate();
    log_param.TimeStamp[2] = theTime >> 16; //day
    log_param.TimeStamp[3] = theTime >> 24; //month
    log_param.TimeStamp[4] = theTime;       //year
    
    log_param.Base16[3]    = (int16) NOW_DB.CO_MEAS;
    log_param.Base16[2]    = (int16) NOW_DB.O3_MEAS;
    log_param.Base16[1]    = (int16) NOW_DB.T_MEAS;
    log_param.Base16[0]    = (int16) NOW_DB.RH_MEAS;
    log_param.Base32       = NOW_DB.P_MEAS;
    
    log_param.Length       = 0u;
    
    // Update SFlash from LOG_PARAM
    WriteUserSFlashRow(LOG_SAVE,StructToFlash((LOG_PARAM_T *)&log_param, sizeof(log_param)));
}
/******************************************************************************
* Function Name: StartLog
*******************************************************************************
* Summary:
*  Starts logging parameters in GATT DB & inits T/S and Base if logging on
*
******************************************************************************/
void StartLog(void)
{
    if(log_param.Type & 0x1u)
    {
        InitLog();
        GattSetCharVal(LOG_TIME_STMP, SIZE_5_BYTES, log_param.TimeStamp);
        GattSetCharVal(LOG_BASE16, 8u, VarToCharArray(&log_param.Base16, 8u));
        GattSetCharVal(LOG_BASE32, SIZE_4_BYTES, VarToCharArray(&log_param.Base32, SIZE_4_BYTES));
        GattSetCharVal(LOG_LENGTH, SIZE_2_BYTES, VarToCharArray(&log_param.Length, SIZE_2_BYTES));
    }
    GattSetCharVal(LOG_TYPE, SIZE_2_BYTES, VarToCharArray(&log_param.Type, SIZE_2_BYTES));
}

/******************************************************************************
* Function Name: StopLog
*******************************************************************************
* Summary:
*  Stops any uploading of Log and disables logging
*
******************************************************************************/
void StopLog(void)
{
    log_param.Type &= ~0x0001;
    
    // Update SFlash from LOG_PARAM
    CySysFlashWriteRow(KWJ_SAVE-LOG_PARAM_OS,StructToFlash((LOG_PARAM_T *)&log_param, sizeof(log_param)));
}
/******************************************************************************
* Function Name: LogHandler
*******************************************************************************
* Summary:
*  Called when app writes to LOG_TYPE 
*
*  LOG_TYPE 16bits: CabOcdTefHghPijL  (msb - lsb)  on = 1 / off = 0
*
*  Log  on/off factor              factor*       LOG (L)  Action
* -----+------+-------            --------      --------+---------
*  CO:    C      ab                00  x1           0     Log Off
*  O3:    O      cd                01  x2        0 -> 1   Start Log
*   T:    T      ef                10  x4           1     Log On
*  RH:    H      gh                11  x8        1 -> 0   Stop Log
*   P:    P      ij
*  LOG:   L     Action           *factor is compression in log file
*
******************************************************************************/
void    LogHandler(uint8 * value)
{
    int16 type = value[1];
    type <<= 8;
    type |= value[0];
    if((type & 0x0001) && ~(log_param.Type & 0x0001))
    {
        log_param.Type = type;
        StartLog();
    }
    if(~(type & 0x0001) && (log_param.Type & 0x0001)) StopLog();
    log_param.Type = type;
}
/******************************************************************************
* Function Name: WriteUserSFlashRow
*******************************************************************************
* Summary: Save system vitals (SAV_DB) onto protected supervisory Flash
*
* Parameters:
*  Row in SFlash (0-3)
*  Buffer containing vitals in byte format
*
* Return:
*  Write Result (0xA0000000 if successful)
*
******************************************************************************/
uint32 WriteUserSFlashRow(uint8 userRowNUmber, uint8 *buffer)
{
    uint8 localCount;
	volatile uint32 retValue=0;
	volatile uint32 cmdDataBuffer[(CY_FLASH_SIZEOF_ROW/4) + 2];
	volatile uint32 reg1,reg2,reg3,reg4,reg5,reg6;
	
	/* Store the clock settings temporarily */
    reg1 =	CY_GET_XTND_REG32((void CYFAR *)(CYREG_CLK_SELECT));
    reg2 =  CY_GET_XTND_REG32((void CYFAR *)(CYREG_CLK_IMO_CONFIG));
    reg3 =  CY_GET_XTND_REG32((void CYFAR *)(CYREG_PWR_BG_TRIM4));
    reg4 =  CY_GET_XTND_REG32((void CYFAR *)(CYREG_PWR_BG_TRIM5));
    reg5 =  CY_GET_XTND_REG32((void CYFAR *)(CYREG_CLK_IMO_TRIM1));
    reg6 =  CY_GET_XTND_REG32((void CYFAR *)(CYREG_CLK_IMO_TRIM2));
	
	/* Initialize the clock necessary for flash programming */
	CY_SET_REG32(CYREG_CPUSS_SYSARG, 0x0000e8b6);
	CY_SET_REG32(CYREG_CPUSS_SYSREQ, 0x80000015);
	
	/******* Initialize SRAM parameters for the LOAD FLASH command ******/
	/* byte 3 (i.e. 00) is the Macro_select */
	/* byte 2 (i.e. 00) is the Start addr of page latch */
	/* byte 1 (i.e. d7) is the key 2  */
	/* byte 0 (i.e. b6) is the key 1  */
  	cmdDataBuffer[0]=0x0000d7b6;
	
	/****** Initialize SRAM parameters for the LOAD FLASH command ******/
	/* byte 3,2 and 1 are null */
	/* byte 0 (i.e. 7F) is the number of bytes to be written */
	cmdDataBuffer[1]=0x0000007F;	 
    
	/* Initialize the SRAM buffer with data bytes */
    uint8 bfrOS = 0;
    for(localCount = 0; localCount < (CY_FLASH_SIZEOF_ROW/4); localCount++)    
	{
        uint8 j;
        uint32 dataVal = 0;
        for(j = 0; j < 4; j++)
        {
            uint32 dataTmp = buffer[bfrOS++];
            dataTmp <<= (8*j);
            dataVal |= dataTmp;
        }
		cmdDataBuffer[localCount + 2] = dataVal; 
	}
	
	/* Write the following to registers to execute a LOAD FLASH bytes */
	CY_SET_REG32(CYREG_CPUSS_SYSARG, &cmdDataBuffer[0]);
	CY_SET_REG32(CYREG_CPUSS_SYSREQ, LOAD_FLASH);
	
    /****** Initialize SRAM parameters for the WRITE ROW command ******/
	/* byte 3 & 2 are null */
	/* byte 1 (i.e. 0xeb) is the key 2  */
	/* byte 0 (i.e. 0xb6) is the key 1  */
	cmdDataBuffer[0] = 0x0000ebb6;
    
	/* byte 7,6 and 5 are null */
	/* byte 4 is desired SFlash user row 
	 * Allowed values 0 - row 4
	                  1 - row 5 
					  2 - row 6
					  3 - row 7 */
	cmdDataBuffer[1] = (uint32) userRowNUmber;
	
	/* Write the following to registers to execute a WRITE USER SFlash ROW command */
	CY_SET_REG32(CYREG_CPUSS_SYSARG, &cmdDataBuffer[0]);
	CY_SET_REG32(CYREG_CPUSS_SYSREQ, WRITE_USER_SFLASH_ROW);
    
	/* Read back SYSARG for the result. 0xA0000000 = SUCCESS; */
	retValue = CY_GET_REG32(CYREG_CPUSS_SYSARG);
	/* Restore the clock settings after the flash programming is done */
    CY_SET_XTND_REG32((void CYFAR *)(CYREG_CLK_SELECT),reg1);
    CY_SET_XTND_REG32((void CYFAR *)(CYREG_CLK_IMO_CONFIG),reg2);
    CY_SET_XTND_REG32((void CYFAR *)(CYREG_PWR_BG_TRIM4),reg3);
    CY_SET_XTND_REG32((void CYFAR *)(CYREG_PWR_BG_TRIM5),reg4);
    CY_SET_XTND_REG32((void CYFAR *)(CYREG_CLK_IMO_TRIM1),reg5);
    CY_SET_XTND_REG32((void CYFAR *)(CYREG_CLK_IMO_TRIM2),reg6);  
	
	return retValue;
}

/******************************************************************************
* Function Name: StructToFlash
*******************************************************************************
* Summary:
*  Return a uint8[] pointer representing the Structure for storing
*  in Flash. Flash row is 128 Bytes. Structure is 128 Bytes max
*
* Parameters:
*  Structure pointer
*
* Return:
*  uint8 [] pointer
*
******************************************************************************/
uint8 * StructToFlash(void *db_name, uint8 db_size)
{
    uint8 i;
    uint8 *row = &BFR[0];
    
    for(i = 0; i < db_size; i++){
        BFR[i] = (*((char*)(db_name) + i));
        if(i == CY_FLASH_SIZEOF_ROW - 1) 
            break; // cutoff anything larger than row size
    }
    return row;

}
/******************************************************************************
* Function Name: FlashToStruct
*******************************************************************************
* Summary:
*  Use Flash row data representing a structure and rebuild the raw content
*
* Parameters:
*  Flash Row uint8 [] pointer, Structure pointer
*
******************************************************************************/
void FlashToStruct(uint8 theRow, void *db_name)
{
    uint8 *sflashPtr;
    uint8 *dbPtr;
    uint8 i;
                
    dbPtr     = db_name;
    sflashPtr = (uint8 *)(USER_SFLASH_BASE_ADDRESS+USER_SFLASH_ROW_SIZE*theRow);

    for(i = 0; i < USER_SFLASH_ROW_SIZE; i++)
    {
        *dbPtr++ = *sflashPtr++;
    }
}
/******************************************************************************
* Function Name: LogMeas
*******************************************************************************
* Summary:
*  Calculates a delta log value and keeps accumulator for deltas greater than
*  log term (signed 3 bit). Log term is scaled by 1x, 2x, 4x or 8x. Positive
*  checksum in bit 0, CO 15-13, O3 12-10, T 9-7, H 6-4, P 3-1
*
* Parameters:
*  none
*
* Return:
*  none
*
* Calls:
*  Log_Readings with Log Term as Parameter
*
******************************************************************************/
void LogMeas(void)
{
    if(log_param.Type & 0x01u) // logging enabled
    {
        uint16 logData = 0u;
        int32  deltaMeas;
        uint16 i;
        
        for(i = 0; i < 5; i++)
        {
            logData <<= 3;
            switch(i)
            {
                case 0: // CO
                    deltaMeas = NOW_DB.CO_MEAS - SAV_DB.CO_MEAS;
                    SAV_DB.CO_MEAS = NOW_DB.CO_MEAS;
                break;
                case 1: // O3
                    deltaMeas = NOW_DB.O3_MEAS - SAV_DB.O3_MEAS;
                    SAV_DB.O3_MEAS = NOW_DB.O3_MEAS;
                break;
                case 2: // Temp
                    deltaMeas = NOW_DB.T_MEAS - SAV_DB.T_MEAS;
                    SAV_DB.T_MEAS = NOW_DB.T_MEAS;
                break;
                case 3: // Rel Humidity
                    deltaMeas = NOW_DB.RH_MEAS - SAV_DB.RH_MEAS;
                    SAV_DB.RH_MEAS = NOW_DB.RH_MEAS;
                break;
                case 4: // Pressure
                    deltaMeas = NOW_DB.P_MEAS - SAV_DB.P_MEAS;
                    SAV_DB.P_MEAS = NOW_DB.P_MEAS;
                break;
            }
            deltaMeas >>= MEAS_FACTOR[i];
            
            MEAS_ACUMULATOR[i] += deltaMeas;
            if(MEAS_ACUMULATOR[i] >= 4) deltaMeas = 3;
            else if(MEAS_ACUMULATOR[i] < -4) deltaMeas = -4;
            MEAS_ACUMULATOR[i] -= deltaMeas;
            
            logData |= deltaMeas;
        }
        // Add positive parity
        logData <<= 1;
        for(i = 1; i > 0; i<<=1)
        {
            if(i & logData) logData ^= 1;
        }
        Log_Readings(logData);
    }
}
/******************************************************************************
* Function Name: Log_Readings
*******************************************************************************
* Summary:
*  Buffer Loggged mesurements and transfer to Flash
*
* Parameters:
*  Log Term: 16 bit with positive check sum
*
* Return:
*  none
*
******************************************************************************/
void Log_Readings(uint16 log_data)
{
    if(log_param.Length < NUM_LOG_ROWS)
    {
        LOG_RAM[RAM_PTR++] = log_data;
        if(RAM_PTR == 64)
        {
            RAM_PTR = 0;
            uint8 i;
        
            for(i = 0; i < 128; i++){
                BFR[i] = *((char*)(&LOG_RAM) + i);
            }
            CySysFlashWriteRow(FLASH_ROW--, BFR);
            log_param.Length++;
            GattSetCharVal(LOG_LENGTH, SIZE_2_BYTES,
                VarToCharArray(&log_param.Length, SIZE_2_BYTES));
        }
    }
}
/******************************************************************************
* Function Name: LogUploader
*******************************************************************************
* Summary:
*  Uploads Loggged mesurements from Flash 1 row per 'Notification' enable
*  Resets the 'Notification' enable bit and uploads the row and notifies
*  until LOG_LENGTH has been reached
*
* Parameters:
*  CCCD value for LOG
*
******************************************************************************/
void    LogUploader(uint8 * value)
{
    uint16 cmd = value[1];
    cmd <<= 8;
    cmd |= value[0];
    
    if(cmd & 0x0001u)       // Check if Notify Enabled set
    {
        
        if(log_param.Type & 0x0001u)
        {
            
            FLASH_ROW = KWJ_SAVE - LOG_OS;
            log_param.Type &= ~0x0001u; // stop logging
            GattSetCharVal(LOG_TYPE, 2u, VarToCharArray(&log_param.Type, 2u));
        }
        if(FLASH_ROW >= KWJ_SAVE - LOG_OS - log_param.Length)
        {
            
            CYBLE_GATTS_HANDLE_VALUE_NTF_T ntfReqParam;
            
            ntfReqParam.attrHandle = (CYBLE_GATT_DB_ATTR_HANDLE_T) LOG_DATA;
            ntfReqParam.value.val = VarToCharArray(&FLASH_ROW, 128u);
            ntfReqParam.value.len = 128u;

            if(CYBLE_GATT_ERR_NONE == CyBle_GattsWriteAttributeValue(&ntfReqParam,
                0u, NULL, CYBLE_GATT_DB_LOCALLY_INITIATED))
            {
            
                GattSetCharVal(LOG_CCCD, 2u, VarToCharArray(&cmd, 2u));
                if(YES == CYBLE_IS_NOTIFICATION_ENABLED(LOG_CCCD))
                {
                    if(CYBLE_ERROR_OK==CyBle_GattsNotification(cyBle_connHandle,
                        &ntfReqParam))
                    {                        
                        FLASH_ROW--;
                        cmd &= ~0x0001;    // Reset Notify Enable
                        GattSetCharVal(LOG_CCCD, 2u, VarToCharArray(&cmd, 2u));
                    }
                }
            }
        }
    }
}
/* [] END OF FILE */
