/* ========================================
 * File Name: main.h
 *
 * Version: 1.0
 *
 * Description:
 * This file implements the Global Variable Definitions for KWJ
 *
 * Hardware Dependency:
 * CYBLE-014008
 *
 * Copyright ODI, 2016
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF ODI.
 * RCRegna - Author
 * ========================================
*/
#ifndef KWJ_MAIN_H
#define KWJ_MAIN_H
    
#include "project.h"
#include "KWJ_DEF.h"
    
/*******************************************
* Compile Time Firmware Options
*******************************************/
#define AUTO_CONNECT // Auto connect BLE
//#define O3_OPTION    // Enables code for O3 Option
#define DS_OKAY      // Enables deepsleep mode
//#define KWJ_TEST     // Test Mode
#define FAIL_SAFE    // Use SysTick to monitor for program hangup recovery

/*******************************************
* Interupt Service Routine Declarations
*******************************************/
#ifdef FAIL_SAFE
    CY_ISR_PROTO(SysTick_ISR);
#endif
CY_ISR_PROTO(WatchdogTimer_Isr);
CY_ISR_PROTO(ButtonPress_Isr);
CY_ISR_PROTO(LedTimer_Isr);
CY_ISR_PROTO(BzrTimer_Isr);

/*****************************************************************************
* CONSTANT Declarations
*****************************************************************************/
/* 32 Bit System Flag Register - Defaults to all 0 */
#define WAKEUP_SOURCE_WDT               (1u << 0)
#define MEASUREMENT_INTERVAL            (1u << 1)
#define DEEP_SLEEP_DISABLED             (1u << 2)
#define NEW_GATT_DB                     (1u << 3)
#define CHANGE_BLE                      (1u << 4)
#define LOW_RATE_BLE                    (1u << 5)
#define ENABLE_HIBERNATE                (1u << 6)
#define STAND_ALONE_MODE                (1u << 7)
#define ENABLE_LOGGING                  (1u << 8)
#define START_UP                        (1u << 9)
#define AUTO_CONNECT_MODE               (1u << 10)
//#define UNUSED_1                        (1u << 11)
#define BAT_CHARGING                    (1u << 12)
#define LOW_BATTERY                     (1u << 13)
#define CRITICAL_BATTERY                (1u << 14)
#define FULL_BATTERY                    (1u << 15)
#define FIVE_MINUTE                     (1u << 16)
#define LAST_STATE                      (1u << 17)
#define BAS_NOTIFICATION                (1u << 18)
#define DIS_NOTIFICATION                (1u << 19)
#define ESS_H_NOTIFICATION              (1u << 20)
#define ESS_P_NOTIFICATION              (1u << 21)
#define ESS_T_NOTIFICATION              (1u << 22)
#define KWJ_CO_NOTIFICATION             (1u << 23)
#define KWJ_O3_NOTIFICATION             (1u << 24)
#define ALL_NOTIFICATION                (1u << 25)
#define OP_WARMUP                       (1u << 26)
#define MUTE_ALARM                      (1u << 27)
#define ALERT_HAZARDOUS                 (1u << 28)
#define LED_PWM_RUNNING                 (1u << 29)
#define BZR_PWM_RUNNING                 (1u << 30)
//#define UNUSED_2                        (1u << 31)

/* 32 Bit CAL Complete Flag Register - Defaults to all 0 */
#define CO_GAIN_CAL                     (1u << 0)
#define CO_OS_CAL                       (1u << 1)
#define CO_TZRO_TC_CAL                  (1u << 2)
#define CO_SNS_FAC_CAL                  (1u << 3)
#define CO_MOD_CAL                      (1u << 4)
#define CO_UNH_CAL                      (1u << 5)
#define CO_HAZ_CAL                      (1u << 6)
#define O3_GAIN_CAL                     (1u << 7)
#define O3_OS_CAL                       (1u << 8)
#define O3_TZRO_TC_CAL                  (1u << 9)
#define O3_SNS_FAC_CAL                  (1u << 10)
#define O3_MOD_CAL                      (1u << 11)
#define O3_UNH_CAL                      (1u << 12)
#define O3_HAZ_CAL                      (1u << 13)
#define T_GAIN_CAL                      (1u << 14)
#define T_OS_CAL                        (1u << 15)
#define RH_GAIN_CAL                     (1u << 16)
#define RH_OS_CAL                       (1u << 17)
#define P_GAIN_CAL                      (1u << 18)
#define P_OS_CAL                        (1u << 19)
#define BAT_GAIN_CAL                    (1u << 20)
#define BAT_OS_CAL                      (1u << 21)
#define BAT_LO_CAL                      (1u << 22)
#define BAT_CRIT_CAL                    (1u << 23)
#define LOG_TYPE_CAL                    (1u << 24)
#define DIS_MNFR_CAL                    (1u << 25)
#define DIS_MODL_CAL                    (1u << 26)
#define DIS_SW_REV_CAL                  (1u << 27)

    
#define NOTIFICATIONS (BAS_NOTIFICATION | DIS_NOTIFICATION | ESS_H_NOTIFICATION\
            | ESS_P_NOTIFICATION | ESS_T_NOTIFICATION | ESS_P_NOTIFICATION\
            | ESS_T_NOTIFICATION | KWJ_CO_NOTIFICATION | KWJ_O3_NOTIFICATION)
  
/************** GLOBAL VARIABLES   ********************************/
char   SOFTWARE[15];  // Software revision
#ifdef FAIL_SAFE
uint32 SYSTK_CNT;     // Failsafe counter
#endif
uint8  BLE_RETRY;     // Counter for retrying BLE reconnection
uint16 MIN_COUNT;     // Counter for start up minutes elapsed
uint16 WRM_COUNT;     // Counter for number of warm_up cycles
uint32 P0_INIT;
uint32 P1_INIT;
uint32 P2_INIT;
uint32 P3_INIT;
uint8  BFR[128];      // Temporary storage of memory data
uint8  alert;         // Present Alert Level Good/MOD/UNH/HAZ
uint16 CO_MOD;        // PPB of CO Moderate Threshold
uint16 CO_UNH;        // PPB of CO Unhealthy Threshold
uint16 CO_HAZ;        // PPB of CO Hazardous Threshold
uint16 O3_MOD;        // PPB of O3 Moderate Threshold
uint16 O3_UNH;        // PPB of O3 Unhealthy Threshold
uint16 O3_HAZ;        // PPB of O3 Hazardous Threshold
uint8  BAT_LO;        // Battery Low Threshold
uint8  BAT_CRIT;      // Battery Critical Threshold

typedef struct
{
    /** Used to create a database structure in RAM for ongoing measurements,
    *   in GATT for a GATT_DB exchange stack and FLASH for non-volatile storage
    ********************************************/
    // CO Sensor Params (12 bytes)....
    int16  CO_MEAS;
    int16  CO_RAW;
    float  CO_GAIN;
    float  CO_OS;
    int32  CO_TZRO_TC;
    float  CO_SNS_FAC;
    // O3 Sensor Params (12 bytes)....
    int16  O3_MEAS;
    int16  O3_RAW;
    float  O3_GAIN;
    float  O3_OS;
    int32  O3_TZRO_TC;
    float  O3_SNS_FAC;
    // Temperature Sensor Params (12 bytes)....
    int16  T_MEAS;
    uint16 T_RAW;
    float  T_GAIN;
    float  T_OS;
    // Relative Humidity Sensor Params (12 bytes)....
    uint16 RH_MEAS;
    uint16 RH_RAW;
    float  RH_GAIN;
    float  RH_OS;
    // Barametric Pressure Sensor Params (16 bytes)....
    uint32 P_MEAS;
    uint32 P_RAW;
    float  P_GAIN;
    float  P_OS;
    // Battery Charge Params (12 bytes)....
    uint16 BAT_MEAS;
    int16  BAT_RAW;
    float  BAT_GAIN;
    float  BAT_OS;
    // CAL Done Save (4 bytes)...
    uint32 CAL_DONE;
    // Device Params (48 bytes)....
    uint8  MODEL[16];
    uint8  MNFR[8];
    uint8  SERIAL[8];
    
}PARAM_DB_T; // Total 128 bytes

/* Global DataBase Structures */
PARAM_DB_T SAV_DB;   // Flash parameter DB
PARAM_DB_T NOW_DB;   // SRAM  parameter DB

/*****************************************************************************
* System Function Declarations
*****************************************************************************/
// main.c
extern void setFlag(uint32 theFlag);
extern void rstFlag(uint32 theFlag);
extern bool chkFlag(uint32 theFlag);
extern void System_Init(void);
extern void SAmode(void);
extern void BLEmode(void);

// KWJ_Smpl.c
extern void SampleAll(void);

// KWJ_Button.c
extern void PowerOn_ISR(void);
extern void ButtonPress_ISR(void);

// KWJ_Wdt.c
extern void UpdateCycle(void);
extern void setInterval(uint32 theInterval);
extern void Hibernate (void);
extern void GoToDeepSleep (void);
extern void GoToSleep (void);
extern void WakeFromSleep (void);
extern void WatchdogTimer_Isr(void);
extern void WatchdogTimer_Start(uint32 wdtPeriodMs);
extern void WatchdogTimer_Stop(void);
extern void WatchdogTimer_Lock(void);
extern void WatchdogTimer_Unlock(void);

// KWJ_Sensor.c
extern void   Init_Gas_DB(void);
extern int16  Make_CO_Measurement(void);
extern int16  Make_O3_Measurement(void);

// KWJ_Led_Bzr.c
extern void LedTimer_Isr(void);
extern void BzrTimer_Isr(void);
extern void AllOffLED(void);
extern void HandleBZR(uint16 mode, bool ovride);
extern void HandleLED(uint8 led,uint16 mode, bool ovride);
extern void MuteBZR(void);

// KWJ_Log.c
extern void    InitLogType(void);
extern void    InitLog(void);
extern void    StartLog(void);
extern void    LogHandler(uint8 * value);
extern void    LogUploader(uint8 * value);
extern void    FlashToStruct(uint8 theRow, void *db_name);
extern void    Log_Readings(uint16 log_data);
extern uint8 * StructToFlash(void *db_name, uint8 db_size);
extern void    LogMeas(void);
extern uint32  WriteUserSFlashRow(uint8 userRowNUmber, uint8 *buffer);

/*****************************************************************************
* BLE Function Declarations
*****************************************************************************/
// KWJ_BLE_Process.c
extern void DisconnectBLE(void); // Zoltan
extern void initGATT(void);
extern void ConnectionEventHandler(uint32 event, void *eventParam);
extern void SendGattDbOverBLE(void);
extern void KWJ_BLE_Process(void);
extern uint8 * VarToCharArray(void *  memLoc, uint8 numByte);
extern CYBLE_API_RESULT_T GattSetCharVal(
    CYBLE_GATT_DB_ATTR_HANDLE_T serviceIndex, 
    uint8  attrSize, 
    uint8 *attrValue);

// KWJ_Custom.c
extern void   KwjEventHandler(CYBLE_GATTS_WRITE_REQ_PARAM_T *wrReqParam);
extern void   SendKwjOverBLE(void);

// KWJ_ESS.c
extern void   EssEventHandler(CYBLE_GATTS_WRITE_REQ_PARAM_T *wrReqParam);
extern void   SendEssOverBLE(void);
extern void   Init_Ess_DB(void);
extern uint16 Make_RH_Measurement(void);
extern uint32 Make_P_Measurement(void);
extern int16  Make_T_Measurement(void);

// KWJ_Batt.c
extern void  BasEventHandler(CYBLE_GATTS_WRITE_REQ_PARAM_T *wrReqParam);
extern void  SendBasOverBLE(void);
extern void  Init_Bat_DB(void);
extern bool  Make_BAT_Measurement(void);

// KWJ_Device.c
extern void  Init_Dev_DB(void);
extern void  DisEventHandler(CYBLE_GATTS_WRITE_REQ_PARAM_T *wrReqParam);
extern void  DisUpdateFirmWareRevision(void);
extern void  SendDisOverBLE(void);

// KWJ_Rtc.c
extern void   RtcEventHandler(CYBLE_GATTS_WRITE_REQ_PARAM_T *wrReqParam);
extern void   Sys_RTC_Update(uint8 * value);

#endif
/* [] END OF FILE */
